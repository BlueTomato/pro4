﻿Shader "Unlit/Colorize"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_AddColor ("Add Color", Color) = (0,0,0,0)
		_SubtractColor ("Subtract Color", Color) = (0,0,0,0)
		_TargetColor ("Tint", Color) = (1,1,1,1)
		_MinColor ("Minimum Color", Color) = (0,0,0,0)
		_MaxColor ("Maximum Color", Color) = (1,1,1,1)
		_Weight("Weight", range(0, 1))=0
		_Multiply("Multiply", range(0, 1))=0
		_Brightness("Brightness", range(0, 2))=1
		_ShiftRedGreen("Shift Red Green", range(0, 1))=0
		_ShiftRedBlue("Shift Red Blue", range(0, 1))=0
		_ShiftGreenBlue("Shift Green Blue", range(0, 1))=0

		_Pixel ("Pixel per unit", int)=16

		_Overlay ("Overlay Texture (RGBA)", 2D) = "white" {}
		_OverlayColor ("Overlay Color", Color) = (0,0,0,0)
		_OverlayScale ("Overlay Scale",  float)=1
		_OverlayScaleX ("Overlay Scale X",  float)=1
		_OverlayScaleY ("Overlay Scale Y",  float)=1
		_OverlaySpeedX("Overlay Speed X", float)=0.1
		_OverlaySpeedY("Overlay Speed Y", float)=0.1
		_OverlayOffsetX ("Overlay offset X", float)=0
		_OverlayOffsetY ("Overlay offset Y", float)=0

		[MaterialToggle] _CenterOverlay("Center Overlay", Float) = 0
	}
		  
	SubShader
	{
		Tags
		{
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
            "RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			"DisableBatching"="True"
		}
		LOD 200
		 
        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend One OneMinusSrcAlpha
							 
		CGPROGRAM
		#pragma surface surf NoLighting alphatest:_Cutoff
		#include "Assets/Shaders/Recolor.cginc"

		sampler2D _MainTex;
			   
		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
			float4 color : COLOR;
		};

		half4 LightingNoLighting (SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o)
		{
			float4 c = tex2D (_MainTex, IN.uv_MainTex);

			c=applyColorOperations(c,IN.worldPos.xy);

			o.Albedo=c*IN.color*_Color.a;
			o.Alpha=c.a*IN.color.a*_Color.a;
		}
		ENDCG
	}
}