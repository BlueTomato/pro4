﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBoat : MonoBehaviour
{
    public Rigidbody2D rb;
    public Vector3 previousPosition;
    public Vector3 currentPosition;
    public void Start()
    {
        rb = GetComponentInParent<Rigidbody2D>();
    }

    public void FixedUpdate()
    {
        if (rb.transform.position.x > 90)
        {
            rb.transform.position = new Vector3(90, rb.transform.position.y, rb.transform.position.z);
        }
        previousPosition = currentPosition;
        currentPosition = transform.position;
    }

	public void OnTriggerStay2D(Collider2D other)
    {
        if(other.transform==GameManager.Instance.player.transform)
        {
            rb.velocity = new Vector2(4,0);
            if (other.transform.position.y < -3.5f)
            {
                other.transform.position = new Vector3(other.transform.position.x,-3.5f, other.transform.position.z);
            }
            other.transform.position = other.transform.position + (currentPosition-previousPosition);
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform == GameManager.Instance.player.transform)
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
}
