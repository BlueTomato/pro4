﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkAndGrow : MonoBehaviour
{
    public float min = 1;
    public float max = 2;
    public float period = 1;
	
	void Update ()
    {
        transform.localScale = Vector3.one * Sine.Get(Time.time*period, min, max,1f);
	}
}
