﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class Ability : ScriptableObjectEnum<Ability>
{
    public bool enabled = false;
    public int index=1;
    public Sprite image;
    [Multiline]
    public string description;
    private int setting=0;
    public int maxSetting = 1;

    public override void OnEnable()
    {
        _list.Add(this.name, this);
    }

    public void Toggle(int step)
    {
        setting = (int)Mathf.Repeat(setting + step, maxSetting + 1);
    }

    public void Toggle()
    {
        Toggle(1);
    }

    public void ToggleDown()
    {
        Toggle(-1);
    }

    public void Activate()
    {
        setting = maxSetting;
    }

    public void Deactivate()
    {
        setting = 0;
    }

    public int Setting
    {
        get
        {
            return setting;
        }
    }

    public int MaxSetting
    {
        get
        {
            return maxSetting;
        }
    }

    public bool IsActive
    {
        get
        {
            return Setting > 0;
        }
    }

    public bool IsFullyActive
    {
        get
        {
            return Setting >= MaxSetting;
        }
    }

    public bool IsEnabledAndActive
    {
        get
        {
            return enabled && IsActive;
        }
    }

    public bool NonBinary
    {
        get
        {
            return MaxSetting > 1;
        }
    }

    public static Ability[] GetEnabled()
    {
        List<Ability> enabledList = new List<Ability>();
        FillArray();
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].enabled)
            {
                enabledList.Add(array[i]);
            }
        }
        return enabledList.ToArray();
    }

    public static SortedDictionary<String,Ability> GetEnabledList()
    {
        Ability[] enabled = GetEnabled();
        SortedDictionary<String, Ability> enabledList = new SortedDictionary<String, Ability>();
        for(int i=0;i<enabled.Length;i++)
        {
            enabledList.Add(enabled[i].name,enabled[i]);
        }
        return enabledList;
    }
}