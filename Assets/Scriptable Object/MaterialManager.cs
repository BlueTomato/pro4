﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu]
public class MaterialManager : SingletonScriptableObject<MaterialManager>
{
    public List<Material> materialInGroup = new List<Material>();
    private List<Material> originalMaterial = new List<Material>();
    public List<MaterialGroup> groupForMaterial = new List<MaterialGroup>();
    public List<MaterialProperties> properties=new List<MaterialProperties>();

    void OnEnable()
    {
        properties = new List<MaterialProperties>();
        SaveMaterials();
    }

    public void SaveMaterials()
    {
        TrimUneven();
        RemoveEmpty();
        RemoveDoubles();
        ResetMaterials();
        originalMaterial = new List<Material>();

        List<Material> checkList = new List<Material>();

        for (int i = 0; i < materialInGroup.Count; i++)
        {
            Material m = materialInGroup[i];

            if (!checkList.Contains(m))
            {
                originalMaterial.Add(new Material(m));
            }
            else
            {
                int index = checkList.IndexOf(m);
                originalMaterial.Add(originalMaterial[index]);
            }
            checkList.Add(m);
        }
    }

    public void ResetMaterials()
    {
        for (int i=0;i< originalMaterial.Count && i<materialInGroup.Count;i++)
        {
            if(originalMaterial[i])
            {
                materialInGroup[i].CopyPropertiesFromMaterial(originalMaterial[i]);
            }
        }
    }
	

    public List<Material> GetMaterialsInGroup(MaterialGroup group)
    {
        TrimUneven();
        List<Material> m = new List<Material>();
        for (int i=0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if(groupForMaterial[i]==group || group==MaterialGroup.All)
            {
                m.Add(materialInGroup[i]);
            }
        }
        return m;
    }

    public List<MaterialGroup> GetGroupsFromMaterial(Material material)
    {
        TrimUneven();
        List<MaterialGroup> g = new List<MaterialGroup>();
        for (int i = 0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if (materialInGroup[i]==material)
            {
                g.Add(groupForMaterial[i]);
            }
        }
        return g;
    }

    public bool Add(MaterialGroup group, Material material)
    {
        TrimUneven();
        for (int i = 0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if(group==MaterialGroup.All || (groupForMaterial[i]==group && materialInGroup[i]==material))
            {
                return false;
            }
        }
        groupForMaterial.Add(group);
        materialInGroup.Add(material);
        return true;
    }

    public bool Remove(MaterialGroup group, Material material)
    {
        TrimUneven();
        for (int i = 0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if (groupForMaterial[i] == group && materialInGroup[i] == material)
            {
                groupForMaterial.RemoveAt(i);
                materialInGroup.RemoveAt(i);
                return true;
            }
        }
        return false;
    }

    public bool Remove(MaterialGroup group)
    {
        TrimUneven();
        bool removed = false;
        for (int i = 0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if (group == MaterialGroup.All || groupForMaterial[i] == group)
            {
                groupForMaterial.RemoveAt(i);
                materialInGroup.RemoveAt(i);
                i--;
                removed = true;
            }
        }
        return removed;
    }

    public bool Remove(Material material)
    {
        TrimUneven();
        bool removed = false;
        for (int i = 0; i < materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            if (materialInGroup[i] == material)
            {
                groupForMaterial.RemoveAt(i);
                materialInGroup.RemoveAt(i);
                i--;
                removed = true;
            }
        }
        return removed;
    }

    public void ApplyProperties()
    {
        ResetMaterials();

        properties.Sort(new MaterialProperties.Comparer());

        for(int i=0;i<properties.Count;i++)
        {
            properties[i].Apply();
        }
    }

    public void TrimUneven()
    {
        if (materialInGroup.Count > groupForMaterial.Count)
        {
            materialInGroup.RemoveRange(groupForMaterial.Count, materialInGroup.Count - groupForMaterial.Count);
        }
        else if (materialInGroup.Count < groupForMaterial.Count)
        {
            groupForMaterial.RemoveRange(materialInGroup.Count, groupForMaterial.Count - materialInGroup.Count);
        }
    }

    public void RemoveEmpty()
    {
        TrimUneven();

        for (int i=0;i<groupForMaterial.Count;i++)
        {
            if(!materialInGroup[i])
            {
                groupForMaterial.RemoveAt(i);
                i--;
            }
            else if(groupForMaterial[i]==MaterialGroup.All)
            {
                groupForMaterial.RemoveAt(i);
                materialInGroup.RemoveAt(i);
                i--;
            }
        }
    }

    public void RemoveDoubles()
    {
        for (int i=0;i< materialInGroup.Count && i < groupForMaterial.Count; i++)
        {
            for(int j= i+1;j< materialInGroup.Count && i < groupForMaterial.Count; j++)
            {
                if(materialInGroup[i]==materialInGroup[j] && groupForMaterial[i]==groupForMaterial[j])
                {
                    materialInGroup.RemoveAt(j);
                    groupForMaterial.RemoveAt(j);
                    j--;
                }
            }
        }
    }

    public void OnDestroy()
    {
        ResetMaterials();

        originalMaterial = new List<Material>();
        properties = new List<MaterialProperties>();
    }
}