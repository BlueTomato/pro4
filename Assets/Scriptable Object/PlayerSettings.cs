﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class PlayerSettings : SingletonScriptableObject<PlayerSettings>
{
    public GameObject player;
    public GameObject projectile;
    public float playerHealth=100;
    public float playerHealthMin;
    public float shotCost;
    public bool winged;
    public int jumps = 1;
    public float jumpHeight = 4;
    public Vector3 position = Vector3.zero;
    public bool placePlayerAtPosition = false;
    public bool track;
    public GameObject camera;
    public GameObject canvas;
    public GameObject gameManager;
    public Material magnetMaterial;
}
