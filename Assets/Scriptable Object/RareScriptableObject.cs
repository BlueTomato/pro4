﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RareScriptableObject : ScriptableObject
{
    public float rarity = 1;
}