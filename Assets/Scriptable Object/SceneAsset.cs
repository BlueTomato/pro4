﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class SceneAsset : ScriptableObjectEnum<SceneAsset>
{
    [Multiline]
    public string description;

    public override void OnEnable()
    {
        _list.Add(this.name, this);
    }
}