﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CorporateDesign : SingletonScriptableObject<CorporateDesign>
{
    public Color red = new Color(0.75f,0,0,1);
    public Font mainFont;
    public Vector2 smallIconSize = new Vector2(16, 16);
    public Vector2 iconSize = new Vector2(24,24);
    public string UILayer = "UI";
    public Sprite UIImage;
    public Shader spriteShader;
}