﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObjectEnum<Item>
{
    public GameObject item;
    [Multiline]
    public string description;

    public override void OnEnable()
    {
        _list.Add(this.name, this);
    }
}