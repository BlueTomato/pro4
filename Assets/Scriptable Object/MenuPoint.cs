﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public abstract class MenuPoint : ScriptableObjectEnum<MenuPoint>
{
    public MenuPoint parent;
    public List<MenuPoint> menuPoints = new List<MenuPoint>();
    public int index = 0;

    public override void OnEnable()
    {
        List.Add(this.name, this);
        Reset();
        foreach(MenuPoint m in menuPoints)
        {
            m.parent = this;
        }
    }

    public string[] ListMenuPoints()
    {
        string[] points = new string[menuPoints.Count+1];
        for(int i=0;i<menuPoints.Count;i++)
        {
            points[i] = menuPoints[i].name;
        }
        points[points.Length - 1] = "Close";
        return points;
    }

    public void Reset()
    {
        index = 0;
    }

    public virtual void Execute()
    {
    }

    public MenuPoint Activate()
    {
        if(index<menuPoints.Count)
        {
            menuPoints[index].Execute();
            return menuPoints[index];
        }
        return parent;
    }

    private void IncreaseIndex(int step)
    {
        index = (int)Mathf.Repeat(index += step, menuPoints.Count+1);
    }

    public void IndexUp()
    {
        IncreaseIndex(-1);
    }

    public void IndexDown()
    {
        IncreaseIndex(1);
    }
}