﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScriptableObjectEnum<T> : RareScriptableObject where T : RareScriptableObject
{
    protected static SortedDictionary<String, T> _list = new SortedDictionary<String, T>();
    public static T[] array;

    public static T Get(String name)
    {
        return _list[name];
    }

    public static SortedDictionary<String, T> List
    {
        get
        {
            return _list;
        }
    }

    public abstract void OnEnable();

    public static void FillArray()
    {
        SortedDictionary<String, T>.Enumerator enumerator = List.GetEnumerator();
        array = new T[List.Count];
        for (int i = 0; i < array.Length; i++)
        {
            enumerator.MoveNext();
            array[i] = enumerator.Current.Value;
        }
    }

    public static T GetRandomElement(float minRarity, float maxRarity)
    {
        if (array == null)
        {
            FillArray();
        }

        float range = 0;

        List<T> pool = new List<T>();

        for (int i = 0; i < array.Length; i++)
        {
            if(array[i].rarity>=minRarity && array[i].rarity<=maxRarity)
            {
                range += array[i].rarity;
                pool.Add(array[i]);
            }
        }

        float chosen = UnityEngine.Random.Range(0, range);
        float current = 0;
        int index = -1;
        if(pool.Count>0)
        {
            while (current < chosen)
            {
                index++;
                current += pool[index].rarity;
            }
        }

        if (index >= 0 && index < pool.Count)
        {
            return pool[index];
        }

        return null;
    }

    public static T GetRandomElement()
    {
        return GetRandomElement(0,float.MaxValue);
    }
}
