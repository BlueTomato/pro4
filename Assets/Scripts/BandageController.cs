﻿using UnityEngine;
using System.Collections;

public class BandageController : MonoBehaviour
{
    public Vector3 previousPosition = Vector3.zero;

    public Material material;
    public GameObject focusPoint;

    public int bandages=1;

    public int LOD = 0;
    private int previousLOD = 0;

    public float unit = 1;
    public int unitsX = 1;
    public int unitsZ = 1;
    private int previousUnitsX;
    private int previousUnitsZ;

    public int flattenUntil;

    public int waves = 1;
    private int previousWaves = -1;
    public bool[] additive;
    public bool[] spiked;
    public bool[] inverted;

    private float[] waterCycle = new float[0];

    public float waterSwayFactor = 1;
    public float[] waterSway;

    public float waveLengthFactor = 1;
    public float[] waveLength;

    public float waterSpeedFactor = 0.01f;
    public float[] waterSpeed;

    public float[] rotation;
    public float[] waveOffsetX;
    public float[] waveOffsetZ;
    public float waterOffset;

    public float waterRandomFactor = 0;
    public float[] waterRandom;
    public int[] waterRandomPeriodMin;
    public int[] waterRandomPeriodMax;


    private SmartArray<Mesh> mesh = new SmartArray<Mesh>();
    private SmartArray<MeshFilter> meshFilter = new SmartArray<MeshFilter>();
    private SmartArray<Bandage> plane = new SmartArray<Bandage>();


    public void Set()
    {
        for (int i = 0; i < plane.Length; i++)
        {
            Destroy(plane[i].gameObject);
        }

        plane.Resize(bandages);

        for (int i = 0; i < plane.Length; i++)
        {
            CreateMesh(i);
        }


        previousUnitsX = unitsX;
        previousUnitsZ = unitsZ;
        previousLOD = LOD;
        CreateGrid();
    }

    private void CreateGrid()
    {
    }


    public int[] CreateTriangles(int uX, int uZ)
    {
        int[] triangles = new int[uX * uZ * 6];

        int point = 0;
        while (point < triangles.Length)
        {
            for (int x = 0; x <= uX; x++)
            {
                for (int z = 0; z < uZ && point < triangles.Length; z++)
                {
                    int value = x * (uZ + 1) + z;

                    triangles[point] = value;
                    point++;
                    triangles[point] = value + 1;
                    point++;
                    triangles[point] = value + uZ + 1;
                    point++;
                    triangles[point] = value + 1;
                    point++;
                    triangles[point] = value + 2 + uZ;
                    point++;
                    triangles[point] = value + uZ +1;
                    point++;
                }
            }
        }
        return triangles;
    }

    public int[] CreateTriangles()
    {
        return CreateTriangles(GetLengthX(), GetLengthZ());
    }

    private void CreateMesh(int m)
    {
        mesh[m] = new Mesh();
        meshFilter[m] = new MeshFilter();
        mesh[m].name = "ScriptedMesh";
        GameObject p = new GameObject("Wave Plane " + (m + 1));
        plane[m] = p.AddComponent<Bandage>();
        p.transform.parent = transform;
        p.transform.localPosition = Vector3.zero;
        meshFilter[m] = (MeshFilter)p.AddComponent(typeof(MeshFilter));
        MeshRenderer renderer = p.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        renderer.material.shader = Shader.Find("Lambert");
        renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        renderer.receiveShadows = false;
        renderer.material = material;
        meshFilter[m].mesh = mesh[m];
    }

    private void UpdateValues()
    {
        if (unitsX < 1)
        {
            unitsX = 1;
        }
        if (unitsZ < 1)
        {
            unitsZ = 1;
        }
        if (waves < 0)
        {
            waves = 0;
        }
        if (LOD < 0)
        {
            LOD = 0;
        }

        if (waves != previousWaves)
        {
            previousWaves = waves;
            if (waterRandom.Length < 3)
            {
                waterRandom = SmartArray<float>.Resize(waterRandom, 3);
            }
            if (waterRandomPeriodMax.Length < 3)
            {
                waterRandomPeriodMax = SmartArray<int>.Resize(waterRandomPeriodMax, 3);
            }
            if (waterRandomPeriodMin.Length < 3)
            {
                waterRandomPeriodMin = SmartArray<int>.Resize(waterRandomPeriodMin, 3);
            }
            if (waterSway.Length < 3 * waves)
            {
                waterSway = SmartArray<float>.Resize(waterSway, 3 * waves);
            }
            if (rotation.Length < waves)
            {
                rotation = SmartArray<float>.Resize(rotation, waves);
            }
            if (additive.Length < waves)
            {
                additive = SmartArray<bool>.Resize(additive, waves);
            }
            if (inverted.Length < waves)
            {
                inverted = SmartArray<bool>.Resize(inverted, waves);
            }
            if (spiked.Length < waves)
            {
                spiked = SmartArray<bool>.Resize(spiked, waves);
            }
            if (waterSpeed.Length < 3 * waves)
            {
                waterSpeed = SmartArray<float>.Resize(waterSpeed, 3 * waves);
            }
            if (waveLength.Length < 3 * waves)
            {
                waveLength = SmartArray<float>.Resize(waveLength, 3 * waves);
            }
            if (waveOffsetX.Length < 3)
            {
                waveOffsetX = SmartArray<float>.Resize(waveOffsetX, 3);
            }
            if (waveOffsetZ.Length < 3)
            {
                waveOffsetZ = SmartArray<float>.Resize(waveOffsetZ, 3);
            }
            if (waterCycle.Length < waveLength.Length)
            {
                waterCycle = SmartArray<float>.Resize(waterCycle, waveLength.Length);
            }
        }

        if (LOD != previousLOD || unitsX != previousUnitsX || unitsZ != previousUnitsZ)
        {
            Set();
        }
    }

    public float Finalize(int type, float x, float z)
    {
        float value = 0;
        if (type == 0)
        {
            value += ((x - (unitsX / 2f)) * unit);
        }
        else if (type == 2)
        {
            value += z* unit;
        }

        return value;
    }


    public float ApplySine(int type, float x, float z, Vector3 center)
    {
        float value = 0;

        x = (x + (center.x / unit) - (GetUnitsX() / 2f)) * unit + transform.position.x;
        z = (z + (center.z / unit) - (GetUnitsZ() / 2f)) * unit + transform.position.z;

        for (int s = 0; s < waves; s++)
        {
            int u = 3 * s + type;

            if (waveLengthFactor * waveLength[u] != 0)
            {
                if (s == 0)
                {
                    value = 1;
                }
                float rotationX = Mathf.Sin(rotation[s]*Mathf.Deg2Rad);
                float rotationZ = Mathf.Cos(rotation[s] * Mathf.Deg2Rad);

                float calculation = waterSway[u] * Mathf.Sin(((rotationX * (x - (unitsX / 2f))) + (rotationZ * (z - (unitsZ / 2f))) + (waveOffsetX[type] * x) + (waveOffsetZ[type] * z) + waterCycle[u]) / (waveLengthFactor * waveLength[u]));
                if (spiked[s])
                {
                    calculation = Mathf.Abs(calculation) * -1f * Mathf.Sign(waterSway[u]);
                }
                if (inverted[s])
                {
                    calculation = -calculation;
                }

                if (!additive[s])
                {
                    value *= calculation;
                }
                else
                {
                    value += calculation;
                }
            }
        }

        return value *= waterSwayFactor;
    }

    public float ApplySine(int type, float x, float z)
    {
        Vector3 offset = new Vector3((GetUnitsX() / 2f) * unit, 0, (GetUnitsZ() / 2f) * unit);
        return ApplySine(type, x, z, offset);
    }

    public float FindY(float x, float z)
    {
        x = (x - transform.position.x) / unit;
        z = (z - transform.position.z) / unit;

        return ApplySine(1, x, z);
    }


    private void CycleStep()
    {
        for (int n = 0; n < 3; n++)
        {
            for (int i = 0; i < waves; i++)
            {
                waterCycle[n + 3 * i] += waterSpeedFactor * waterSpeed[(n + 3 * i) % waterSpeed.Length] * Time.timeScale;
            }
        }
    }


    public int GetUnitsX()
    {
        return unitsX;
    }
    public int GetUnitsZ()
    {
        return unitsZ;
    }
    public int GetLengthX()
    {
        return unitsX + 1;
    }
    public int GetLengthZ()
    {
        return unitsZ + 1;
    }

    public int GetVertexLengthX()
    {
        return ((GetLengthX() * 2) - 2);
    }

    public int GetVertexLengthZ()
    {
        return ((GetLengthZ() * 2) - 2);
    }




    void Start()
    {

    }


    void Update()
    {
        UpdateValues();
        CycleStep();
        previousPosition = transform.position;
    }

    void Awake()
    {
        Set();
    }
}