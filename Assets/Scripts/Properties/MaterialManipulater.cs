﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialManipulater : MonoBehaviour
{
    private List<Material> globalMaterials;
    private List<Material> localMaterials;

    public List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
    public List<SpriteMeshInstance> spriteMeshInstances = new List<SpriteMeshInstance>();

    public virtual void Start()
    {
        FindMaterials();
    }

    private void FindMaterials()
    {
        globalMaterials = new List<Material>();
        localMaterials = new List<Material>();
        spriteRenderers = Globals.GetComponentsRecursive<SpriteRenderer>(transform);
        spriteMeshInstances = Globals.GetComponentsRecursive<SpriteMeshInstance>(transform);
        for (int i = 0; i < spriteRenderers.Count; i++)
        {
            Material material = spriteRenderers[i].sharedMaterial;
            if (!localMaterials.Contains(material))
            {
                Material local = new Material(material);
                globalMaterials.Add(material);
                localMaterials.Add(local);
                spriteRenderers[i].sharedMaterial = local;
            }
        }
        for (int i = 0; i < spriteMeshInstances.Count; i++)
        {
            Material material = spriteMeshInstances[i].sharedMaterial;
            if (!localMaterials.Contains(material))
            {
                Material local = new Material(material);
                globalMaterials.Add(material);
                localMaterials.Add(local);
                spriteMeshInstances[i].sharedMaterial = local;
            }
        }
    }

    public void SetColor(Color color)
    {
        SetColor(color, 1);
    }

    public void SetColor(Color color, float weight)
    {
        foreach (Material m in localMaterials)
        {
            m.color = Color.Lerp(m.color, color, weight);
        }
        foreach (SpriteRenderer s in spriteRenderers)
        {
            s.color = Color.Lerp(s.color, color, weight);
        }
        foreach (SpriteMeshInstance s in spriteMeshInstances)
        {
            s.color = Color.Lerp(s.color, color, weight);
        }
    }

    public void SetColor(string property, Color color)
    {
        SetColor(property, color, 1);
    }

    public void SetColor(string property, Color color, float weight)
    {
        foreach (Material m in localMaterials)
        {
            if (m.HasProperty(property))
            {
                m.SetColor(property, Color.Lerp(m.GetColor(property), color, weight));
            }
        }
    }

    public void SetFloat(string property, float value)
    {
        SetFloat(property, value, 1);
    }

    public void SetFloat(string property, float value, float weight)
    {
        foreach (Material m in localMaterials)
        {
            if (m.HasProperty(property))
            {
                m.SetFloat(property, Mathf.Lerp(m.GetFloat(property), value, weight));
            }
        }
    }


    public void ResetMaterialProperties()
    {
        for (int i = 0; i < localMaterials.Count; i++)
        {
            localMaterials[i].CopyPropertiesFromMaterial(globalMaterials[i]);
        }
    }

    public virtual void Update()
    {
        ResetMaterialProperties();
    }
}
