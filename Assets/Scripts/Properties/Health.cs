﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    private float health;
    public float minHealth = 0;
    public float maxHealth = 100;

    public float recoverableHealth;
    public float naturalRecovery = 1;
    public float recoveryInterval = 1;
    private float lastRecovery;

    private float lastDamaged;
    private float lastHealed;
    private float lastFull;
    private float immuneUntil;
    private float invincibleUntil;
    private float immunityTime;
    private float invincibilityTime;
    public List<HealthText> healthTexts = new List<HealthText>();
    public Controller controller;

    private GameObject energyBarContainer;
    private GameObject energyBar;
    private GameObject recoveryBar;
    private Vector3 regularEnergyBarScale = new Vector3(2, 0.19f, 1);
    public float showSmallEnergyBarFor = 1;

    public float shotRecoveryPool=0;

    public float Get
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
            ClampHealth();
        }
    }

    public bool FullHealth
    {
        get
        {
            return health>=maxHealth;
        }
    }

    public bool NoHealth
    {
        get
        {
            return health <= 0;
        }
    }

    public float RelativeHealth
    {
        get
        {
            return Get / maxHealth;
        }
    }

    public float RelativeRecoverable
    {
        get
        {
            return recoverableHealth / maxHealth;
        }
    }

    public float RelationMinToMaxHealth
    {
        get
        {
            return minHealth / maxHealth;
        }
    }

    void Start()
    {
        health = maxHealth;
        ClampHealth();
        controller = GetComponent<Controller>();
        if(gameObject.tag!="Player")
        {
            CreateSmallHealthBar();
        }
        lastFull = -showSmallEnergyBarFor;
    }

    public void CreateSmallHealthBar()
    {
        energyBarContainer = GameObject.CreatePrimitive(PrimitiveType.Quad);
        energyBarContainer.name = gameObject.name + " Energy Bar";
        energyBarContainer.layer = LayerMask.NameToLayer(CorporateDesign.Instance.UILayer);
        Material m = energyBarContainer.GetComponent<Renderer>().material;
        m.color = Color.black;
        m.shader = CorporateDesign.Instance.spriteShader;

        energyBar = Instantiate(energyBarContainer);
        m = energyBar.GetComponent<Renderer>().material;
        m.color = CorporateDesign.Instance.red;

        recoveryBar = Instantiate(energyBarContainer);
        m = recoveryBar.GetComponent<Renderer>().material;
        m.color = Color.white;

        energyBar.transform.SetParent(energyBarContainer.transform);
        recoveryBar.transform.SetParent(energyBarContainer.transform);
        energyBar.transform.localPosition = new Vector3(0, 0, -0.25f);
        recoveryBar.transform.localPosition = new Vector3(0, 0, -0.25f);
        energyBarContainer.transform.localScale = regularEnergyBarScale;
    }

    public void CreateText(string text, Color color, bool shadow)
    {
        GameObject textObject = new GameObject();
        textObject.layer=LayerMask.NameToLayer(CorporateDesign.Instance.UILayer);
        textObject.name = text;
        TextMesh t = textObject.AddComponent<TextMesh>();
        //t.font = CorporateDesign.Instance.mainFont;
        t.fontStyle = FontStyle.Bold;
        t.alignment = TextAlignment.Center;
        t.anchor = TextAnchor.MiddleCenter;
        t.fontSize = 8;
        t.color = color;
        t.text = text;
        //t.transform.localScale = Vector3.one / 16f * t.fontSize;
        t.GetComponent<MeshRenderer>().material = Instantiate(t.font.material);
        if (shadow)
        {
            TextMeshShadow tms=TextMeshShadow.SetShadow(t, new Vector3(1, -1, 1) * 0.1f, Color.black);
            tms.gameObject.layer = textObject.layer;
        }
        HealthText ht = textObject.AddComponent<HealthText>();
        ht.lastPosition = transform.position;
        ht.mother = this;
        healthTexts.Add(ht);
    }

    private void ClampHealth()
    {
        maxHealth = Mathf.Max(maxHealth, 0);
        minHealth = Mathf.Clamp(minHealth, 0, maxHealth);
        health = Mathf.Clamp(health, 0, maxHealth);
    }

    private void ChangeHealth(float amount)
    {
        health += amount;
        health = Mathf.Clamp(health, 0, maxHealth);
        recoverableHealth = Mathf.Clamp(recoverableHealth, 0, maxHealth - health);

        if (amount > 0)
        {
            lastHealed = Time.time;
            if(FullHealth)
            {
                lastFull = Time.time;
            }
        }
        else if (amount < 0)
        {
            lastDamaged = Time.time;
        }
    }

    public void PissTake()
    {
        ChangeHealth(0);
        CreateText("-0", Color.yellow, true);
    }

    public bool Increase(float amount, bool pierce)
    {
        if (!IsImmune)
        {
            ChangeHealth(amount);
            CreateText("+" + amount, Color.white, true);
            return true;
        }
        return false;
    }

    public bool IncreaseThroughRecovery(float amount, bool pierce)
    {
        recoverableHealth -= amount;
        if(shotRecoveryPool>0)
        {
            shotRecoveryPool -= amount;
        }
        else
        {
            shotRecoveryPool = 0;
        }
        return Increase(amount, pierce);
    }

    public bool Decrease(float amount, bool slashRecovery, bool pierce)
    {
        if (pierce || !IsInvincible)
        {
            if (slashRecovery)
            {
                recoverableHealth = 0;
            }
            ChangeHealth(-amount);
            CreateText("-" + amount, CorporateDesign.Instance.red, true);
            return true;
        }
        return false;
    }

    public bool RecoverableDecrease(float amount, bool slashRecovery, bool pierce)
    {
        bool retVal = Decrease(amount, slashRecovery, pierce);
        if (pierce || !IsInvincible)
        {
            recoverableHealth += amount;
        }
        return retVal;
    }

    public void ResetRecovery()
    {
        lastRecovery = Time.time;
    }

    public void SetImmuneFor(float time)
    {
        immuneUntil = Time.time + time;
        immunityTime = time;
    }

    public void SetInvincibleFor(float time)
    {
        invincibleUntil = Time.time + time;
        invincibilityTime = time;
    }

    public bool IsInvincible
    {
        get
        {
            return Time.time < invincibleUntil;
        }
    }

    public bool IsImmune
    {
        get
        {
            return Time.time < immuneUntil;
        }
    }

    public bool CanRecover
    {
        get
        {
            return Time.time >= lastRecovery + recoveryInterval;
        }
    }

    public float TimeSinceDamaged
    {
        get
        {
            return lastDamaged - Time.time;
        }
    }

    public float TimeSinceHealed
    {
        get
        {
            return lastHealed - Time.time;
        }
    }

    public float TimeSinceFull
    {
        get
        {
            return Time.time-lastFull;
        }
    }

    public float TimeUntilInvincibilityEnds
    {
        get
        {
            return Mathf.Max(invincibleUntil - Time.time, 0);
        }
    }

    public float TimeUntilImmunityEnds
    {
        get
        {
            return Mathf.Max(immuneUntil - Time.time, 0);
        }
    }

    public float RelativeInvincibility
    {
        get
        {
            return 1f - Mathf.Min((Time.time - lastDamaged) / invincibilityTime, 1f);
        }
    }

    public float RelativeImmunity
    {
        get
        {
            return 1f - Mathf.Min((Time.time - lastHealed) / immunityTime, 1f);
        }
    }

    void Update()
    {
        if (CanRecover && !IsImmune && !IsInvincible)
        {
            ResetRecovery();
            if ((recoverableHealth > 0 || health<minHealth) && controller.IsAlive)
            {
                IncreaseThroughRecovery(naturalRecovery, true);
            }
        }
        ClampHealth();

        if(energyBarContainer)
        {
            energyBarContainer.transform.position = GameManager.Instance.PixelPerfect(controller.Top);

            if(FullHealth && TimeSinceFull > showSmallEnergyBarFor)
            {
                energyBarContainer.transform.localScale = Vector3.zero;
            }
            else
            {
                energyBarContainer.transform.localScale = regularEnergyBarScale;

                if (energyBar)
                {
                    energyBar.transform.localScale = new Vector3(RelativeHealth, 1, 1);
                    energyBar.transform.localPosition = new Vector3(-(1f - RelativeHealth) / 2f, 0, energyBar.transform.localPosition.z);
                    if (recoveryBar)
                    {
                        float value = RelativeRecoverable;
                        if (NoHealth)
                        {
                            value = 0;
                        }
                        recoveryBar.transform.localScale = new Vector3(value, 1, 1);
                        recoveryBar.transform.localPosition = new Vector3((-1f + RelativeRecoverable) / 2f + RelativeHealth, 0, recoveryBar.transform.localPosition.z);

                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        if(energyBarContainer)
        {
            Destroy(energyBarContainer);
        }
    }
}
