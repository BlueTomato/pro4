﻿using UnityEngine;
using System.Collections;

public class HealthText : MonoBehaviour
{
    public Vector3 originalScale;
    public float offset=0;
    public HealthText lastOne;
    public Vector2 startoffset;
    public Vector3 lastPosition;
    public float time = 0;
    public Health mother;
    public Vector3 lastStickingPoint;
    private float siblingOffset;

    void Start ()
    {
        originalScale = transform.localScale;
        transform.localScale = Vector3.zero;
    }
	
	void Update ()
    {
        float sineTime = time / Mathf.PI;

        if(mother)
        {
            int index = mother.healthTexts.IndexOf(this);

            siblingOffset = 0;
            for (int i=index+1;i<mother.healthTexts.Count;i++)
            {
                siblingOffset+=mother.healthTexts[i].offset;
            }
            siblingOffset *= 0.6f;
        }

        if (sineTime <=0.5f || sineTime > 2.5)
        {
            offset += Time.deltaTime * 3f;
        }


        if (mother)
        {
            Controller c = mother.GetComponent<Controller>();

            if (c && c.BoxCollider.bounds.extents != Vector3.zero)
            {
                lastStickingPoint = c.Top;
            }
            lastPosition = lastStickingPoint+new Vector3(0,siblingOffset,0);
        }

        transform.position = lastPosition+new Vector3(0,offset,0);

        if (sineTime <= 0.5f)
        {
            transform.localScale = new Vector3(originalScale.x, originalScale.y * Mathf.Sin(time), originalScale.z);
        }
        else if (sineTime > 2.5f && sineTime <= 3)
        {
            transform.localScale = originalScale * Mathf.Sin(time);
        }
        else if (sineTime > 3)
        {
            if(mother)
            {
                mother.healthTexts.Remove(this);
            }
            Destroy(gameObject);
        }
        time += Time.deltaTime * 5f;
    }
}
