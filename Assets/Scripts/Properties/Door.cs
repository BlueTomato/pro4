﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : UniqueName<Door>
{
    private Vector3 startPosition;
    public bool open = false;
    public Switch[] switches;
    public bool closeAgain;
    public float openTime = 1;
    private float openEnd;
    private float closeEnd;
    public Vector3 movement;

    public static SortedDictionary<string, bool> doorStates = new SortedDictionary<string, bool>();
    public static SortedDictionary<string, float> openEnds = new SortedDictionary<string, float>();
    public static SortedDictionary<string, float> closeEnds = new SortedDictionary<string, float>();

    public override void Start()
    {
        base.Start();

        if (open)
        {
            openEnd = Time.time;
            closeEnd = openEnd - openTime;
        }
        else
        {
            closeEnd = Time.time;
            openEnd = closeEnd - openTime;
        }
        startPosition = transform.localPosition;

        if (!doorStates.ContainsKey(Name))
        {
            doorStates.Add(Name, open);
            openEnds.Add(Name, openEnd);
            closeEnds.Add(Name, closeEnd);
        }
        else
        {
            open = doorStates[Name];
            openEnd = openEnds[Name];
            closeEnd = closeEnds[Name];
        }
    }

    void OnDestroy()
    {
        doorStates[Name] = open;
        openEnds[Name] = openEnd;
        closeEnds[Name] = closeEnd;
    }

    private bool SwitchesSolved
    {
        get
        {
            bool switchesSolved = true;

            for (int i = 0; i < switches.Length; i++)
            {
                switchesSolved &= switches[i].IsSolved;
            }

            return switchesSolved;
        }
    }

    void Update()
    {
        if (open)
        {
            if (openEnd <= closeEnd)
            {
                openEnd = Time.time + openTime - Mathf.Max(closeEnd - Time.time, 0);
                GameManager.Instance.StartEarthquake(openTime * 1.5f, 2, transform.position, 15);
            }

            if (closeAgain && !SwitchesSolved)
            {
                open = false;
            }
        }
        else
        {
            if (closeEnd < openEnd)
            {
                closeEnd = Time.time + openTime - Mathf.Max(openEnd - Time.time, 0);
                GameManager.Instance.StartEarthquake(openTime * 1.5f, 2, transform.position, 15);
            }

            if (SwitchesSolved)
            {
                open = true;
            }
        }

        float end = openEnd;
        float start = openTime;
        int direction = 1;

        if (closeEnd >= openEnd)
        {
            end = closeEnd;
            direction = -1;
            start = 0;
        }

        transform.localPosition = startPosition + (start + (-Mathf.Clamp(end - Time.time, 0, openTime) * direction)) / openTime * movement;
    }
}
