﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class UniqueName<T> : MonoBehaviour where T : MonoBehaviour
{
	public virtual void Start ()
    {
        T[] ofSameType = GameObject.FindObjectsOfType<T>();

        for (int i = 0; i < ofSameType.Length; i++)
        {
            if (ofSameType[i] != this && ofSameType[i].name == gameObject.name)
            {
                gameObject.name += " (1)";
            }
        }
    }

    public string Name
    {
        get
        {
            return SceneManager.GetActiveScene() + " " + name;
        }
    }
}
