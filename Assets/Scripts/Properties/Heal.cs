﻿using UnityEngine;
using System.Collections;
using System;

public class Heal : ManipulateHealth
{
    protected override void Action(Controller controller)
    {
        controller.health.Increase(healthImpact,pierceHealthImpact);
    }

    protected override void Burst()
    {
    }
}