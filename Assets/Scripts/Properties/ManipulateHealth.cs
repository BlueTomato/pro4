﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ManipulateHealth : MonoBehaviour
{
    public bool active = true;

    public float healthImpact;
    public float recoveryTime;

    public bool pierceHealthImpact = false;

    public float pushBack;
    public float pushBackTime;

    public bool piercePushBack = false;

    public List<string> targets = new List<string>();
    public List<string> pushBackTargets = new List<string>();
    public List<string> ignore = new List<string>();
    public bool destroyOnCollision = false;
    public bool destroyOnTargetCollision = false;

    public bool dragAlong = false;
    protected bool dragObligation;
    protected Vector2 moveVector;
    public bool restrictPushAngle = false;
    public bool RestrictCounterClockwise = false;
    public bool RestrictToValues = false;
    public float restrictPushAngleA = -90;
    public float restrictPushAngleB = 90;

    private Controller controller;
    protected Collider2D col2D;

    public static bool pushOrder;

    public virtual void Start()
    {
        ignore.Add("Ignore");
        ignore.Add("Through");

        col2D = GetComponent<Collider2D>();
        if (!col2D)
        {
            col2D = gameObject.AddComponent<BoxCollider2D>();
        }
        col2D.isTrigger = true;

        controller = GetComponent<Controller>();
        if (!controller)
        {
            controller = GetComponentInParent<Controller>();
        }
    }

    public Collider2D Collider
    {
        get
        {
            return col2D;
        }
    }

    public void SetActive(bool value)
    {
        active = value;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        CollisionCheck(other);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        CollisionCheck(other);
    }

    public virtual void CollisionCheck(Collider2D other)
    {
        if (active)
        {
            string layerName = LayerMask.LayerToName(other.gameObject.layer);
            bool isHealthAffected = targets.Contains(other.tag);
            bool isPushed = pushBackTargets.Contains(other.tag);

            if (!isHealthAffected || !isPushed)
            {
                isHealthAffected = isHealthAffected || targets.Contains(layerName);
                isPushed = isPushed || pushBackTargets.Contains(layerName);
            }
            bool toBeIgnored = ignore.Contains(other.tag);

            isHealthAffected &= !toBeIgnored;
            isPushed &= !toBeIgnored;

            isHealthAffected &= healthImpact != 0;
            isPushed &= pushBack != 0;

            Controller otherC = other.GetComponent<Controller>();

            if (!otherC)
            {
                otherC = other.GetComponentInParent<Controller>();
                if (!otherC)
                {
                    otherC = other.GetComponentInChildren<Controller>();
                }
            }

            if (otherC && otherC.IsAlive)
            {
                if (isHealthAffected && (pierceHealthImpact || (otherC.health && !otherC.health.IsInvincible)))
                {
                    Action(otherC);
                }
                if (isPushed && (piercePushBack || !otherC.IsKnockedBack))
                {
                    if (otherC.TimeSinceKnockBack >= 0.5f && (!isHealthAffected || healthImpact == 0))
                    {
                        otherC.health.PissTake();
                    }

                    Vector2 pos = transform.position;
                    Vector2 otherPos = other.transform.position;

                    if (controller)
                    {
                        pos = controller.Center;
                    }

                    otherPos = otherC.Center;

                    if (pos == otherPos)
                    {
                        if (pushOrder)
                        {
                            pos.x += 1;
                        }
                        else
                        {
                            otherPos.x += 1;
                        }
                        pushOrder = !pushOrder;
                    }

                    Vector2 normalizedVector = (otherPos - pos).normalized;
                    float angle = 0;

                    if (!dragAlong && !dragObligation)
                    {
                        angle = Globals.VectorToAngle(normalizedVector);

                        if (restrictPushAngle)
                        {
                            if (RestrictToValues || !RestrictCounterClockwise)
                            {
                                angle = Globals.RestrictAngleWithin(angle, restrictPushAngleA, restrictPushAngleB);
                            }
                            if (RestrictToValues || RestrictCounterClockwise)
                            {
                                angle = Globals.RestrictAngleOutsideOf(angle, restrictPushAngleA, restrictPushAngleB);
                            }
                        }
                    }
                    else
                    {
                        angle = Globals.VectorToAngle(moveVector.normalized);
                    }

                    otherC.ContinuousKnockBack(angle, pushBackTime, pushBack);
                }
            }
            if (destroyOnTargetCollision && (isPushed || isHealthAffected))
            {
                Destroy(gameObject);
            }
        }
    }

    protected abstract void Action(Controller controller);

    protected abstract void Burst();
}
