﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAbility : Heal
{
    public Ability ability;

    public override void Start()
    {

        if (ability && !Ability.GetEnabledList().ContainsValue(ability))
        {
            base.Start();
            destroyOnTargetCollision = true;
            healthImpact = 100;
            SpriteRenderer sr = new GameObject(ability.name + " renderer").AddComponent<SpriteRenderer>();
            sr.sprite = ability.image;
            sr.transform.SetParent(transform);
            sr.transform.localScale = Vector3.one * 0.3f;
            sr.transform.localPosition = new Vector3(0, 0, -0.5f);
            sr.gameObject.layer = gameObject.layer;
            targets.Add("Player");
        }
        else
        {
            Destroy(gameObject);
        }
    }

    protected override void Action(Controller controller)
    {
        controller.health.Increase(healthImpact, pierceHealthImpact);
        ability.enabled = true;
        AbilityManager.Instance.SetAbilities();
        GameManager.Instance.SuspendGameFor(5);
        AbilityManager.Instance.JumpTo(ability);
        ability.Activate();
    }

    protected override void Burst()
    {
    }
}
