﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System;

public class Projectile : Damage
{
    public ParticleSystem explosion;
    public ParticleSystem trail;
    private Rigidbody2D rigidBody;

    public float cost;

    private int shotNumber;

    public float targetAngle;
    public float angleToTarget;
    public float angle;

    private float targetSpeed = 15;
    private float rotationDegreePerSeconds = 360;
    private float speed;
    private int speedDirection = 1;

    private bool bounceMode;
    private bool phantomMode;
    private bool boomerangMode;
    private bool magneticMode;
    private bool targetMode;
    private bool linkedMode;
    private bool spongeMode;
    private bool pushMode;

    private bool phantom;
    private float lastGhostEnd;
    private float lastGhostStart;
    private bool boomerang;
    private bool homing;
    private bool sponge;

    private bool target;

    private float ghostTransitionTime = 0.5f;

    private GameObject targetSphere;
    private float targetSphereScale;
    private Vector2 normalAhead;
    public PlayerInput character;
    public Vector2 position;
    public List<String> targetedTags = new List<String>();
    private float targetRange = 8f;
    private float lastBounce = -1f;
    private List<GameObject> hasEntered = new List<GameObject>();
    private List<float> enterTimes = new List<float>();
    private int bounces;
    private int maxBounces = 6;

    private float radius = 1.5f;

    private bool lockedIn;

    public GameObject sprite;
    public GameObject boom;
    public GameObject magn;

    public override void Start()
    {
        base.Start();

        shotNumber = character.numberOfShots;
        character.numberOfShots++;
        if(linkedMode)
        {
            character.numberOfLinkedShots++;
        }

        character.controller.health.shotRecoveryPool += cost;

        rigidBody = gameObject.AddComponent<Rigidbody2D>();
        rigidBody.gravityScale = 0;

        angle = targetAngle;
        angleToTarget = targetAngle;
        speed = targetSpeed;

        ActivatePhantom(phantomMode);

        if (targetMode)
        {
            targetSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            targetSphere.transform.SetParent(transform);
            Renderer renderer = targetSphere.GetComponent<Renderer>();
            renderer.material.shader = CorporateDesign.Instance.spriteShader;
            renderer.material.color = new Color(0.8f, 0, 0, 0.05f);
            renderer.receiveShadows = false;
            renderer.shadowCastingMode = ShadowCastingMode.Off;
            targetSphere.transform.localPosition = Vector3.zero;
            targetSphere.transform.localScale = new Vector3(1, 1, 0) * targetSphereScale * 2;
        }

        if(pushMode)
        {
            piercePushBack = true;
        }

        SpriteRenderer boomerangSprite = boom.GetComponent<SpriteRenderer>();

        boomerangSprite.enabled = boomerangMode;


        SpriteRenderer magnetSprite = magn.GetComponent<SpriteRenderer>();

        magnetSprite.enabled = magneticMode;
    }

    public void AddTarget(string tag)
    {
        targetedTags.Add(tag);
    }

    public void SetBounce(bool value)
    {
        bounceMode = value;
    }
    public void SetPhantom(bool value)
    {
        phantomMode = value;
    }
    public void SetSponge(bool value)
    {
        spongeMode = value;
    }
    public void SetPush(bool value)
    {
        pushMode = value;
    }
    public void SetBoomerang(bool value)
    {
        boomerangMode = value;
    }
    public void SetMagnet(bool value)
    {
        magneticMode = value;
    }
    public void SetTarget(bool value)
    {
        targetMode = value;
    }
    public void SetLink(bool value)
    {
        linkedMode = value;
    }
    public void SetTargetSpeed(float value)
    {
        targetSpeed = value;
    }

    public void ActivatePhantom(bool value)
    {
        if (phantomMode && value != phantom)
        {
            if (value)
            {
                SetActive(false);
                lastGhostStart = Time.time;
                phantom = true;
            }
            else
            {
                SetActive(true);
                lastGhostEnd = Time.time;
                phantom = false;
            }
        }
    }

    public void ActivateSponge(bool value)
    {
        if(spongeMode)
        {
            sponge = value;
        }
    }

    public void ActivateBoomerang(bool value)
    {
        if (boomerangMode && value != boomerang)
        {
            if (value)
            {
                boomerang = true;
                rotationDegreePerSeconds *= 4f;
                speedDirection = -1;
            }
            else
            {
                boomerang = false;
                rotationDegreePerSeconds /= 4f;
                speedDirection = 1;
            }
        }
    }

    public void ActivateHoming(bool value)
    {
        if (targetMode && value != homing)
        {
            if (value)
            {
                homing = true;
            }
            else
            {
                homing = false;
            }
        }
    }

    private void RetainTrail()
    {
        ParticleSystem.MainModule mm = trail.main;
        mm.loop = false;
        ParticleSystem.EmissionModule emitter = trail.emission;
        trail.transform.SetParent(null);
        trail.gameObject.AddComponent<ParticleSystemAutoDestroy>();
    }

    protected override void Burst()
    {
        if (explosion)
        {
            GameObject particles = Instantiate(explosion.gameObject);
            ParticleSystem system = particles.GetComponent<ParticleSystem>();
            ParticleSystem.MainModule mm = system.main;
            mm.loop = false;
            system.transform.position = transform.position;
            system.Emit(20);
            particles.AddComponent<ParticleSystemAutoDestroy>();
            RetainTrail();
        }
        Destroy(gameObject);
    }

    private float RotationDegreeInSeconds
    {
        get
        {
            return rotationDegreePerSeconds * Mathf.Min(Time.time - lastBounce, 1f);
        }
    }

    private float RelativeSpeed
    {
        get
        {
            return speed/(targetSpeed*speedDirection);
        }
    }

    public override void CollisionCheck(Collider2D other)
    {
        base.CollisionCheck(other);

        if (active)
        {
            if(other.transform==character.transform && sponge)
            {
                character.controller.health.IncreaseThroughRecovery((int)Mathf.Clamp(cost,0,character.controller.health.shotRecoveryPool), true);
                Destroy(gameObject);
            }
            else if(!other.GetComponent<Projectile>() && other.gameObject.layer != LayerMask.NameToLayer("Player") && !ignore.Contains(other.tag) && other.tag != "Phantomphobic")
            {
                if (bounceMode)
                {
                    if (bounces < maxBounces && hasEntered.Count<1 && !hasEntered.Contains(other.gameObject))
                    {
                        lastBounce = Time.time;
                        bounces++;
                        Vector2 reflection = Vector2.Reflect(Quaternion.AngleAxis(angle, Vector3.back) * Vector3.up, normalAhead);
                        Vector3 cross = Vector3.Cross(Vector2.up, reflection);
                        float newAngle = Vector2.Angle(Vector2.up, reflection);
                        if (cross.z > 0)
                        {
                            newAngle = -newAngle;
                        }
                        ChangeAngle(newAngle);
                    }
                    else if (bounces >= maxBounces)
                    {
                        Destroy(gameObject);
                    }
                }
                else
                {
                    bool isTargeted= targets.Contains(other.tag) || targets.Contains(LayerMask.LayerToName(other.gameObject.layer));
                    if ((destroyOnCollision && !isTargeted) || (destroyOnTargetCollision && isTargeted))
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }
        else if(phantom && other.tag=="Phantomphobic")
        {
            Destroy(gameObject);
        }

        if (hasEntered.Contains(other.gameObject))
        {
            int index = hasEntered.IndexOf(other.gameObject);
            if (Time.time - enterTimes[index] >= 0f && !phantomMode)
            {
                Destroy(gameObject);
            }
        }
        else if ((phantomMode || bounceMode) && other.tag!="Phantomphobic" && other.gameObject.layer != LayerMask.NameToLayer("Player") && !ignore.Contains(other.tag))
        {
            hasEntered.Add(other.gameObject);
            enterTimes.Add(Time.time);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if(hasEntered.Contains(other.gameObject))
        {
            int indexToRemove = hasEntered.IndexOf(other.gameObject);
            hasEntered.RemoveAt(indexToRemove);
            enterTimes.RemoveAt(indexToRemove);
        }
    }

    public void FindTarget()
    {
        if (targetMode && homing)
        {
            List<GameObject> targetGameObjects = new List<GameObject>();
            for (int i = 0; i < targetedTags.Count; i++)
            {
                GameObject[] tagObject = GameObject.FindGameObjectsWithTag(targetedTags[i]);
                for (int j = 0; j < tagObject.Length; j++)
                {
                    targetGameObjects.Add(tagObject[j]);
                }
            }

            Collider2D[] colliders = Collider2D.FindObjectsOfType<Collider2D>();
            for(int i=0;i<colliders.Length;i++)
            {
                if(targetedTags.Contains(LayerMask.LayerToName(colliders[i].gameObject.layer)))
                {
                    targetGameObjects.Add(colliders[i].gameObject);
                }
            }

            float minDistance = targetSphereScale;
            for (int i = 0; i < targetGameObjects.Count; i++)
            {
                Transform t = targetGameObjects[i].transform;

                float distance = Vector2.Distance(t.position, transform.position);
                if (distance <= minDistance)
                {
                    minDistance = distance;
                    position = t.position;
                    target = true;
                }
            }
        }
    }

    public void ChangeAngle(float newAngle)
    {
        newAngle = Mathf.Repeat(newAngle, 360f);

        angle = newAngle;
        targetAngle = angle;
        angleToTarget = angle;
    }

    public void FindNormal()
    {
        if (bounceMode)
        {
            Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.back);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, rotation * Vector3.up * speedDirection, float.MaxValue);
            RaycastHit2D hitTop = Physics2D.Raycast(transform.position + (rotation * new Vector3(-col2D.bounds.extents.x, 0)), rotation * Vector3.up * speedDirection, float.MaxValue);
            RaycastHit2D hitBottom = Physics2D.Raycast(transform.position + (rotation * new Vector3(col2D.bounds.extents.x, 0)), rotation * Vector3.up * speedDirection, float.MaxValue);

            if (!hit.transform || (hitTop.transform && hitTop.distance < hit.distance))
            {
                hit = hitTop;
            }
            if (!hit.transform || (hitBottom.transform && hitBottom.distance < hit.distance))
            {
                hit = hitBottom;
            }
            if (hit.transform)
            {
                normalAhead = hit.normal;
            }
        }
    }


    private void AdjustSpeed()
    {
        float speedToTarget = targetSpeed * speedDirection;
        if (speed != speedToTarget)
        {
            float direction = 50f * Time.deltaTime * speedDirection;

            if ((speed < speedToTarget && speed - direction > speedToTarget) || (speed > speedToTarget && speed - direction < speedToTarget))
            {
                speed = speedToTarget;
            }
            else
            {
                speed += direction;
            }
        }
        pushBack = Mathf.Abs(speed);
    }

    private void TargetAngle()
    {
        if (!target && magneticMode)
        {
            if (!lockedIn && (!boomerang || Vector2.Distance(character.arm.transform.position, transform.position) > 1f))
            {
                float modifyer = Mathf.Clamp(speed, -1f, 1f);
                float distance = Vector2.Distance(character.arm.transform.position, transform.position) + 1f * modifyer;
                position = new Vector3(distance * Mathf.Sin(targetAngle * Mathf.Deg2Rad), distance * Mathf.Cos(targetAngle * Mathf.Deg2Rad)) + character.arm.transform.position;
            }
            else
            {
                float seed = Time.time * targetSpeed + ((2f * Mathf.PI) / character.numberOfShots * shotNumber);
                position = character.controller.Center + new Vector3(Mathf.Sin(seed)*radius, 1f + Mathf.Cos(seed) * radius, character.transform.position.z);
                if (lockedIn || Vector2.Distance(position, transform.position) < 0.75f)
                {
                    lockedIn = true;
                    float positioningSeed = seed - 0.1f;
                    Vector3 newPosition = character.controller.Center + new Vector3(Mathf.Sin(positioningSeed) * radius, 1f + Mathf.Cos(positioningSeed) * radius, character.transform.position.z);
                    moveVector = newPosition - transform.position;
                    transform.position = newPosition;
                }
            }
        }
        if (magneticMode || target)
        {
            angleToTarget = Mathf.Atan2(position.x - transform.position.x, position.y - transform.position.y) * Mathf.Rad2Deg;
        }

        if (angle != angleToTarget)
        {
            float deltaAngle;
            if (speed >= 0)
            {
                deltaAngle = Mathf.DeltaAngle(angle, angleToTarget);
            }
            else
            {
                deltaAngle = Mathf.DeltaAngle(angle, angleToTarget + 180);
            }

            float direction = Mathf.Sign(deltaAngle) * RotationDegreeInSeconds * Time.deltaTime;

            if (Mathf.Sign(deltaAngle) > 0)
            {
                direction = Mathf.Min(deltaAngle, direction);
            }
            else
            {
                direction = Mathf.Max(deltaAngle, direction);
            }

            angle += direction;
        }
    }

    private void AdjustSize()
    {
        if (boomerangMode)
        {
            boom.transform.rotation = Quaternion.Euler(0, 0, (transform.position.x + transform.position.y) * 200f);
        }

        if (magneticMode)
        {
            Vector3 initialPosition = character.arm.transform.position;
            if (lockedIn)
            {
                initialPosition = character.controller.Center+new Vector3(0,1,0);
            }
            magn.transform.position = (transform.position + initialPosition) / 2f;
            magn.transform.localScale = new Vector3(Vector2.Distance(transform.position, initialPosition) * 2f, 1f, 1f);
            magn.transform.rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan2(transform.position.y - initialPosition.y, transform.position.x - initialPosition.x));
        }

        sprite.transform.localScale = Vector3.one;
        if (targetMode)
        {
            float delta = 5f * targetRange * Time.deltaTime;
            if (homing)
            {
                targetSphereScale += delta;
            }
            else
            {
                targetSphereScale -= delta;
            }
            targetSphereScale = Mathf.Clamp(targetSphereScale, 0, targetRange);
            targetSphere.transform.localScale = new Vector3(1, 1, 0) * targetSphereScale * 2f;
        }
        if (phantomMode)
        {
            float ghostSize;
            if (lastGhostEnd > lastGhostStart)
            {
                ghostSize = Time.time - lastGhostEnd;
                ghostSize /= ghostTransitionTime;
            }
            else
            {
                ghostSize = lastGhostStart + ghostTransitionTime - Time.time;
            }

            sprite.transform.localScale *= Mathf.Clamp(ghostSize, 0, 1);
        }
        if (bounceMode)
        {
            sprite.transform.localScale *= 0.5f + ((maxBounces - bounces) / (float)maxBounces) / 2f;
        }
    }


    private void DestroyIfOutside()
    {
        float distanceX = Mathf.Abs(transform.position.x - character.controller.Center.x);
        float distanceY = Mathf.Abs(transform.position.y - character.controller.Center.y);
        if (distanceX > GameManager.Instance.ScreenWidth || distanceY > GameManager.Instance.ScreenHeight)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, angle);
        AdjustSize();
        DestroyIfOutside();
    }

    void FixedUpdate()
    {
        moveVector = Vector2.zero;
        if(pushMode)
        {
            if (lockedIn || bounceMode || RelativeSpeed<0.95f)
            {

                dragObligation = false;
            }
            else
            {
                dragObligation = true;
            }
        }

        if (!character.controller.IsShooting)
        {
            ActivateBoomerang(true);
            ActivatePhantom(false);
            ActivateHoming(true);
            ActivateSponge(true);
        }
        else if (linkedMode)
        {
            lockedIn = false;
            ActivateBoomerang(false);
            ActivatePhantom(true);
            ActivateHoming(false);
            ActivateSponge(false);
        }

        FindNormal();
        FindTarget();
        AdjustSpeed();
        TargetAngle();

        if (!lockedIn || target)
        {
            moveVector = new Vector2(Mathf.Sin(angle * Mathf.Deg2Rad) * speed * Time.deltaTime, Mathf.Cos(angle * Mathf.Deg2Rad) * speed * Time.deltaTime);
            rigidBody.MovePosition(transform.position+(Vector3)moveVector);
        }
        target = false;
    }

    public void OnDestroy()
    {
        Burst();
        character.numberOfShots--;
        if (linkedMode)
        {
            character.numberOfLinkedShots--;
        }
    }
}
