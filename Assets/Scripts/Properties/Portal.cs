﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(BoxCollider2D))]
public class Portal : MonoBehaviour
{
    private SpriteRenderer sprite;
    public SceneAsset transport;
    public Vector3 enteredFrom;
    public BoxCollider2D col;

    public static SceneAsset oldScene;
    public static SceneAsset newScene;

    public static Vector3 oldPortalPosition=Vector3.zero;
    public static bool changingScene;

    void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.enabled = false;
        col = GetComponent<BoxCollider2D>();
        col.isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other == GameManager.Instance.player.controller.BoxCollider)
        {
            enteredFrom = other.transform.position;
            OnTriggerStay2D(other);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other == GameManager.Instance.player.controller.BoxCollider)
        {
            Vector3 o = other.transform.position;
            Vector3 p = transform.position;
            bool case1 = (enteredFrom.x > p.x && o.x < p.x);
            bool case2 = (enteredFrom.x < p.x && o.x > p.x);
            if (case1 || case2)
            {
                ChangeScene(transport);
            }
        }
    }


    public void ChangeScene(SceneAsset newScene)
    {
        if(!changingScene)
        {
            changingScene = true;
            oldPortalPosition = transform.position;
            string activeScene = SceneManager.GetActiveScene().name;
            if (!oldScene && SceneAsset.List.ContainsKey(activeScene))
            {
                oldScene = SceneAsset.List[activeScene];
            }
            SceneManager.sceneLoaded += PlaceAtPortal;
            GameManager.Instance.LoadSceneWithTransition(newScene);
        }
    }

    public void PlaceAtPortal(Scene scene, LoadSceneMode mode)
    {
        Portal[] portals = Portal.FindObjectsOfType<Portal>();

        if(portals.Length>1)
        {
            Vector3 difference = new Vector3();

            for (int i = 0; i < portals.Length; i++)
            {
                if (portals[i].transport && portals[i].transport.Equals(oldScene))
                {
                    difference = portals[i].transform.position - oldPortalPosition;
                }
            }
            GameManager.Instance.player.transform.position += difference;
        }

        oldScene = newScene;

        changingScene = false;
        SceneManager.sceneLoaded -= PlaceAtPortal;
    }
}