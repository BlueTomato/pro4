﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{
    public float time = 0;
    public bool sticky = false;
    public float moveX = 0;
    public float moveY = 0;
    public bool cosX = false;
    public bool cosY = false;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(moveX, 0);
    }

    void Update()
    {
        float velocityX = rb.velocity.x;
        float velocityY = rb.velocity.y;

        if (moveX != 0)
        {
            if (!cosX)
            {
                velocityX = moveX * Mathf.Sin(time);
            }
            else
            {
                velocityX = moveX * Mathf.Cos(time);
            }
        }
        if (moveY != 0)
        {
            if (!cosY)
            {
                velocityY = moveY * Mathf.Sin(time);
            }
            else
            {
                velocityY = moveY * Mathf.Cos(time);
            }
        }

        rb.velocity=new Vector2(velocityX, velocityY);

        time += Time.deltaTime;
    }
}


/*
using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{
    private Vector2 origin;
    public float time = 0;
    public bool sticky = false;
    public float moveX = 0;
    public float moveY = 0;
    public bool cosX = false;
    public bool cosY = false;
    private Rigidbody2D rb;
    public Vector3 velocity;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        origin = transform.position; ;
        StartCoroutine(CalcVelocity());
    }

    void FixedUpdate()
    {
        float newPositionX = 0;
        float newPositionY = 0;

        if (moveX != 0)
        {
            if (!cosX)
            {
                newPositionX = moveX * Mathf.Sin(time);
            }
            else
            {
                newPositionX = moveX * Mathf.Cos(time);
            }
        }
        if (moveY != 0)
        {
            if (!cosY)
            {
                newPositionY = moveY * Mathf.Sin(time);
            }
            else
            {
                newPositionY = moveY * Mathf.Cos(time);
            }
        }

        rb.MovePosition(origin+new Vector2(newPositionX, newPositionY));

        time += Time.deltaTime;
    }


    IEnumerator CalcVelocity()
    {
        while (Application.isPlaying)
        {
            Vector3 prevPos = transform.position;
            yield return new WaitForEndOfFrame();
            Vector3 currVel = (prevPos - transform.position) / Time.deltaTime;
            velocity=-currVel;
        }
    }
}
*/