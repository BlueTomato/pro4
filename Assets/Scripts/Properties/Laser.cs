﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    public GameObject laser;
    public GameObject pointA;
    public GameObject pointB;
    public float reach;

	// Use this for initialization
	void Start ()
    {
        laser = gameObject;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(pointA && pointB)
        {
            Vector3 pointC = (pointA.transform.position + pointB.transform.position) / 2f;
            transform.position = Vector3.Lerp(pointA.transform.position, pointC, reach);
            transform.localScale = new Vector3(Vector2.Distance(pointA.transform.position, pointB.transform.position) * reach, 1f, 1f);
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Mathf.Atan2(pointA.transform.position.y - pointB.transform.position.y, pointA.transform.position.x - pointB.transform.position.x));
        }
    }
}
