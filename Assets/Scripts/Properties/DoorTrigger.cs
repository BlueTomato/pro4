﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public Door door;
    public bool open;
    public bool continuous;
    public bool reverseOnExit;
    public List<string> tags = new List<string>();

    void OnTriggerEnter2D(Collider2D other)
    {
        if(tags.Count<=0 || tags.Contains(other.tag))
        {
            door.open = open;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (continuous)
        {
            OnTriggerEnter2D(other);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (reverseOnExit && (tags.Count <= 0 || tags.Contains(other.tag)))
        {
            door.open = !open;
        }
    }
}
