﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CameraArea : MonoBehaviour {

    public Vector2 pointA;
    public Vector2 pointB;
    public GameObject clipA;
    public GameObject clipB;

    public bool makeCameraSized;
    public bool centerObject;

    public MaterialProperties materialProperties;

    void Start ()
    {
        SetPoints();
	}

    void OnDrawGizmos()
    {
        SetPoints();
        Gizmos.color = new Color(1f, 0.1f, 0.1f, 0.25f);
        Gizmos.DrawCube((pointA+(pointB-pointA)/2f),pointA-pointB);
        Gizmos.color = new Color(1f, 0.1f, 0.1f, 1f);
        DrawRect(pointA,pointB);

        if(makeCameraSized)
        {
            float x = GameManager.Instance.ScreenWidth / 2f;
            float y = GameManager.Instance.ScreenHeight / 2f;
            clipA.transform.localPosition = new Vector3(-x,y,clipA.transform.position.z);
            clipB.transform.localPosition = new Vector3(x, -y, clipB.transform.position.z);
            makeCameraSized = false;
        }

        if(centerObject)
        {
            Vector3 oldPosition = transform.position;
            transform.position = (pointA + pointB) / 2f;
            clipA.transform.position += oldPosition - transform.position;
            clipB.transform.position += oldPosition - transform.position;
            centerObject = false;
        }
    }

    void DrawRect(Vector2 pointA, Vector2 pointB)
    {
        Gizmos.DrawLine(pointA, new Vector2(pointB.x, pointA.y));
        Gizmos.DrawLine(pointA, new Vector2(pointA.x, pointB.y));
        Gizmos.DrawLine(pointB, new Vector2(pointA.x, pointB.y));
        Gizmos.DrawLine(pointB, new Vector2(pointB.x, pointA.y));
    }

    void SetPoints()
    {
        if(clipA==null && transform.childCount>0)
        {
            clipA = transform.GetChild(0).gameObject;

            if (clipB == null && transform.childCount > 1)
            {
                clipB = transform.GetChild(1).gameObject;
            }
        }

        if (clipA != null)
        {
            pointA = clipA.transform.position;
        }
        else
        {
            clipA = new GameObject("Clip A");
            clipA.transform.parent = transform;
        }

        if (clipB != null)
        {
            pointB = clipB.transform.position;
        }
        else
        {
            clipB = new GameObject("Clip B");
            clipB.transform.parent = transform;
        }
    }

    public Vector2 GetTarget(Vector3 playerPosition)
    {
        Vector3 position = playerPosition;

        float cameraHeight = Camera.main.orthographicSize * 2f;
        float cameraWidth = cameraHeight * Camera.main.aspect;

        Vector2 biggest = PointA - (new Vector2(cameraWidth, cameraHeight) / 2f);
        Vector2 smallest = PointB + (new Vector2(cameraWidth, cameraHeight) / 2f);

        position = SetX(smallest, biggest, position);
        position = SetY(smallest, biggest, position);

        position -= playerPosition;

        return position;
    }

    public void Update()
    {
        if (IsWithin(GameManager.Instance.cameraManager.playerPosition))
        {
            GameManager.Instance.cameraManager.Suppress(this);
        }
        if(materialProperties)
        {
            materialProperties.weight = Mathf.Max(materialProperties.weight, CameraManager.Instance.SuppressionWeight(this));
        }
    }


    Vector3 SetX(Vector2 smallest, Vector2 biggest, Vector3 position)
    {
            Vector3 camPosition = position;

            if(biggest.x<smallest.x)
            {
                position = new Vector3((smallest.x + biggest.x) / 2f, camPosition.y, camPosition.z);
            }
            else if (camPosition.x < smallest.x)
            {
                position = new Vector3(smallest.x, camPosition.y, camPosition.z);
            }
            else if (camPosition.x > biggest.x)
            {
                position = new Vector3(biggest.x, camPosition.y, camPosition.z);
            }

        return position;
    }

    Vector3 SetY(Vector2 smallest, Vector2 biggest, Vector3 position)
    {
        Vector3 camPosition = position;

        if (biggest.y<smallest.y)
        {
            position = new Vector3(camPosition.x, (smallest.y + biggest.y) / 2f, camPosition.z);
        }
        else if (camPosition.y < smallest.y)
        {
            position = new Vector3(camPosition.x, smallest.y, camPosition.z);
        }
        else if (camPosition.y > biggest.y)
        {
            position = new Vector3(camPosition.x, biggest.y, camPosition.z);
        }

        return position;
    }

    Vector2 PointA
    {
        get
        {
            return new Vector2(Mathf.Max(pointA.x, pointB.x), Mathf.Max(pointA.y, pointB.y));
        }
    }

    Vector2 PointB
    {
        get
        {
            return new Vector2(Mathf.Min(pointA.x, pointB.x), Mathf.Min(pointA.y, pointB.y));
        }
    }

    public bool IsWithin(Vector2 point)
    {
        Vector2 biggest = PointA;
        Vector2 smallest = PointB;

        if (point.x<=biggest.x && point.x>=smallest.x && point.y<=biggest.y && point.y>=smallest.y)
        {
            return true;
        }
        return false;
    }

    public class Comparer : IComparer<CameraArea>
    {
        public int Compare(CameraArea x, CameraArea y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            if (x.GetInstanceID() > y.GetInstanceID())
            {
                return 1;
            }
            return -1;
        }
    }
}
