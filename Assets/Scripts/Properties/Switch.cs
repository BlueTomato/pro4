﻿using UnityEngine;
using System.Collections.Generic;

public class Switch : UniqueName<Switch>
{
    public SpriteRenderer sphere;
    private Material originalMaterial;
    public Color on = new Color(0.5f,0.65f,1.0f);
    public Color off = new Color(1.0f, 0.5f, 0.5f);
    public bool active;
    private bool activated;
    private float lastActivation;
    public float lastsFor = 0;

    public bool onOff;

    private GameObject lastHit;

    public static SortedDictionary<string, bool> switchActive = new SortedDictionary<string, bool>();
    public static SortedDictionary<string, bool> switchActivated = new SortedDictionary<string, bool>();
    public static SortedDictionary<string, float> lastActivations = new SortedDictionary<string, float>();

    public override void Start()
    {
        base.Start();

        if (!active)
        {
            lastActivation -= lastsFor;
        }
        else
        {
            lastActivation = Time.time;
        }
        if (!sphere)
        {
            sphere = GetComponentInChildren<SpriteRenderer>();

        }
        if (sphere)
        {
            originalMaterial = sphere.sharedMaterial;
        }
        sphere.gameObject.transform.rotation = Quaternion.Euler(0,0,0);

        if (!switchActive.ContainsKey(Name))
        {
            switchActive.Add(Name, active);
            switchActivated.Add(Name, activated);
            lastActivations.Add(Name, lastActivation);
        }
        else
        {
            active = switchActive[Name];
            activated = switchActivated[Name];
            lastActivation = lastActivations[Name];
        }

        if(lastsFor>0)
        {
            TextMesh t = new GameObject().AddComponent<TextMesh>();
            t.text = lastsFor.ToString();
            t.font = CorporateDesign.Instance.mainFont;
            t.anchor = TextAnchor.MiddleCenter;
            t.transform.SetParent(sphere.transform);
            t.transform.localPosition = new Vector3(0, 0, -0.1f);
        }
        else
        {
            lastsFor = 0;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        Damage damage = other.GetComponent<Damage>();
        if (damage && damage.targets.Contains("Enemy"))
        {
            if (damage.active)
            {
                if (lastHit != other.gameObject)
                {
                    if (!onOff || !IsSolved)
                    {
                        Activate();
                    }
                    else
                    {
                        Deactivate();
                    }
                    lastHit = other.gameObject;
                }
            }
            else
            {
                OnTriggerExit2D(other);
            }
        }
    }

    protected virtual void OnTriggerStay2D(Collider2D other)
    {
        OnTriggerEnter2D(other);
    }

    protected virtual void OnTriggerExit2D(Collider2D other)
    {
        if (lastHit == other.gameObject)
        {
            lastHit = null;
        }
    }

    void Update()
    {
        if(sphere)
        {
            sphere.material.CopyPropertiesFromMaterial(originalMaterial);
            sphere.material.color = Color.Lerp(off, on, State);
        }
    }

    void OnDestroy()
    {
        switchActive[Name] = active;
        switchActivated[Name] = activated;
        lastActivations[Name] = lastActivation;
    }

    public void Activate()
    {
        active=true;
        activated = true;
        lastActivation = Time.time;
    }

    public void Deactivate()
    {
        active = false;
        activated = false;
    }

    public float State
    {
        get
        {
            if(active && !activated)
            {
                Activate();
            }
            if(active)
            {
                if (lastsFor > 0)
                {
                    if(Time.time-lastsFor>=lastActivation)
                    {
                        Deactivate();
                    }
                    return Mathf.Clamp(1f - ((Time.time - lastActivation) / lastsFor), 0f, 1f);
                }
                return 1;
            }
            return 0;
        }
    }

    public bool IsSolved
    {
        get
        {
            return State > 0;
        }
    }
}
