﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Damage : ManipulateHealth
{
    public bool slashRecovery;
    public bool recoverable;

    protected override void Action(Controller controller)
    {
        bool decreased = false;
        if(recoverable)
        {
            decreased=controller.health.RecoverableDecrease(healthImpact, slashRecovery, pierceHealthImpact);
        }
        else
        {
            decreased=controller.health.Decrease(healthImpact, slashRecovery, pierceHealthImpact);
        }
        if(decreased)
        {
            controller.health.SetInvincibleFor(recoveryTime);
        }
    }

    protected override void Burst()
    {
    }
}