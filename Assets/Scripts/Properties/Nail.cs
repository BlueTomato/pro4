﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Nail : MonoBehaviour
{
    public Health health;
    public BoxCollider2D bc;
    public GameObject explosion;

    void Start()
    {
        bc = GetComponent<BoxCollider2D>();
        bc.isTrigger = true;
        health = gameObject.AddComponent<Health>();
        health.maxHealth = 6;
    }
	
	void Update ()
    {
        transform.eulerAngles = new Vector3(0,0,180)*Mathf.Sin(Time.time);
	}
}
