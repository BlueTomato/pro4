﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour
{

    public Transform[] backgrounds; // all elements to be parallaxed
    private float[] parallaxScales; // proportions of camera's movement for moving backgrounds
    private Vector3[] startingPositions;
    public float smoothing; // >0

 //   private Vector3 camPos = Camera.main.transform.position;
    public Vector3 origin;
    private Transform cam;
    private Vector3 previousCamPos; // position of cam in previous frame

    //void Awake()
    //{
        
    //}

    void Start()
    {
        cam = Camera.main.transform;
        //store previous frame
        origin = transform.position;
        previousCamPos = cam.position;

       

        //assign respective parallax scales for every bg element
        parallaxScales = new float[backgrounds.Length];
        for (int i = 0; i < backgrounds.Length; i++)
        {
            //GameObject[] gm = (GameObject) backgrounds.Clone();
            //startingPositions[i] = gm.transform.position;

            //  GameObject p = backgrounds[i].transform.parent.gameObject;
            Transform p = backgrounds[i].gameObject.transform;
            p.transform.position = new Vector3(p.transform.position.x + backgrounds[i].position.z * 4f, p.transform.position.y, p.transform.position.z);
            parallaxScales[i] = backgrounds[i].position.z * -1;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        for (int i = 0; i < backgrounds.Length; i++)
        {

            //difference multiplied by scale
            float parallax = (previousCamPos.x - cam.position.x) * (parallaxScales[i] / smoothing);

            float backgroundTargetPosX = backgrounds[i].position.x + parallax;

            //target position for bg 
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);

            // fade between current position and target position  using lerp
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }

        //set previous cam pos at end of frame
        previousCamPos = cam.position;
    }

    //private void OnApplicationQuit()
    //{
    //    for (int i = 0; i < backgrounds.Length; i++)
    //    {
    //        backgrounds[i].position = startingPositions[i];
    //    //    cam.position = camPos;
    //    }
    //}
}
