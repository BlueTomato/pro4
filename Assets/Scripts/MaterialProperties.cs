﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MaterialProperties:ScriptableObject
{
    public bool resetWeight;
    public List<MaterialGroup> groups=new List<MaterialGroup>();
    public float weight;
    public int layer;
    public int id;
    protected static int currentID;
    public List<string> colorProperties=new List<string>();
    public List<Color> colorValues = new List<Color>();
    public List<string> floatProperties = new List<string>();
    public List<float> floatValues = new List<float>();

    void OnEnable()
    {
        id = currentID;
        currentID++;
        if(MaterialManager.Instance)
        {
            MaterialManager.Instance.properties.Add(this);
        }
    }

    public bool Add(string name, Color color)
    {
        if (!colorProperties.Contains(name))
        {
            colorProperties.Add(name);
            colorValues.Add(color);
            return true;
        }
        return false;
    }

    public bool Add(string name, float value)
    {
        if (!floatProperties.Contains(name))
        {
            floatProperties.Add(name);
            floatValues.Add(value);
            return true;
        }
        return false;
    }

    public bool Remove(string name, Color color)
    {
        if (colorProperties.Contains(name))
        {
            int index = colorProperties.IndexOf(name);
            colorProperties.RemoveAt(index);
            colorValues.RemoveAt(index);
            return true;
        }
        else if(floatProperties.Contains(name))
        {
            int index = floatProperties.IndexOf(name);
            floatProperties.RemoveAt(index);
            floatValues.RemoveAt(index);
            return true;
        }
        return false;
    }

    public Material Apply(Material material, float weight)
    {
        for (int i = 0; i < colorProperties.Count; i++)
        {
            if (material.HasProperty(colorProperties[i]))
            {
                material.SetColor(colorProperties[i], Color.Lerp(material.GetColor(colorProperties[i]), colorValues[i], weight));
            }
        }
        for (int i = 0; i < floatProperties.Count; i++)
        {
            if (material.HasProperty(floatProperties[i]))
            {
                material.SetFloat(floatProperties[i], Mathf.Lerp(material.GetFloat(floatProperties[i]), floatValues[i], weight));
            }
        }
        return material;
    }

    public List<Material> Apply(float weight)
    {
        List<Material> materials = new List<Material>();
        foreach(MaterialGroup g in groups)
        {
            materials.AddRange(MaterialManager.Instance.GetMaterialsInGroup(g));
        }
        for(int i=0;i<materials.Count;i++)
        {
            Apply(materials[i], weight);
        }
        return materials;
    }

    public List<Material> Apply()
    {
        List<Material> materials=Apply(weight);
        if(resetWeight)
        {
            weight = 0;
        }
        return materials;
    }

    public class Comparer : IComparer<MaterialProperties>
    {
        public int Compare(MaterialProperties x, MaterialProperties y)
        {
            if (x.Equals(y))
            {
                return 0;
            }
            if (x.layer > y.layer || (x.layer == y.layer && x.id > y.id))
            {
                return 1;
            }
            return -1;
        }
    }
}
