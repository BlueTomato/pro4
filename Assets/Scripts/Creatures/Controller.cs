﻿using UnityEngine;
using System.Collections;

public class Controller : RaycastController
{
    public Health health;

    public float maxHealth = 100;
    public float minHealth = 0;

    public float maxSlopeAngle = 80;
    public float moveSpeed = 10;

    protected Transform platform;
    private Vector3 previousPlatformPosition;

    public float gripUnitsPerSecond = 100f;
    protected float gravity;
    protected float gravityScale = 1.0f;

    protected Vector3 velocity;
    protected Vector2 velocitySmoothing;

    public Vector2 accelerationTimeAirborne = new Vector2(.2f,.1f);
    public Vector2 accelerationTimeGrounded = new Vector2(.2f, .1f);

    protected Vector2 directionalInput;

    protected float knockBackStart;
    protected Vector2 knockBack;
    protected float knockBackEnd;

    protected bool sliding;
    protected float slideEnd;
    protected float slideSpeed;
    protected float slideCoolDown;

    protected bool previouslyGrounded = false;
    protected float lastGrounded = -1;

    protected bool jumping;
    protected bool flying;
    protected bool shooting;
    protected bool crouching;
    protected bool lookingUp;

    protected float attackTime = 0.2f;
    protected float attackEnd;

    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .5f;
    private int currentJump = 0;
    private int maxJumps = 2;
    private float groundedTime;

    private float maxJumpVelocity;
    private float minJumpVelocity;

    public bool winged;

    public CollisionInfo collisions;
    [HideInInspector]
    public Vector2 movement;

    public float weight = 1f;

    private int depictedFaceDirection=1;

    private float fallThroughTime;

    private bool dead;
    private bool nailInTheCoffin;
    private float died;
    private float toBeDestroyed;

    protected override void Start()
    {
        ReadjustPhysics();

        health = gameObject.AddComponent<Health>();

        health.maxHealth = maxHealth;
        health.minHealth = minHealth;

        base.Start();

        dead = false;
        nailInTheCoffin = false;
        collisions.faceDirection = 1;
    }

    public void SetDirectionalInput(Vector2 input)
    {
        directionalInput = input;
    }

    public Vector2 GetMovement(Vector2 moveAmount)
    {
        Vector2 input = directionalInput;

        if (IsKnockedBack)
        {
            input = Vector2.zero;
            moveAmount.x=0;
        }

        SetDepictedFaceDirection(input.x);

        if (IsKnockedBack)
        {
            moveAmount += knockBack/weight;
        }

        if (SlideMode)
        {
            moveAmount.x += depictedFaceDirection * slideSpeed;
        }

        moveAmount *= Time.deltaTime;

        UpdateRaycastOrigins();

        collisions.Reset();
        collisions.moveAmountOld = moveAmount;

        movement = input;
        SetFaceDirection(moveAmount.x);

        moveAmount += MoveWithPlatform();

        if (moveAmount.y < 0)
        {
            moveAmount=DescendSlope(moveAmount);
        }
        moveAmount=HorizontalCollisions(moveAmount);
        if (moveAmount.y != 0)
        {
            moveAmount=VerticalCollisions(moveAmount);
        }

        return moveAmount;
    }

    public void Move(Vector2 moveAmount)
    {
        transform.Translate(GetMovement(moveAmount));
        if(platform)
        {
            previousPlatformPosition = platform.position;
        }
    }

    public void Move()
    {
        Move(velocity);
    }

    protected void SetFaceDirection(float direction)
    {
        if (direction != 0)
        {
            collisions.faceDirection = (int)Mathf.Sign(direction);
        }
    }

    protected Vector2 MoveWithPlatform()
    {
        if (platform)
        {
            if (collisions.below && platform.position != previousPlatformPosition && Vector3.Distance(platform.position, previousPlatformPosition) / Time.deltaTime <= gripUnitsPerSecond)
            {
                return (platform.position - previousPlatformPosition)*weight;
            }
        }
        return Vector2.zero;
    }

    protected void CalculateVelocityX()
    {
        float targetVelocity = directionalInput.x * moveSpeed;
        float accelerationTime = accelerationTimeGrounded.x;
        if (!collisions.below)
        {
            accelerationTime = accelerationTimeAirborne.x;
        }
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocity, ref velocitySmoothing.x, accelerationTime);
    }

    protected void CalculateVelocityY()
    {
        float targetVelocity = directionalInput.y * moveSpeed;
        float accelerationTime = accelerationTimeGrounded.y;
        if (!HitsWall)
        {
            accelerationTime = accelerationTimeAirborne.y;
        }
        velocity.y = Mathf.SmoothDamp(velocity.y, targetVelocity, ref velocitySmoothing.y, accelerationTime);
    }

    protected void CalculateVelocity()
    {
        CalculateVelocityX();
        CalculateVelocityY();
    }

    protected void ApplyGravity()
    {
        Vector2 g = Physics2D.gravity.normalized * (gravity * Time.deltaTime) * gravityScale;
        velocity.x+=g.x;
        velocity.y += g.y;
        velocity.y = Mathf.Clamp(velocity.y, Physics2D.gravity.y * gravityScale, float.MaxValue);
    }

    Vector2 HorizontalCollisions(Vector2 moveAmount)
    {
        float directionX = collisions.faceDirection;
        float rayLength = Mathf.Abs(moveAmount.x) + Raycast.SkinWidth;

        if (Mathf.Abs(moveAmount.x) < Raycast.SkinWidth)
        {
            rayLength = 2 * Raycast.SkinWidth;
        }

        int i = 0;
        Raycast.ResetRays();

        while (Raycast.HasNextRay(Vector2.right*directionX))
        {
            RaycastHit2D hit = Raycast.NextRay(Vector2.right * directionX, rayLength, 0);

            if (hit)
            {
                if (hit.distance == 0 || hit.collider.tag == "Through")
                {
                    continue;
                }

                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                if (i == 0 && slopeAngle <= maxSlopeAngle)
                {
                    if (collisions.descendingSlope)
                    {
                        collisions.descendingSlope = false;
                        moveAmount = collisions.moveAmountOld;
                    }
                    float distanceToSlopeStart = 0;
                    if (slopeAngle != collisions.slopeAngleOld)
                    {
                        distanceToSlopeStart = hit.distance - Raycast.SkinWidth;
                        moveAmount.x -= distanceToSlopeStart * directionX;
                    }
                    moveAmount=ClimbSlope(moveAmount, slopeAngle,hit.normal);
                    moveAmount.x += distanceToSlopeStart * directionX;
                }

                if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle)
                {
                    moveAmount.x = (hit.distance - Raycast.SkinWidth) * directionX;
                    rayLength = hit.distance;

                    if (collisions.climbingSlope)
                    {
                        moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);
                    }

                    collisions.left = directionX == -1;
                    collisions.right = directionX == 1;
                }
            }
            i++;
        }
        return moveAmount;
    }

    Vector2 VerticalCollisions(Vector2 moveAmount)
    {
        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + Raycast.SkinWidth;

        Raycast.ResetRays();
        while (Raycast.HasNextRay(Vector2.up*directionY))
        {
            RaycastHit2D hit = Raycast.NextRay(Vector2.up * directionY, rayLength, moveAmount.x);

            if (hit)
            {
                if (hit.collider.tag == "Through")
                {
                    if (directionY==1 || hit.distance == 0)
                    {
                        continue;
                    }
                    if (collisions.fallingThroughPlatform)
                    {
                        continue;
                    }
                    if (IsFallingThrough)
                    {
                        Physics2D.IgnoreCollision(hit.collider, BoxCollider);
                        continue;
                    }
                }

                moveAmount.y = (hit.distance - Raycast.SkinWidth) * directionY;
                rayLength = hit.distance;
                platform = hit.collider.transform;
                previousPlatformPosition = platform.position;

                if (collisions.climbingSlope)
                {
                    moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                }

                collisions.below = directionY == -1;
                collisions.above = directionY == 1;
            }
        }

        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            rayLength = Mathf.Abs(moveAmount.x) + Raycast.SkinWidth;
            Vector2 rayOrigin = ((directionX == -1) ? Raycast.BottomLeft : Raycast.BottomRight) + Vector2.up * moveAmount.y;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    moveAmount.x = (hit.distance - Raycast.SkinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                }
            }
        }
        return moveAmount;
    }

    Vector2 ClimbSlope(Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal)
    {
        float moveDistance = Mathf.Abs(moveAmount.x);
        float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        if (moveAmount.y <= climbmoveAmountY)
        {
            moveAmount.y = climbmoveAmountY;
            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
            collisions.below = true;
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
            collisions.slopeNormal = slopeNormal;
        }
        return moveAmount;
    }

    Vector2 DescendSlope(Vector2 moveAmount)
    {

        RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(Raycast.BottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
        RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(Raycast.BottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
        if (maxSlopeHitLeft ^ maxSlopeHitRight)
        {
            moveAmount=SlideDownMaxSlope(maxSlopeHitLeft, moveAmount);
            moveAmount=SlideDownMaxSlope(maxSlopeHitRight, moveAmount);
        }

        if (!collisions.slidingDownMaxSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            Vector2 rayOrigin = (directionX == -1) ? Raycast.BottomRight : Raycast.BottomLeft;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle)
                {
                    if (Mathf.Sign(hit.normal.x) == directionX)
                    {
                        if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))
                        {
                            float moveDistance = Mathf.Abs(moveAmount.x);
                            float descendmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                            moveAmount.y -= descendmoveAmountY;

                            collisions.slopeAngle = slopeAngle;
                            collisions.descendingSlope = true;
                            collisions.below = true;
                            collisions.slopeNormal = hit.normal;
                        }
                    }
                }
            }
        }
        return moveAmount;
    }

    Vector2 SlideDownMaxSlope(RaycastHit2D hit, Vector2 moveAmount)
    {
        if (hit)
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            if (slopeAngle > maxSlopeAngle)
            {
                moveAmount.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

                collisions.slopeAngle = slopeAngle;
                collisions.slidingDownMaxSlope = true;
                collisions.slopeNormal = hit.normal;
            }
        }
        return moveAmount;
    }

    public void SetDepictedFaceDirection(float direction)
    {
        if(direction!=0)
        {
            depictedFaceDirection = (int)Mathf.Sign(direction);
        }
    }

    public void SetGravityScale(float scale)
    {
        gravityScale = scale;
    }

    public void SetMaxJumps(int jumps)
    {
        maxJumps=jumps;
    }

    public void ResetJumps()
    {
        if(!collisions.slidingDownMaxSlope)
        {
            currentJump = 0;
        }
    }

    public float SpaceAbove(float rayLength)
    {
        float distance = float.MaxValue;
        Raycast.ResetRays();
        while(Raycast.HasNextRay(Vector2.up))
        {
            RaycastHit2D hit = Raycast.NextRay(Vector2.up, rayLength, 0);
            if(hit.transform && hit.transform.tag!="Ignore" && hit.transform.tag!="Through")
            {
                distance = Mathf.Min(distance,hit.distance);
            }
        }
        return distance;
    }


    public bool IsFacingRight
    {
        get
        {
            return depictedFaceDirection >= 0;
        }
    }

    public int FaceDirection
    {
        get
        {
            return (int)Mathf.Sign(depictedFaceDirection);
        }
    }

    public bool IsSliding
    {
        get
        {
            return Time.time < slideEnd;
        }
    }

    public bool SlideMode
    {
        get
        {
            return sliding;
        }
    }

    public bool CanSlide
    {
        get
        {
            if(IsGrounded && !IsSliding)
            {
                return Time.time >= slideCoolDown;
            }
            return false;
        }
    }

    public bool IsIdle
    {
        get
        {
            if (IsGrounded && !IsFalling && !IsCrouching && !SlideMode && !IsFlying && !IsJumping && (!IsMoving || HitsWall))
            {
                return true;
            }
            return false;
        }
    }

    public bool HitsWall
    {
        get
        {
            return collisions.left || collisions.right;
        }
    }

    public bool IsGrounded
    {
        get
        {
            if (collisions.below)
            {
                if (!previouslyGrounded)
                {
                    lastGrounded = Time.time;
                }
                previouslyGrounded = true;
            }
            else
            {
                previouslyGrounded = false;
            }
            return previouslyGrounded;
        }
    }

    public bool IsPeaking
    {
        get
        {
            if (collisions.above)
            {
                return true;
            }
            return false;
        }
    }

    public bool IsMoving
    {
        get
        {
            return movement.x != 0;
        }
    }

    public bool TriesMoving
    {
        get
        {
            return directionalInput.x!=0;
        }
    }

    public bool IsFalling
    {
        get
        {
            return !IsGrounded && !IsJumping && !IsFlying && velocity.y < 0;
        }
    }

    public bool IsLanding
    {
        get
        {
            return Time.time <= lastGrounded;
        }
    }

    public bool IsJumping
    {
        get
        {
            return jumping && velocity.y > 0;
        }
    }

    public int CurrentJump
    {
        get
        {
            return currentJump;
        }
    }

    public int MaxJumps
    {
        get
        {
            return maxJumps;
        }
    }

    public bool IsCrouching
    {
        get
        {
            return crouching;
        }
    }

    public bool IsLookingUp
    {
        get
        {
            return lookingUp;
        }
    }

    public bool IsFallingThrough
    {
        get
        {
            if (Time.time > fallThroughTime)
            {
                StopFallingThrough();
            }
            return collisions.fallingThroughPlatform;
        }
    }

    public bool IsFlying
    {
        get
        {
            return flying;
        }
    }

    public bool IsShooting
    {
        get
        {
            return shooting;
        }
    }

    public bool IsAttacking
    {
        get
        {
            return Time.time < attackEnd;
        }
    }

    public float GravityScale
    {
        get
        {
            return gravityScale;
        }
    }

    public bool IsKnockedBack
    {
        get
        {
            return Time.time < knockBackEnd;
        }
    }

    public bool IsAlive
    {
        get
        {
            if (dead)
            {
                if (Time.time >= toBeDestroyed && !nailInTheCoffin)
                {
                    Destroy(gameObject);
                    nailInTheCoffin = true;
                }
                return false;
            }
            return true;
        }
    }

    public float TimeSinceDeath
    {
        get
        {
            if (!IsAlive)
            {
                return Time.time - died;
            }
            return 0;
        }
    }

    public float TimeSinceKnockBack
    {
        get
        {
            return Time.time - knockBackStart;
        }
    }

    public float TimeUntilRecovery
    {
        get
        {
            return Mathf.Max(0, knockBackEnd - Time.time);
        }
    }

    public float KnockBackWeight
    {
        get
        {
            return 1f - ((Time.time - knockBackStart) / (knockBackEnd - knockBackStart));
        }
    }

    public Vector2 KnockBackDirection
    {
        get
        {
            return knockBack;
        }
    }

    public void StartJump()
    {
        if (currentJump < maxJumps)
        {
            collisions.below = false;

            jumping = true;

            velocity.y = maxJumpVelocity;

            currentJump++;
        }
    }

    public void EndJump()
    {
        jumping = false;
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }

    public void Crouch()
    {
        if(IsGrounded)
        {
            crouching = true;
        }
    }

    public void CrouchEnd()
    {
        crouching = false;
    }

    public void LookUp()
    {
        lookingUp = true;
    }

    public void LookUpEnd()
    {
        lookingUp = false;
    }

    public void ResetVelocityY()
    {
        velocity.y = 0;
    }

    public void StartFallingThrough()
    {
        collisions.fallingThroughPlatform = true;
        fallThroughTime = Time.time + 0.1f;
    }

    public void StopFallingThrough()
    {
        collisions.fallingThroughPlatform = false;
    }

    public void StartFlight()
    {
        flying = true;
        SetGravityScale(0.01f);
    }

    public void EndFlight()
    {
        flying = false;
        SetGravityScale(1.0f);
    }

    public void StartShooting()
    {
        shooting = true;
    }

    public void EndShooting()
    {
        shooting = false;
    }

    public void Attack()
    {
        attackEnd = Time.time + attackTime;
    }

    private void KnockBack(float angle, float time, float force, bool apply)
    {
        if (apply)
        {
            knockBack = Globals.AngleToVector(angle) * force;
            knockBackStart = Time.time;
            knockBackEnd = Time.time + time;
        }
    }

    public void KnockBack(float angle, float time, float force)
    {
        KnockBack(angle,time,force, !IsKnockedBack);
    }

    public void ContinuousKnockBack(float angle, float time, float force)
    {
        KnockBack(angle, time, force, true);
    }

    public void StartSlide(float time, float force, float coolDown)
    {
        sliding = true;
        slideSpeed = force;
        slideEnd = Time.time + time;
        slideCoolDown = slideEnd+coolDown;
    }

    public void StartSlide(float time, float force)
    {
        StartSlide(time,force,0);
    }

    public void ContinueSlide()
    {
        StartSlide(0, slideSpeed, slideCoolDown-slideEnd);
    }

    public void EndSlide()
    {
        sliding = false;
    }

    public void Die(float dieIn)
    {
        if(IsAlive)
        {
            dead = true;
            died = Time.time;
            toBeDestroyed = died + dieIn;
        }
    }

    public void ReadjustPhysics()
    {
        gravity = (2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
    }

    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public bool climbingSlope;
        public bool descendingSlope;
        public bool slidingDownMaxSlope;

        public float slopeAngle, slopeAngleOld;
        public Vector2 slopeNormal;
        public Vector2 moveAmountOld;
        public int faceDirection;
        public bool fallingThroughPlatform;

        public void Reset()
        {
            above = below = false;
            left = right = false;
            climbingSlope = false;
            descendingSlope = false;
            slidingDownMaxSlope = false;
            slopeNormal = Vector2.zero;

            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }
}