﻿using UnityEngine;
using System.Collections;

public class GroundedController : Controller
{
    void Update()
    {
        if(!GameManager.Instance.IsPausedOrSuspended)
        {
            CalculateVelocityX();
            ApplyGravity();
            Move(velocity);
        }
    }
}