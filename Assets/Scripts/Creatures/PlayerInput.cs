﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Controller))]
public class PlayerInput : ControllerController
{
  
    public GameObject bloodshot;
    public GameObject arm;
    public Damage damager;
    public GameObject rotator;
    private Vector3 rotatorPosition;

    public Vector3 crouchSize=new Vector3(1,2,0);
    public Vector3 crouchOffset = new Vector3(0,1,0);

    public Vector3 slideSize = new Vector3(1,1,0);
    public Vector3 slideOffset = new Vector3(0,0.5f,0);

    public int numberOfShots;
    public int numberOfLinkedShots;
    public float minShot = 15;
    
    public override void Start()
    {
     
        base.Start();
        
        controller.health.maxHealth = PlayerSettings.Instance.playerHealth;
        controller.health.minHealth = PlayerSettings.Instance.playerHealthMin;
        controller.winged = PlayerSettings.Instance.winged;
        controller.SetMaxJumps(PlayerSettings.Instance.jumps);
        controller.maxJumpHeight = PlayerSettings.Instance.jumpHeight;
        controller.ReadjustPhysics();

        

        if (PlayerSettings.Instance.placePlayerAtPosition)
        {
            controller.transform.position = PlayerSettings.Instance.position;
        }

        if(rotator)
        {
            rotatorPosition = rotator.transform.localPosition;
        }
    }

    public override void Update()
    {
        
        base.Update();
        controller.SetDirectionalInput(Vector2.zero);
        if(!GameManager.Instance.IsPausedOrSuspended)
        {
            Vector2 directionalInput = new Vector2(0,0);

            if (controlManager.GetLeft())
            {
                directionalInput.x -= 1;
            }
            if (controlManager.GetRight())
            {
                directionalInput.x += 1;
            }
            if (controlManager.GetUp())
            {
                directionalInput.y += 1;
            }
            if (controlManager.GetDown())
            {
                directionalInput.y -= 1;
            }

            if (directionalInput.y > 0)
            {
                controller.LookUp();
            }
            else
            {
                controller.LookUpEnd();
            }

            if (directionalInput.y < 0)
            {
                controller.Crouch();
            }
            if (!controller.IsGrounded || directionalInput.y >= 0 || directionalInput.x!=0)
            {
                controller.CrouchEnd();
            }
            if(controller.IsGrounded)
            {
                controller.ResetJumps();
            }

            float rayLength = controller.originalSize.y - slideSize.y;

            if (controller.SlideMode)
            {
                directionalInput = Vector2.zero;
                controller.SetBoxCollider(slideOffset,slideSize);
                controller.animator.SetBool("sliding", true);
            }

            if(!controller.IsSliding)
            {
                if (controller.SlideMode && controller.SpaceAbove(rayLength) < rayLength)
                {
                    controller.ContinueSlide();
                    controller.animator.SetBool("sliding", true);
                }
                else
                {
                    controller.EndSlide();
                }
            }


            if(!controller.SlideMode)
            {
                if (!controller.IsCrouching)
                {
                    controller.SetOriginalBoxCollider();
                }
                else
                {
                    controller.SetBoxCollider(crouchOffset,crouchSize);
                }

                if (controlManager.GetActionDown(ControlManager.Action.Strike))
                {
                    if (directionalInput.y<0)
                    {
                        if(controller.CanSlide)
                        {
                            directionalInput.x = 0;
                            controller.StartSlide(0.5f, 15, 0.25f);
                        }
                    }
                    else if (!controller.animator.GetBool("striking"))
                    {
                        controller.Attack();
                    }
                }
                if (controlManager.GetActionDown(ControlManager.Action.Shoot))
                {
                    controller.StartShooting();
                    if (directionalInput.y > 0)
                    {
                        Shoot(0);
                    }
                    else
                    {
                        Shoot(90);
                    }
                }
                if (!controlManager.GetAction(ControlManager.Action.Shoot))
                {
                    controller.EndShooting();
                }
                if (controlManager.GetActionDown(ControlManager.Action.Jump))
                {
                    if (directionalInput.y < 0)
                    {
                        controller.StartFallingThrough();
                    }
                    else
                    {
                        controller.StartJump();
                    }
                }

                if (controller.winged && controlManager.GetAction(ControlManager.Action.Jump) && directionalInput.y >= 0 && !controller.IsJumping)
                {
                    controller.StartFlight();
                }

                controller.animator.SetBool("sliding", false);
            }

            if (controller.IsLookingUp)
            {
                controller.animator.SetBool("up", true);
            }
            else
            {
                controller.animator.SetBool("up", false);
            }

            if (controller.IsCrouching)
            {
                controller.animator.SetBool("crouching", true);
                controller.SetDepictedFaceDirection(directionalInput.x);
                directionalInput.x = 0;
            }
            else
            {
                controller.animator.SetBool("crouching", false);
            }

            if (!controlManager.GetAction(ControlManager.Action.Jump))
            {
                controller.StopFallingThrough();
                controller.EndJump();
            }

            if (controller.IsGrounded || !controlManager.GetAction(ControlManager.Action.Jump) || directionalInput.y<0)
            {
                controller.EndFlight();
            }

            if (controller.IsFalling && !controller.IsFallingThrough && directionalInput.y < 0)
            {
                controller.SetGravityScale(2f);
            }
            else if(!controller.IsFlying)
            {
                controller.SetGravityScale(1);
            }

            if (!controller.IsFallingThrough)
            {
                controller.StopFallingThrough();
            }

            controller.SetDirectionalInput(directionalInput);

            if (controller.IsGrounded || controller.IsPeaking)
            {
                controller.ResetVelocityY();
            }

            if (controller.IsFacingRight)
            {
                controller.animator.SetBool("facingRight", true);
            }
            else
            {
                controller.animator.SetBool("facingRight", false);
            }

            if ((controller.IsMoving || controller.TriesMoving) && controller.IsGrounded && !controller.HitsWall)
            {
                controller.animator.SetBool("walking", true);
            }
            else
            {
                controller.animator.SetBool("walking", false);
            }

            if (controller.IsIdle)
            {
                controller.animator.SetBool("idle", true);

                bool leftFootUp = true;
                bool rightFootUp = true;

                controller.Raycast.ResetRays();

                RaycastOrigins.RayDirection direction = RaycastOrigins.RayDirection.down;

                while (controller.Raycast.HasNextRay(direction))
                {
                    float relativeRayDown = 0.5f - controller.Raycast.RelativeRay(direction);
                    relativeRayDown *= controller.FaceDirection;

                    RaycastHit2D hit = controller.Raycast.NextRay(direction,controller.Raycast.SkinWidth,0);

                    if (hit.transform && hit.distance<=controller.Raycast.SkinWidth*2f)
                    {
                        if (relativeRayDown > 0)
                        {
                            rightFootUp &= false;
                        }
                        else if (relativeRayDown < 0)
                        {
                            leftFootUp &= false;
                        }
                    }

                }
                if(leftFootUp && rightFootUp)
                {
                    leftFootUp = rightFootUp = false;
                }
                controller.animator.SetBool("leftFootUp", leftFootUp);
                controller.animator.SetBool("rightFootUp", rightFootUp);
            }
            else
            {
                controller.animator.SetBool("idle", false);
                controller.animator.SetBool("leftFootUp", false);
                controller.animator.SetBool("rightFootUp", false);
            }

            if (controller.IsJumping)
            {
                if(controller.CurrentJump>1)
                {
                    controller.animator.SetBool("doubleJumping", true);
                }
                controller.animator.SetBool("jumping", true);
            }
            else
            {
                controller.animator.SetBool("doubleJumping", false);
                controller.animator.SetBool("jumping", false);
            }

            if(controller.IsFalling)
            {
                controller.animator.SetBool("falling", true);
            }
            else
            {
                controller.animator.SetBool("falling", false);
            }

            if (controller.IsFlying)
            {
                controller.animator.SetBool("flying", true);
            }
            else
            {
                controller.animator.SetBool("flying", false);
            }

            if (controller.IsLanding)
            {
                controller.animator.SetBool("landing", true);
            }
            else
            {
                controller.animator.SetBool("landing", false);
            }

            if (controller.IsKnockedBack)
            {
                if (controller.KnockBackDirection.x * controller.FaceDirection < 0)
                {
                    controller.animator.SetBool("knockBackLeft", true);
                }
                else
                {
                    controller.animator.SetBool("knockBackRight", true);
                }
            }
            else
            {
                controller.animator.SetBool("knockBackLeft", false);
                controller.animator.SetBool("knockBackRight", false);
            }


            if (controller.IsShooting)
            {
                controller.animator.SetBool("shooting", true);
            }
            else
            {
                controller.animator.SetBool("shooting", false);
            }


            if (controller.IsAttacking)
            {
                controller.animator.SetBool("striking", true);

                damager.GetComponent<SpriteRenderer>().enabled = true;
                damager.active = true;
            }
            else
            {
                controller.animator.SetBool("striking", false);

                if(damager)
                {
                    damager.GetComponent<SpriteRenderer>().enabled = false;
                    damager.active = false;
                }
            }

            AlignRotator();

            if(PlayerSettings.Instance.track)
            {
                PlayerSettings.Instance.position = controller.transform.position;
            }
        }
    }

    public void AlignRotator()
    {
        float rotation = 0.0f;

        if (controller.IsLookingUp)
        {
            rotation = 90.0f;
        }

        rotator.transform.rotation = Quaternion.Euler(0, (controller.IsFacingRight) ? 0 : 180, rotation);

        rotator.transform.localPosition = rotatorPosition;

        if (controller.IsCrouching)
        {
            rotator.transform.localPosition += new Vector3(0, -1, 0);
        }
    }

    public void Shoot(float angle)
    {
        SortedDictionary<String, Ability> abilities = Ability.List;

        float shotCost = PlayerSettings.Instance.shotCost;

        if (abilities["Speed"].IsEnabledAndActive && abilities["Speed"].Setting < abilities["Speed"].maxSetting)
        {
            shotCost += abilities["Speed"].Setting - abilities["Speed"].maxSetting;
        }

        if (abilities["Target"].IsEnabledAndActive && abilities["Target"].IsEnabledAndActive)
        {
            shotCost *= 2f;
        }

        if (abilities["Bounce"].IsEnabledAndActive && abilities["Bounce"].IsEnabledAndActive)
        {
            shotCost += 2f;
        }

        if (controller.health.Get - shotCost >= minShot)
        {
            if (numberOfShots < 5)
            {
                if(!(abilities["Link"].IsEnabledAndActive && abilities["Link"].IsEnabledAndActive && numberOfLinkedShots > 0))
                {
                    controller.health.RecoverableDecrease(shotCost, false, true);
                    GameObject box = Instantiate(bloodshot);
                    box.transform.position = arm.transform.position;

                    Projectile shot = box.GetComponent<Projectile>();
                    shot.cost = shotCost;
                    float direction = 1;
                    if (!controller.IsFacingRight)
                    {
                        direction = -1;
                    }
                    shot.targetAngle = angle * direction;

                    shot.character = this;
                    shot.SetTargetSpeed((abilities["Speed"].Setting + 1) * 5);

                    shot.SetBoomerang(abilities["Boomerang"].IsEnabledAndActive);
                    shot.SetLink(abilities["Link"].IsEnabledAndActive);
                    shot.SetMagnet(abilities["Magnet"].IsEnabledAndActive);
                    shot.SetTarget(abilities["Target"].IsEnabledAndActive);
                    shot.SetBounce(abilities["Bounce"].IsEnabledAndActive);
                    shot.SetPhantom(abilities["Phantom"].IsEnabledAndActive);
                    shot.SetSponge(abilities["Sponge"].IsEnabledAndActive);

                    if (abilities["Push"].IsEnabledAndActive)
                    {
                        shot.SetPush(true);
                        shot.destroyOnTargetCollision = false;
                        shot.healthImpact = 0;
                    }
                    else
                    {
                        shot.healthImpact = 10;
                    }
                }
                else
                {
                    controller.health.CreateText("LINK!", Color.cyan, true);
                }
            }
            else
            {
                controller.health.CreateText("MAX!", CorporateDesign.Instance.red, true);
            }
        }
        else
        {
            controller.health.CreateText("HP!", CorporateDesign.Instance.red, true);
        }
    }
}
