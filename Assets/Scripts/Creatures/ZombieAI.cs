﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Controller))]
public class ZombieAI : ControllerController
{
    private int playerSide;
    private int previousPlayerSide;
    private float turnAround;
    private float turnAroundTime = 2;
    private float attackDistance = 8f;

    public List<Damage> damagers=new List<Damage>();

    public override void Start()
    {
        base.Start();

        damagers = Globals.GetComponentsInChildrenRecursive<Damage>(transform);
    }

    public override void Update()
    {
        base.Update();
        if (!GameManager.Instance.IsPausedOrSuspended)
        {
            if (controller.IsAlive)
            {
                Vector2 directionalInput = Vector2.zero;

                if (GameManager.Instance.player.transform.position.x < transform.position.x)
                {
                    playerSide = -1;
                }
                else
                {
                    playerSide = 1;
                }

                if (playerSide != previousPlayerSide)
                {
                    turnAround = Time.time + turnAroundTime;
                }

                previousPlayerSide = playerSide;

                float distance = GameManager.Instance.player.controller.Distance(controller);

                if (distance<attackDistance*3f && turnAround < Time.time)
                {
                    directionalInput.x += playerSide * 0.05f;
                }

                if (controller.IsFacingRight)
                {
                    controller.animator.SetBool("facingRight", true);
                }
                else
                {
                    controller.animator.SetBool("facingRight", false);
                }

                if (controller.IsMoving)
                {
                    controller.animator.SetBool("walking", true);
                }
                else
                {
                    controller.animator.SetBool("walking", false);
                }

                if (distance < attackDistance)
                {
                    controller.Attack();
                    controller.animator.SetBool("charging", true);
                }
                else
                {
                    controller.animator.SetBool("charging", false);
                }

                if (controller.IsKnockedBack)
                {
                    directionalInput.x = 0;

                    float faceDir = 1;
                    if (!controller.IsFacingRight)
                    {
                        faceDir = -1;
                    }
                    if (controller.KnockBackDirection.x * faceDir < 0)
                    {
                        controller.animator.SetBool("knockBackLeft", true);
                    }
                    else
                    {
                        controller.animator.SetBool("knockBackRight", true);
                    }
                }
                else
                {
                    controller.animator.SetBool("knockBackLeft", false);
                    controller.animator.SetBool("knockBackRight", false);
                }

                if (controller.IsMoving && controller.IsAttacking)
                {
                    directionalInput *= 5f;
                }

                controller.SetDirectionalInput(directionalInput);

                if (controller.health.Get <= 0)
                {
                    controller.Die(1.5f);
                    GameObject item;
                    if (spawnOnDestroy)
                    {
                        item = Instantiate(spawnOnDestroy);
                    }
                    else
                    {
                        item = Instantiate(Item.GetRandomElement().item);
                    }
                    item.transform.position = controller.Center;
                }
            }
            else
            {
                controller.SetDirectionalInput(Vector2.zero);
                controller.BoxCollider.gameObject.tag = "Ignore";
                for (int i = 0; i < damagers.Count; i++)
                {
                    damagers[i].Collider.gameObject.tag = "Ignore";
                    damagers[i].active = false;
                }
                controller.animator.SetBool("dead", true);
            }
        }
        float timeUntilInvincibilityEnds = controller.health.TimeUntilInvincibilityEnds;

        if (timeUntilInvincibilityEnds > 0)
        {
            SetColor("_MaxColor", Color.red, controller.health.RelativeInvincibility);
            SetFloat("_Brightness", 0.5f, controller.health.RelativeInvincibility);
        }
        if (!controller.IsAlive)
        {
            float tsd = controller.TimeSinceDeath;
            SetColor("_TargetColor", new Color(1, 1, 1, 0),tsd);
            SetFloat("_Weight", 1, tsd-0.5f);
            SetFloat("_Brightness", 2,tsd);
            SetFloat("_DisplacementScale", 1, tsd);
        }
    }
}