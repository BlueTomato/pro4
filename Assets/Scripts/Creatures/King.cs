﻿using UnityEngine;
using System.Collections;

public class King : Controller
{
    public GameObject nailLeft;
    public GameObject nailRight;
    public GameObject nailCenter;

    protected override void Start()
    {
        base.Start();
        nailLeft = GameObject.Find("NailLeft");
        nailRight = GameObject.Find("NailRight");
        nailCenter = GameObject.Find("NailCenter");
        //flags.SetFlag(States.NailedLeft);
        //flags.SetFlag(States.NailedRight);
        //flags.SetFlag(States.NailedCenter);
    }

    void Update()
    {
        if(!nailLeft)
        {
            //flags.ResetFlag(States.NailedLeft);
            if (Time.time%12>=0 && Time.time % 12 <=3)
            {
                //flags.SetFlag(States.AttackLeft);
            }
            else
            {
                //flags.ResetFlag(States.AttackLeft);
            }
        }
        if (!nailRight)
        {
            //flags.ResetFlag(States.NailedRight);
            if (Time.time % 12 >= 6 && Time.time % 12 <= 9)
            {
                //flags.SetFlag(States.AttackRight);
            }
            else
            {
                //flags.ResetFlag(States.AttackRight);
            }
        }
        if (!nailCenter)
        {
            //flags.ResetFlag(States.NailedCenter);
        }

        if (!nailLeft && !nailRight && !nailCenter)
        {
        }
    }
}
