﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Controller))]
public class DragonHeadAI : ControllerController
{
    public float offsetPlayerTop;
    public float range;
    public bool goingDown = true;

	public override void Start ()
    {
        base.Start();
        controller.SetGravityScale(0);
    }

    public override void Update ()
    {
        base.Update();
        if (!GameManager.Instance.IsPausedOrSuspended)
        {
            if (controller.IsAlive)
            {
                Vector2 position = GameManager.Instance.player.transform.position + new Vector3(0, GameManager.Instance.player.controller.BoxCollider.bounds.extents.y + offsetPlayerTop);
                if (transform.position.y > position.y + range || controller.collisions.above)
                {
                    goingDown = true;
                }
                if (transform.position.y < position.y)
                {
                    goingDown = false;
                }

                float y = 1;
                if (goingDown)
                {
                    y = -1;
                }

                Vector2 directionalInput = new Vector2(Mathf.Sign(position.x - transform.position.x), y);
                transform.localScale = new Vector3(Mathf.Sign(directionalInput.x), 1, 1);
                controller.SetDirectionalInput(directionalInput);
                controller.StartFallingThrough();

                if (controller.health.Get <= 0)
                {
                    controller.Die(1);
                    GetComponent<Damage>().active = false;
                }
            }
        }

        float timeUntilInvincibilityEnds = controller.health.TimeUntilInvincibilityEnds;

        if (timeUntilInvincibilityEnds > 0)
        {
            SetColor("_Tint", Color.red, controller.health.RelativeInvincibility);
            SetFloat("_Weight", 0, controller.health.RelativeInvincibility);
            SetFloat("_Brightness", 0, controller.health.RelativeInvincibility / 2f);
        }
        if(!controller.IsAlive)
        {
            float tsd = controller.TimeSinceDeath;
            SetColor(new Color(1, 0, 0, 0), tsd);
            SetFloat("_DisplacementScale", 1, tsd);
            SetFloat("_Brightness", 2, tsd);
        }
    }
}
