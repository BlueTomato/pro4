﻿using UnityEngine;
using System.Collections;

public class FloatingController : Controller
{
    protected override void Start()
    {
        base.Start();
        accelerationTimeAirborne.x = 1;
        accelerationTimeAirborne.y = 0.5f;
        accelerationTimeGrounded.x = 0.5f;
        accelerationTimeGrounded.y = 0.5f;
        weight = 0.75f;
        moveSpeed = 10f;
    }

    void Update()
    {
        if (!GameManager.Instance.IsPausedOrSuspended)
        {
            CalculateVelocity();
            Move(velocity);
        }
    }
}