﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastController : MonoBehaviour
{
    public Animator animator;
    public LayerMask collisionMask;

    private BoxCollider2D boxCollider;
    public Vector3 originalOffset;
    public Vector3 originalSize;
    private RaycastOrigins raycastOrigins;

    public float skinWidth = 0.015f;
    public float maximumRayDistance = 0.25f;

    protected virtual void Awake()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        originalOffset = boxCollider.offset;
        originalSize = boxCollider.size;
    }

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        if (!animator)
        {
            animator = Globals.GetComponentInChildrenRecursive<Animator>(transform);
        }
        raycastOrigins = new RaycastOrigins(BoxCollider, collisionMask,skinWidth,maximumRayDistance);
    }

    public void UpdateRaycastOrigins()
    {
        raycastOrigins.SkinWidth = skinWidth;
        raycastOrigins.MaximumRayDistance = maximumRayDistance;
        raycastOrigins.BoxCollider = BoxCollider;
        raycastOrigins.CollisionMask = collisionMask;
    }

    public Vector3 PersistentCenter
    {
        get
        {
            return gameObject.transform.position+originalOffset;
        }
    }
    public Vector3 Center
    {
        get
        {
            return boxCollider.bounds.center;
        }
    }

    public float Distance(Vector2 position)
    {
        return Vector2.Distance(Center,position);
    }

    public float Distance(RaycastController controller)
    {
        return Vector2.Distance(Center,controller.Center);
    }

    public Vector3 Top
    {
        get
        {
            return Center + new Vector3(0, boxCollider.bounds.extents.y, 0);
        }
    }

    public Vector3 Bottom
    {
        get
        {
            return Center - new Vector3(0, -boxCollider.bounds.extents.y, 0);
        }
    }

    public Vector3 Left
    {
        get
        {
            return Center + new Vector3(-boxCollider.bounds.extents.x, 0, 0);
        }
    }

    public Vector3 Right
    {
        get
        {
            return Center + new Vector3(boxCollider.bounds.extents.x, 0, 0);
        }
    }

    public BoxCollider2D BoxCollider
    {
        get
        {
            return boxCollider;
        }
    }

    public RaycastOrigins Raycast
    {
        get
        {
            return raycastOrigins;
        }
    }

    public void SetBoxCollider(Vector3 offset, Vector3 size)
    {
        boxCollider.offset = offset;
        boxCollider.size = size;
    }

    public void SetOriginalBoxCollider()
    {
        SetBoxCollider(originalOffset,originalSize);
    }
}
