﻿using UnityEngine;

public class RaycastOrigins
{
    private LayerMask collisionMask;

    private BoxCollider2D boxCollider;
    private bool boundsSet;
    private Bounds bounds;
    private int currentHorizontal;
    private int currentVertical;
    private float skinWidth = 0.015f;
    private float maximumRayDistance = 0.5f;

    private int horizontalRayCount, verticalRayCount;
    private int upIndex, downIndex, leftIndex, rightIndex;
    private float horizontalRaySpacing, verticalRaySpacing;

    public RaycastOrigins(BoxCollider2D collider, LayerMask collisionMask, float skinWidth, float maximumRayDistance)
    {
        BoxCollider = collider;
        CollisionMask = collisionMask;
        SkinWidth = skinWidth;
        MaximumRayDistance = maximumRayDistance;
        CalculateBounds();
    }

    public BoxCollider2D BoxCollider
    {
        get
        {
            return boxCollider;
        }
        set
        {
            boundsSet = false;
            boxCollider = value;
        }
    }

    public float SkinWidth
    {
        get
        {
            return skinWidth;
        }
        set
        {
            if (skinWidth != value)
            {
                boundsSet = false;
                skinWidth = value;
            }
        }
    }

    public float MaximumRayDistance
    {
        get
        {
            return maximumRayDistance;
        }
        set
        {
            if (maximumRayDistance != value)
            {
                boundsSet = false;
                maximumRayDistance = value;
            }
        }
    }

    public LayerMask CollisionMask
    {
        get
        {
            return collisionMask;
        }
        set
        {
            collisionMask = value;
        }
    }

    public Bounds Bounds
    {
        get
        {
            if (!boundsSet)
            {
                CalculateBounds();

                boundsSet = true;
            }
            return bounds;
        }
    }

    private void CalculateBounds()
    {
        bounds = BoxCollider.bounds;

        horizontalRayCount = (int)Mathf.Max(2, Mathf.Floor(bounds.size.y / maximumRayDistance + 1));
        verticalRayCount = (int)Mathf.Max(2, Mathf.Floor(bounds.size.x / maximumRayDistance + 1));

        bounds.Expand(skinWidth * -2f);

        horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1f);
        verticalRaySpacing = bounds.size.x / (verticalRayCount - 1f);
    }

    public Vector2 TopLeft
    {
        get
        {
            return new Vector2(Bounds.min.x, Bounds.max.y);
        }
    }

    public Vector2 TopRight
    {
        get
        {
            return new Vector2(Bounds.max.x, Bounds.max.y);
        }
    }

    public Vector2 BottomLeft
    {
        get
        {
            return new Vector2(Bounds.min.x, Bounds.min.y);
        }
    }

    public Vector2 BottomRight
    {
        get
        {
            return new Vector2(Bounds.max.x, Bounds.min.y);
        }
    }

    public RaycastHit2D CastRay(float index, Vector2 direction, float rayLength, float offset)
    {
        direction.x = Globals.SignZero(direction.x);
        direction.y = Globals.SignZero(direction.y);

        float rayCount = 0;
        Vector2 rayOrigin;

        if (direction.x < 0)
        {
            rayOrigin = BottomLeft;
            rayCount = horizontalRayCount;
        }
        else if (direction.x > 0)
        {
            rayOrigin = BottomRight;
            rayCount = horizontalRayCount;
        }
        else if (direction.y < 0)
        {
            rayOrigin = BottomLeft;
            rayCount = verticalRayCount;
        }
        else if (direction.y > 0)
        {
            rayOrigin = TopLeft;
            rayCount = verticalRayCount;
        }
        else
        {
            return new RaycastHit2D();
        }
        if (index < 0 || index >= rayCount)
        {
            return new RaycastHit2D();
        }

        if(direction.x!=0)
        {
            rayOrigin += Vector2.up * (horizontalRaySpacing * index + offset);
            direction.y = 0;
        }
        else if(direction.y!=0)
        {
            rayOrigin+=Vector2.right * (verticalRaySpacing * index + offset);
        }
        Debug.DrawRay(rayOrigin, direction, Color.red);

        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, direction, rayLength, CollisionMask);
        return hit;
    }

    public RaycastHit2D CastRay(float index, Vector2 direction, float rayLength)
    {
        return CastRay(index,direction,rayLength,0);
    }

    public int MaxRays(RayDirection direction)
    {
        switch (direction)
        {
            case RayDirection.up: return verticalRayCount;
            case RayDirection.down: return verticalRayCount;
            case RayDirection.left: return horizontalRayCount;
            case RayDirection.right: return horizontalRayCount;
            default: return 0;
        }
    }

    public int GetRayCount(Vector2 direction)
    {
        return MaxRays(VectorToRayDirection(direction));
    }



    public RaycastHit2D CastRayNormalized(float position, Vector2 direction, float rayLength, float offset)
    {
        return CastRay(position * Mathf.Max(0, GetRayCount(direction)-1), direction, rayLength, offset);
    }
    public RaycastHit2D CastRayNormalized(float position, Vector2 direction, float rayLength)
    {
        return CastRayNormalized(position, direction, rayLength, 0);
    }


    public RaycastHit2D CastRay(int index, RayDirection direction, float rayLength, float offset)
    {
        return CastRay(index, Vector2.up, rayLength, offset);
    }


    public RaycastHit2D NextRay(Vector2 direction, float rayLength, float offset)
    {
        return CastRay(Increase(direction),direction,rayLength,offset);
    }

    public RaycastHit2D NextRay(RayDirection direction, float rayLength, float offset)
    {
        return NextRay(RayDirectionToVector(direction),rayLength,offset);
    }


    public bool HasNextRay(RayDirection direction)
    {
        return Index(direction) < MaxRays(direction);
    }

    public bool HasNextRay(Vector2 direction)
    {
        return HasNextRay(VectorToRayDirection(direction));
    }


    public void ResetRay(RayDirection direction)
    {
        switch (direction)
        {
            case RayDirection.up: upIndex=0; break;
            case RayDirection.down: downIndex = 0;break ;
            case RayDirection.left: leftIndex = 0;break ;
            case RayDirection.right: rightIndex = 0;break ;
        }
    }

    public void ResetRay(Vector2 direction)
    {
        ResetRay(VectorToRayDirection(direction));
    }

    public void ResetRays()
    {
        ResetRay(RayDirection.up);
        ResetRay(RayDirection.down);
        ResetRay(RayDirection.left);
        ResetRay(RayDirection.right);
    }


    public int Index(RayDirection direction)
    {
        switch (direction)
        {
            case RayDirection.up: return upIndex;
            case RayDirection.down: return downIndex;
            case RayDirection.left: return leftIndex;
            case RayDirection.right: return rightIndex;
            default: return 0;
        }
    }
    public int Index(Vector2 direction)
    {
        return Index(VectorToRayDirection(direction));
    }


    public int Increase(RayDirection direction)
    {
        switch (direction)
        {
            case RayDirection.up: return upIndex++;
            case RayDirection.down: return downIndex++;
            case RayDirection.left: return leftIndex++;
            case RayDirection.right: return rightIndex++;
            default: return 0;
        }
    }
    public int Increase(Vector2 direction)
    {
        return Increase(VectorToRayDirection(direction));
    }


    public float RelativeRay(Vector2 direction)
    {
        return Index(direction) / (GetRayCount(direction) - 1f);
    }

    public float RelativeRay(RayDirection direction)
    {
        return RelativeRay(RayDirectionToVector(direction));
    }


    public RayDirection VectorToRayDirection(Vector2 direction)
    {
        if(direction.x < 0)
        {
            return RayDirection.left;
        }
        if (direction.x > 0)
        {
            return RayDirection.right;
        }
        if (direction.y < 0)
        {
            return RayDirection.down;
        }
        if (direction.y > 0)
        {
            return RayDirection.up;
        }
        return RayDirection.none;
    }

    public Vector2 RayDirectionToVector(RayDirection direction)
    {
        switch (direction)
        {
            case RayDirection.up: return Vector2.up;
            case RayDirection.down: return Vector2.down;
            case RayDirection.left: return Vector2.left;
            case RayDirection.right: return Vector2.right;
            default: return Vector2.zero;
        }
    }


    public enum RayDirection
    {
        none,
        up,
        down,
        left,
        right
    }
}