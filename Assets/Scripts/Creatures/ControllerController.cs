﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Anima2D;

[RequireComponent(typeof(Controller))]
public abstract class ControllerController : MaterialManipulater
{
    public Controller controller;
    public ControlManager controlManager;
    public GameObject spawnOnDestroy;

    public override void Start ()
    {
        base.Start();
        controller = GetComponent<Controller>();
        controlManager = Resources.FindObjectsOfTypeAll<ControlManager>().FirstOrDefault();

        if(!controlManager)
        {
            controlManager = new ControlManager();
        }
    }
}