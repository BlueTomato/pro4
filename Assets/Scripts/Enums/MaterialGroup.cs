﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MaterialGroup
{
    All,
    Background,
    Creature,
    Enemy,
    Player,
    Projectile,
    UI
}