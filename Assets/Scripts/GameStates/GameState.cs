﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameState : ScriptableObject
{
    private bool started;

    public virtual void Start()
    {
        started = true;
    }

    public virtual void FixedUpdate()
    {
        if(!started)
        {
            Start();
        }
    }

    public virtual void Update()
    {
        if (!started)
        {
            Start();
        }
    }
}