﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStatePlay : GameState
{
    MaterialProperties timeStop;
    MaterialProperties darkness;

    public override void FixedUpdate()
    {
    }

    public override void Start()
    {
        base.Start();

        timeStop = MaterialProperties.CreateInstance<MaterialProperties>();
        timeStop.groups.Add(MaterialGroup.Background);
        timeStop.groups.Add(MaterialGroup.Enemy);
        timeStop.Add("_TargetColor", new Color(0.6f, 0.6f, 1, 0.6f));
        timeStop.Add("_Multiply", 1);
        timeStop.Add("_Brightness", 0.8f);

        darkness = MaterialProperties.CreateInstance<MaterialProperties>();
        darkness.layer = 7;
        darkness.groups.Add( MaterialGroup.Background);
        darkness.groups.Add(MaterialGroup.Enemy);
        darkness.Add("_MaxColor", Color.black);
    }

    public override void Update()
    {
        base.Update();

        timeStop.weight = UIManager.Instance.lastWeight;

        if (!GameManager.Instance.IsFrozen)
        {
            if (GameManager.Instance.player.controlManager.GetAction(ControlManager.Action.Abilities))
            {
                GameManager.Instance.SuspendGame();
            }
            else
            {
                GameManager.Instance.UnsuspendGame();
            }
        }
        if (GameManager.Instance.IsPaused)
        {
            float brightness = 0;
            if (Time.unscaledTime < GameManager.Instance.timeToLoad)
            {
                brightness = (GameManager.Instance.timeToLoad - Time.unscaledTime) / CameraManager.Instance.transitionTime;
            }
            if (GameManager.Instance.sceneToLoad && Time.unscaledTime >= GameManager.Instance.timeToLoad)
            {
                CameraManager.Instance.frozen = false;
                SceneManager.LoadScene(GameManager.Instance.sceneToLoad.name);
                GameManager.Instance.sceneToLoad = null;
                GameManager.Instance.timeToUnpause = Time.unscaledTime + CameraManager.Instance.transitionTime * 3f;
            }
            else if (Time.unscaledTime >= GameManager.Instance.timeToUnpause)
            {
                CameraManager.Instance.frozen = false;
                brightness = 1;
                GameManager.Instance.UnpauseGame();
            }
            else if (Time.unscaledTime > GameManager.Instance.timeToUnpause - CameraManager.Instance.transitionTime)
            {
                CameraManager.Instance.frozen = true;
                brightness = 1f - ((GameManager.Instance.timeToUnpause - Time.unscaledTime) / CameraManager.Instance.transitionTime);
            }
            darkness.weight = 1f-brightness;
        }
    }
}
