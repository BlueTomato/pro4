﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameStateAbilities : GameState
{
    private bool started;

    public override void Start()
    {
        base.Start();
        GameManager.Instance.SuspendGame();
    }

    public override void Update()
    {
        
    }
}