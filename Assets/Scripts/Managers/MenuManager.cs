﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public abstract class MenuManager : SingletonScriptableObject<MenuManager>
{
    public MenuPoint menu;
    public Text text;

    public int Index
    {
        get
        {
            if(menu)
            {
                return menu.index;
            }
            return 0;
        }
    }

    public void Up()
    {
        menu.IndexUp();
    }

    public void Down()
    {
        menu.IndexDown();
    }

    public void Activate()
    {
        MenuPoint item=menu.Activate();
        if(item)
        {
            if(item!=this)
            {
                menu.Reset();
                menu = item;
            }
        }
        else if(!item.parent)
        {
            menu.Reset();
            Close();
        }
    }

    public string[] Print()
    {
        return menu.ListMenuPoints();
    }

    public void Open()
    {
        string[] points = Print();
        text.text = "";
        for(int i=0;i<points.Length;i++)
        {
            text.text += points[i] + "\n";
        }
    }

    public void Close()
    {
        text.text = "";
    }
}