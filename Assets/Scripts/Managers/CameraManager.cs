﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraManager : SingletonMonoBehaviour<CameraManager>
{
    public Vector2 playerPosition;

    public static float z = -10;

    private RaycastController focus;

    private Camera cam;
    private Camera UICam;
    private LineRenderer lineRenderer;

    public float transitionTime=1f;

    private float parallax = 0.5f;

    public bool frozen;

    private bool initialized=false;

    public List<CameraArea> cameraAreas=new List<CameraArea>();
    public List<bool> suppressing = new List<bool>();
    public List<float> weight = new List<float>();
    public List<Vector3> lastTarget=new List<Vector3>();

    public RaycastController Focus
    {
        get
        {
            if (!focus)
            {
                focus = GameManager.Instance.player.GetComponent<RaycastController>();
            }
            return focus;
        }
    }

    public Camera Camera
    {
        get
        {
            return GetCamera();
        }
    }

    public Camera UICamera
    {
        get
        {
            GetCamera();
            return UICam;
        }
    }

    public LineRenderer LineRenderer
    {
        get
        {
            return lineRenderer;
        }
    }

    public float Parallax
    {
        get
        {
            return parallax;
        }
    }

    public void Awake()
    {
        GetCamera();
    }

    public void Start()
    {
        gameObject.name = "Main Camera";
    }

    private void CreateCamera()
    {
        int canvasLayer = LayerMask.NameToLayer("Canvas");
        int uiLayer = LayerMask.NameToLayer(CorporateDesign.Instance.UILayer);
        int enemyLayer = LayerMask.NameToLayer("Enemy");
        int frontLayer = LayerMask.NameToLayer("Front");
        RenderTexture renderTexture = Resources.Load<RenderTexture>("ScreenRenderTexture");

        cam = gameObject.AddComponent<Camera>();

        gameObject.tag = "MainCamera";

        cam.orthographic = true;
        cam.orthographicSize = GameManager.Instance.OrthographicSize;
        cam.cullingMask = 1 << (PlayerSettings.Instance.player.layer);
        cam.cullingMask |= 1 << enemyLayer;
        cam.cullingMask |= 1 << canvasLayer;
        cam.cullingMask |= 1 << uiLayer;
        cam.cullingMask |= 1 << frontLayer;
        cam.cullingMask = ~cam.cullingMask;
        cam.targetTexture = renderTexture;
        cam.depth = 0;
        cam.nearClipPlane = 0;
        cam.farClipPlane = 1000;
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = new Color(0, 0, 0, 0);

        Camera playerCam = Instantiate(cam);
        playerCam.name = "Player Camera";
        playerCam.depth = 1;
        playerCam.gameObject.layer = GameManager.Instance.player.gameObject.layer;
        playerCam.clearFlags = CameraClearFlags.Depth;
        playerCam.cullingMask = 1 << GameManager.Instance.player.gameObject.layer;

        Camera enemyCam = Instantiate(cam);
        enemyCam.name = "Enemy Camera";
        enemyCam.gameObject.layer = enemyLayer;
        enemyCam.depth = 2;
        enemyCam.clearFlags = CameraClearFlags.Depth;
        enemyCam.cullingMask = 1 << enemyLayer;

        Camera foregroundCam = Instantiate(cam);
        foregroundCam.name = "Foreground Camera";
        foregroundCam.depth = 3;
        foregroundCam.clearFlags = CameraClearFlags.Depth;
        foregroundCam.farClipPlane = -CameraManager.z;
        //foregroundCam.GetComponent<Transform>().position = new Vector3(0, 0, 100);

        Camera frontCam = Instantiate(cam);
        frontCam.name = "Front Cam";
        frontCam.gameObject.layer = frontLayer;
        frontCam.depth = 4;
        frontCam.clearFlags = CameraClearFlags.Depth;
        frontCam.cullingMask=1 << frontLayer;

        UICam = Instantiate(cam);
        lineRenderer=UICam.gameObject.AddComponent<LineRenderer>();
        lineRenderer.material.shader = CorporateDesign.Instance.spriteShader;
        lineRenderer.useWorldSpace = false;

        UICam.name = "UI Cam";
        UICam.gameObject.layer = uiLayer;
        UICam.cullingMask = 1 << uiLayer;
        UICam.depth = 5;
        UICam.clearFlags = CameraClearFlags.Depth;

        playerCam.transform.SetParent(cam.transform);
        enemyCam.transform.SetParent(cam.transform);
        foregroundCam.transform.SetParent(cam.transform);
        frontCam.transform.SetParent(cam.transform);
        UICam.transform.SetParent(cam.transform);

        playerCam.transform.localPosition = new Vector3(0, 0, CameraManager.z);
        enemyCam.transform.localPosition = new Vector3(0, 0, CameraManager.z);
        foregroundCam.transform.localPosition = new Vector3(0, 0, CameraManager.z);
        frontCam.transform.localPosition = new Vector3(0, 0, CameraManager.z);
        UICam.transform.localPosition = new Vector3(0, 0, CameraManager.z);

        cam.gameObject.AddComponent<AudioListener>();

        GameObject projection = GameObject.CreatePrimitive(PrimitiveType.Quad);
        projection.name = "Projection";
        projection.layer = canvasLayer;
        projection.transform.localScale = new Vector3(16, 9, 1);
        projection.GetComponent<Renderer>().sharedMaterial = Resources.Load<Material>("Screen");
        projection.transform.SetParent(cam.transform);

        Camera projectionCam = new GameObject("Projection Camera").AddComponent<Camera>();
        projectionCam.orthographic = true;
        projectionCam.cullingMask = 1 << canvasLayer;
        projectionCam.orthographicSize = 4.5f;
        projectionCam.transform.SetParent(projection.transform);
        projectionCam.transform.localPosition = new Vector3(0, 0, -projection.transform.childCount);
    }

    public Camera GetCamera()
    {
        if(CameraManager.Instance!=this)
        {
            Destroy(this);
            return null;
        }
        if (!cam)
        {
            CreateCamera();
        }
        return cam;
    }

    public void Suppress(CameraArea cameraArea)
    {
        if(!cameraAreas.Contains(cameraArea))
        {
            cameraAreas.Add(cameraArea);
            lastTarget.Add(Vector3.zero);
            if(!initialized)
            {
                weight.Add(1);
            }
            else
            {
                weight.Add(0);
            }
            suppressing.Add(true);
        }
        else
        {
            suppressing[cameraAreas.IndexOf(cameraArea)] = true;
        }
    }

    public bool IsSuppressed()
    {
        return cameraAreas.Count > 0 || lastTarget.Count>0;
    }

    public bool IsSuppressed(CameraArea cameraArea)
    {
        return cameraAreas.Contains(cameraArea);
    }

    public float SuppressionWeight(CameraArea cameraArea)
    {
        if(cameraAreas.Contains(cameraArea))
        {
            return weight[cameraAreas.IndexOf(cameraArea)];
        }
        return 0;
    }

    void LateUpdate()
    {
        UpdateCamera();
    }

    public void UpdateCamera()
    {
        initialized = true;

        playerPosition = Focus.PersistentCenter;
        Vector2 camTarget = Focus.PersistentCenter;

        for(int i=0;i<cameraAreas.Count;i++)
        {
            if(cameraAreas[i])
            {
                lastTarget[i] = cameraAreas[i].GetTarget(playerPosition);
            }
            if(!cameraAreas[i] || !cameraAreas[i].IsWithin(playerPosition))
            {
                suppressing[i] = false;
            }

            if (!frozen)
            {
                float time = Time.unscaledDeltaTime;
                if (!suppressing[i])
                {
                    time *= -1f;
                }
                weight[i] = Mathf.Clamp01(weight[i] + time);
            }

            if (!suppressing[i] && weight[i]<=0)
            {
                cameraAreas.RemoveAt(i);
                weight.RemoveAt(i);
                suppressing.RemoveAt(i);
                lastTarget.RemoveAt(i);
                i--;
            }
        }

        if (IsSuppressed())
        {
            Vector2 target = Vector2.zero;
            float fullWeight = 0;

            for (int i = 0; i < cameraAreas.Count; i++)
            {
                fullWeight += weight[i];
            }
            for (int i = 0; i < cameraAreas.Count; i++)
            {
                target+=(Vector2)lastTarget[i]*Mathf.SmoothStep(0,1,weight[i]/Mathf.Max(1f,fullWeight));
            }

            camTarget += target;
        }

        cam.transform.position = GameManager.Instance.PixelPerfect((Vector2)GameManager.Instance.GetEarthquakeOffset(camTarget) + camTarget);
    }

    public void SceneTransition(bool sceneTransition)
    {

    }
}
