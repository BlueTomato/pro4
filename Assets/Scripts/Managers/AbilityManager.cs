﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AbilityManager : SingletonScriptableObject<AbilityManager>
{
    public Ability[] enableFromStart;
    private Ability[] abilities;
    private int index = 0;
    private Ability jumpTo;

    void OnEnable()
    {
        if(Instance==this)
        {
            Ability.FillArray();
            for (int i = 0; i < Ability.array.Length; i++)
            {
                Ability.array[i].enabled = false;
            }

            for (int i = 0; i < enableFromStart.Length; i++)
            {
                enableFromStart[i].enabled = true;
            }
        }
    }

    public void Start()
    {
        SetAbilities();
    }

    public void SetAbilities()
    {
        SortedDictionary<String, Ability> all = Ability.List;

        abilities = Ability.GetEnabled();

        for (int i = abilities.Length; i > 1; i--)
        {
            for (int j = 0; j < i - 1; j++)
            {
                if (abilities[j].index > abilities[j + 1].index)
                {
                    Ability toSwap = abilities[j];
                    abilities[j] = abilities[j + 1];
                    abilities[j + 1] = toSwap;
                }
            }
        }

        UIManager.Instance.DepictAbilities();
    }

    public Ability[] Abilities
    {
        get
        {
            return abilities;
        }
    }

    public void JumpTo(Ability ability)
    {
        jumpTo = ability;
    }

    void FixIndex()
    {
        index = (int)Mathf.Repeat(index, abilities.Length);
    }

    public void Update()
    {
        UIManager.Instance.SetDescription("");
        if (UIManager.Instance.Canvas)
        {
            for (int i = 0; i < abilities.Length; i++)
            {
                if (abilities[i].NonBinary)
                {
                    UIManager.Instance.Set(i, true);
                    UIManager.Instance.SetSetting(i, abilities[i].Setting, abilities[i].maxSetting);
                }
                else
                {
                    UIManager.Instance.Set(i, abilities[i].IsEnabledAndActive);
                }
            }
            if (GameManager.Instance.IsSuspended)
            {
                if (!GameManager.Instance.IsFrozen)
                {
                    if (GameManager.Instance.player.controlManager.GetLeftDown())
                    {
                        index--;
                    }
                    if (GameManager.Instance.player.controlManager.GetRightDown())
                    {
                        index++;
                    }

                    FixIndex();

                    if (GameManager.Instance.player.controlManager.GetUpDown() || GameManager.Instance.player.controlManager.GetActionDown(ControlManager.Action.Jump))
                    {
                        abilities[index].Toggle();
                    }
                    if (GameManager.Instance.player.controlManager.GetDownDown())
                    {
                        abilities[index].ToggleDown();
                    }

                    if (abilities[index].name == "Destroy" && abilities[index].IsActive)
                    {
                        abilities[index].Deactivate();
                        Projectile[] projectiles = Projectile.FindObjectsOfType<Projectile>();
                        for (int i = 0; i < projectiles.Length; i++)
                        {
                            Destroy(projectiles[i].gameObject);
                        }
                    }
                }

                if (jumpTo)
                {
                    for (int i = 0; i < abilities.Length && abilities[index] != jumpTo; i++)
                    {
                        index++;
                        FixIndex();
                    }
                    jumpTo = null;
                }

                UIManager.Instance.SetHighlighted(index);
                UIManager.Instance.SetTitle(abilities[index].name);
                UIManager.Instance.SetDescription(abilities[index].description);
            }
        }
    }
}
