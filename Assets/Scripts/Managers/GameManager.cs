﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    public PlayerInput player;
    public UIManager canvas;
    public CameraManager cameraManager;

    public GameState gameState;

    public GameStatePlay gameStatePlay;
    public GameStateAbilities gameStateAbilities;
    /*public GameStatePause gameStatePause;
    public GameStateControls gameStateControls;*/

    private bool suspended;
    private float suspendedTime;
    private float unsuspendedTime;

    private bool paused;
    private float pausedTime;
    private float unpausedTime;

    public int pixelsPerUnit = 16;

    private float orthographicSize = 7.03125f;
    private Vector2 resolution = new Vector2(400,225);

    private List<Earthquake> earthquakes = new List<Earthquake>();

    public float frozenUntil;

    public SceneAsset sceneToLoad;
    public float timeToLoad;
    public float timeToUnpause;

    public Vector2 Resolution
    {
        get
        {
            return resolution;
        }
    }

    public float PixelsPerUnit
    {
        get
        {
            return pixelsPerUnit;
        }
    }

    public float SmallestInstance
    {
        get
        {
            return 1f/PixelsPerUnit;
        }
    }

    public float OrthographicSize
    {
        get
        {
            return orthographicSize;
        }
    }

    public float ScreenWidth
    {
        get
        {
            return OrthographicSize * (16f / 9f) * 2f;
        }
    }

    public float ScreenHeight
    {
        get
        {
            return OrthographicSize * 2f;
        }
    }


    public float PixelPerfect(float value)
    {
        return Mathf.Round(value*pixelsPerUnit)*SmallestInstance;
    }

    public Vector2 PixelPerfect(Vector2 position)
    {
        return new Vector2(PixelPerfect(position.x), PixelPerfect(position.y));
    }

    public Vector3 PixelPerfect(Vector3 position)
    {
        return new Vector3(PixelPerfect(position.x), PixelPerfect(position.y), PixelPerfect(position.z));
    }


    void Start()
    {
        if(Instance!=this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Resources.LoadAll("");

            if (!player)
            {
                player = Instantiate(PlayerSettings.Instance.player).GetComponent<PlayerInput>();
            }
            if (!cameraManager)
            {
                cameraManager = CameraManager.Instance;
            }
            if (!canvas)
            {
                canvas = UIManager.Instance;
            }

            DontDestroyOnLoad(player);
            DontDestroyOnLoad(canvas);
            DontDestroyOnLoad(cameraManager);
            DontDestroyOnLoad(gameObject);

            Application.targetFrameRate = 60;

            gameStatePlay = GameStatePlay.CreateInstance<GameStatePlay>();
            gameState = gameStatePlay;

            AbilityManager.Instance.SetAbilities();
        }
    }

    void Update()
    {
        gameState.Update();
        AbilityManager.Instance.Update();
    }

    void LateUpdate()
    {
        MaterialManager.Instance.ApplyProperties();
    }

    void OnDestroy()
    {
        if(GameManager.Instance==this)
        {
            MaterialManager.Instance.OnDestroy();
        }
    }

    public void LoadSceneWithTransition(SceneAsset scene)
    {
        CameraManager.Instance.frozen = true;
        sceneToLoad = scene;
        timeToLoad = Time.unscaledTime+CameraManager.Instance.transitionTime;
        timeToUnpause = timeToLoad + CameraManager.Instance.transitionTime;
        PauseGame();
    }

    public void UnsuspendGame()
    {
        if (IsSuspended)
        {
            Time.timeScale = 1;
            suspended = false;
            unsuspendedTime = Time.unscaledTime;
        }
    }

    public void SuspendGame()
    {
        if(!IsSuspended && !IsPaused)
        {
            Time.timeScale = 0;
            suspended = true;
            suspendedTime = Time.unscaledTime;
        }
    }

    public void SuspendGameFor(float length)
    {
        SuspendGame();
        frozenUntil = Time.unscaledTime + length;
    }

    public bool IsFrozen
    {
        get
        {
            return Time.unscaledTime < frozenUntil;
        }
    }

    public float SuspendedFor
    {
        get
        {
            return Time.unscaledTime - suspendedTime;
        }
    }

    public float UnsuspendedFor
    {
        get
        {
            return Time.unscaledTime - unsuspendedTime;
        }
    }

    public float LastSuspension
    {
        get
        {
            return suspendedTime;
        }
    }

    public float LastUnsuspension
    {
        get
        {
            return unsuspendedTime;
        }
    }

    public float SuspensionLength
    {
        get
        {
            return unsuspendedTime - suspendedTime;
        }
    }

    public bool IsSuspended
    {
        get
        {
            return suspended;
        }
    }

    public void UnpauseGame()
    {
        if (IsPaused)
        {
            Time.timeScale = 1;
            paused = false;
            unpausedTime = Time.unscaledTime;
        }
    }

    public void PauseGame()
    {
        if (!IsPaused && !IsSuspended)
        {
            Time.timeScale = 0;
            paused = true;
            pausedTime = Time.unscaledTime;
        }
    }

    public float PausedFor
    {
        get
        {
            return Time.unscaledTime - pausedTime;
        }
    }

    public float UnpausedFor
    {
        get
        {
            return Time.unscaledTime - unpausedTime;
        }
    }

    public float LastPause
    {
        get
        {
            return pausedTime;
        }
    }

    public float LastUnpause
    {
        get
        {
            return unpausedTime;
        }
    }

    public float PauseLength
    {
        get
        {
            return unpausedTime - pausedTime;
        }
    }

    public bool IsPaused
    {
        get
        {
            return paused;
        }
    }

    public bool IsPausedOrSuspended
    {
        get
        {
            return IsPaused || IsSuspended;
        }
    }



    public Vector3 GetEarthquakeOffset(Vector3 position)
    {
        Vector3 offset = Vector3.zero;
        if(!IsPausedOrSuspended)
        {
            Earthquake[] eq = this.earthquakes.ToArray();
            for (int i = 0; i < eq.Length; i++)
            {
                if (eq[i].toBeDestroyed)
                {
                    earthquakes.Remove(eq[i]);
                }
                else
                {
                    offset += eq[i].Movement(position);
                }
            }
        }
        return offset;
    }

    public void StartEarthquake(float length, float intensity)
    {
        StartEarthquake(length, intensity, Vector3.zero, float.MaxValue);
    }

    public void StartEarthquake(float length, float intensity, Vector3 position, float range)
    {
        earthquakes.Add(new Earthquake(position, length, intensity, range));
    }

    public class Earthquake
    {
        private Vector3 position;
        private float end;
        private float time;
        private float intensity;
        private float range;
        public bool toBeDestroyed = false;

        public Earthquake(Vector3 position, float time, float intensity, float range)
        {
            this.position = position;
            this.time = time;
            end = Time.time + time;
            this.intensity = intensity;
            this.range = range;
        }

        public Vector3 Movement(Vector3 reference)
        {
            if (Time.time >= end)
            {
                toBeDestroyed = true;
                return Vector3.zero;
            }
            return new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)) * intensity * Mathf.Clamp((end - Time.time) / time, 0, 1) * (Mathf.Clamp(range - Vector3.Distance(position, reference), 0, range) / range);
        }
    }
}