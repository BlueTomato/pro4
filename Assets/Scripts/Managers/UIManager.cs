﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIManager : SingletonMonoBehaviour<UIManager>
{
    public float energyBarStart;
    public float previousEnergyBarTarget;

    public Color active = CorporateDesign.Instance.red;
    public Color inactive = new Color(0.2f, 0.2f, 0.2f);

    private Image[] images=new Image[0];
    private Text[] texts = new Text[0];
    private RectTransform ui;
    private Text title;
    private Text description;
    private Image descriptionBackground;
    private RectTransform iconContainer;

    private Text energyText;
    private Image energyBarContainer;
    private Image energyBar;
    private Image minEnergyBar;
    private Image minShot;

    private Canvas canvas;

    float time = -1f;

    public float lastWeight;

    void Start()
    {
        gameObject.name = "UI Canvas";
        DepictAbilities();
    }

    public Canvas Canvas
    {
        get
        {
            return canvas;
        }
    }

    public void DepictAbilities()
    {
        if (!canvas)
        {
            canvas = gameObject.AddComponent<Canvas>();
            gameObject.layer = LayerMask.NameToLayer(CorporateDesign.Instance.UILayer);
            CanvasScaler scaler = gameObject.AddComponent<CanvasScaler>();
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            scaler.referencePixelsPerUnit = GameManager.Instance.PixelsPerUnit;
            scaler.referenceResolution = GameManager.Instance.Resolution;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = CameraManager.Instance.UICamera;
            canvas.pixelPerfect = true;

            Image uiImage = new GameObject("UI").AddComponent<Image>();
            uiImage.gameObject.layer = gameObject.layer;
            uiImage.sprite = CorporateDesign.Instance.UIImage;

            ui = uiImage.rectTransform;
            ui.transform.SetParent(canvas.transform);
            ui.sizeDelta = new Vector2(400, 28);
            ui.pivot = new Vector2(0.5f, 1);
            ui.anchoredPosition = new Vector2(0, (int)(GameManager.Instance.Resolution.y / 2f));

            descriptionBackground = new GameObject("TextBox").AddComponent<Image>();
            descriptionBackground.gameObject.layer = gameObject.layer;
            descriptionBackground.transform.SetParent(ui);
            descriptionBackground.transform.localScale = Vector3.one;
            descriptionBackground.color = new Color(0, 0, 0, 0.75f);
            descriptionBackground.rectTransform.pivot = new Vector2(0.5f, 1);
            descriptionBackground.transform.localPosition = new Vector3(0, -ui.sizeDelta.y - CorporateDesign.Instance.iconSize.y, 0);

            title = new GameObject("Title").AddComponent<Text>();
            title.text = "Description";
            title.font = CorporateDesign.Instance.mainFont;
            title.gameObject.layer = gameObject.layer;
            title.fontStyle = FontStyle.Bold;
            title.alignment = TextAnchor.MiddleCenter;
            title.transform.SetParent(descriptionBackground.transform);
            title.transform.localScale = Vector3.one;
            title.rectTransform.pivot = new Vector3(0.5f, 1);
            Canvas.ForceUpdateCanvases();
            float height = title.cachedTextGenerator.GetPreferredHeight(title.text, title.GetGenerationSettings(title.rectTransform.sizeDelta));
            title.rectTransform.sizeDelta = new Vector2(ui.sizeDelta.x / 4f * 3f, height);

            description = Instantiate(title);
            description.gameObject.name = "Description";
            description.fontStyle = FontStyle.Normal;
            description.alignment = TextAnchor.MiddleLeft;
            description.transform.SetParent(title.transform);
            description.rectTransform.anchoredPosition = new Vector2(0, -title.rectTransform.sizeDelta.y / 2f);

            energyText = new GameObject("EnergyText").AddComponent<Text>();
            energyText.text = "000";
            energyText.fontStyle = FontStyle.Bold;
            energyText.fontSize = 10;
            energyText.font = CorporateDesign.Instance.mainFont;
            energyText.color = CorporateDesign.Instance.red;
            energyText.transform.SetParent(ui);
            Canvas.ForceUpdateCanvases();
            height = energyText.cachedTextGenerator.GetPreferredHeight(energyText.text, description.GetGenerationSettings(energyText.rectTransform.sizeDelta));
            float width = energyText.cachedTextGenerator.GetPreferredWidth(energyText.text, description.GetGenerationSettings(energyText.rectTransform.sizeDelta));
            energyText.rectTransform.sizeDelta = new Vector2(width, height);
            energyText.horizontalOverflow = HorizontalWrapMode.Overflow;
            energyText.verticalOverflow = VerticalWrapMode.Overflow;
            energyText.alignment = TextAnchor.MiddleRight;
            energyText.rectTransform.pivot = new Vector2(0, 0.5f);
            energyText.rectTransform.anchoredPosition = new Vector2(-ui.sizeDelta.x / 2f, 0);

            energyBarContainer = new GameObject("EnergyBarContainer").AddComponent<Image>();
            energyBarContainer.transform.SetParent(ui);
            energyBarContainer.color = Color.black;
            energyBarContainer.rectTransform.pivot = new Vector2(0, 0);
            energyBarContainer.rectTransform.sizeDelta = new Vector2(ui.sizeDelta.x, 3f);
            energyBarContainer.rectTransform.anchoredPosition3D = -ui.sizeDelta / 2f;

            energyBar = Instantiate(energyBarContainer);
            energyBar.gameObject.name = "EnergyBar";
            energyBar.transform.SetParent(energyBarContainer.transform);
            energyBar.rectTransform.anchoredPosition = -energyBar.rectTransform.sizeDelta / 2f;
            energyBar.color = CorporateDesign.Instance.red;
            energyBar.rectTransform.anchoredPosition3D = energyBar.rectTransform.anchoredPosition3D + new Vector3(0, 0, 0.1f);

            minEnergyBar = Instantiate(energyBar);
            minEnergyBar.gameObject.name = "MinEnergyBar";
            minEnergyBar.transform.SetParent(energyBarContainer.transform);
            minEnergyBar.rectTransform.anchoredPosition = energyBar.rectTransform.anchoredPosition;
            minEnergyBar.color = new Color(0, 0, 0, 0.25f);
            minEnergyBar.rectTransform.anchoredPosition3D = minEnergyBar.rectTransform.anchoredPosition3D + new Vector3(0, 0, 0.2f);

            minShot = Instantiate(energyBar);
            minShot.gameObject.name = "MinShot";
            minShot.transform.SetParent(energyBarContainer.transform);
            minShot.rectTransform.anchoredPosition = energyBar.rectTransform.anchoredPosition;
            minShot.color = new Color(1, 1, 1, 0.25f);
            minShot.rectTransform.anchoredPosition3D = minShot.rectTransform.anchoredPosition3D + new Vector3(0, 0, 0.3f);
        }

        if (iconContainer)
        {
            Destroy(iconContainer.gameObject);
        }
        iconContainer = new GameObject("Icons").AddComponent<RectTransform>();
        iconContainer.gameObject.layer = gameObject.layer;
        iconContainer.transform.SetParent(ui.transform);
        iconContainer.pivot = new Vector2(0.5f, 1);
        iconContainer.anchoredPosition3D = new Vector3(0, 0, 0.5f);

        Ability[] abilities = AbilityManager.Instance.Abilities;

        images = new Image[abilities.Length];
        texts = new Text[abilities.Length];

        for (int i = 0; i < abilities.Length; i++)
        {
            Image image = new GameObject(abilities[i].name).AddComponent<Image>();
            image.gameObject.layer = gameObject.layer;
            images[i] = image;
            image.sprite = abilities[i].image;
            image.rectTransform.sizeDelta = CorporateDesign.Instance.iconSize;
            image.transform.SetParent(iconContainer.transform);
            image.rectTransform.pivot = new Vector2(0,0.5f);
            image.rectTransform.anchoredPosition = new Vector3(GetIconPosition(i),0,0);
            image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,CorporateDesign.Instance.iconSize.x);
            image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, CorporateDesign.Instance.iconSize.y);

            GameObject miniText = new GameObject(abilities[i].name);
            miniText.transform.SetParent(image.transform);
            Text txt = miniText.AddComponent<Text>();
            txt.font = CorporateDesign.Instance.mainFont;
            txt.alignment = TextAnchor.MiddleCenter;
            txt.fontStyle = FontStyle.Bold;
            txt.fontSize = 12;
            txt.rectTransform.anchoredPosition = Vector2.zero;
            texts[i] = txt;
        }

        iconContainer.sizeDelta = new Vector2(GetAbilityBarWidth(), CorporateDesign.Instance.iconSize.y);

        ui.localScale = Vector3.one;
    }

    public Vector2 CurrentIconSize()
    {
        return Vector2.Lerp(CorporateDesign.Instance.smallIconSize, CorporateDesign.Instance.iconSize,lastWeight);
    }

    public void SetSetting(int index, int setting, int maxSetting)
    {
        texts[index].text = (setting+1) + "/" + (maxSetting+1);
    }

    public void SetTitle(String text)
    {
        if (text != "")
        {
            title.text = text;
        }
    }

    public void SetDescription(String text)
    {
        if(text!="")
        {
            description.text = text;

            Canvas.ForceUpdateCanvases();
            float height = description.cachedTextGenerator.GetPreferredHeight(text, description.GetGenerationSettings(description.rectTransform.sizeDelta));
            Canvas.ForceUpdateCanvases();
            description.rectTransform.sizeDelta = new Vector2(description.rectTransform.sizeDelta.x,height);
            descriptionBackground.rectTransform.sizeDelta=new Vector2(descriptionBackground.rectTransform.sizeDelta.x, title.rectTransform.sizeDelta.y+height + description.fontSize);
            description.rectTransform.anchoredPosition = new Vector2(0, -title.rectTransform.sizeDelta.y/2f);
            title.rectTransform.anchoredPosition = new Vector2(0,(descriptionBackground.rectTransform.sizeDelta.y-description.fontSize/2f)/2f);
        }
    }

    public void SetHighlighted(int index)
    {
        images[index].color = Color.Lerp(images[index].color, Color.white,0.6f);
    }

    public void Set(int index, bool active)
    {
        if(active)
        {
            SetActive(index);
        }
        else
        {
            SetInActive(index);
        }
    }

    public void SetActive(int index)
    {
        images[index].color = active;
    }

    public void SetInActive(int index)
    {
        images[index].color = inactive;
    }

    public float GetIconPosition(int index)
    {
        return (-GetAbilityBarWidth()/2f)+CorporateDesign.Instance.iconSize.x * index;
    }

    public float GetIconOffset(int index)
    {
        return index * CorporateDesign.Instance.iconSize.x;
    }

    public float GetAbilityBarWidth()
    {
        return AbilityManager.Instance.Abilities.Length * CorporateDesign.Instance.iconSize.x;
    }

    public float GetScaledAbilityBarWidth()
    {
        return GetAbilityBarWidth()/CorporateDesign.Instance.iconSize.x * CorporateDesign.Instance.smallIconSize.x;
    }

    public void Update()
    {
        if(GameManager.Instance.IsSuspended)
        {
            lastWeight += Time.unscaledDeltaTime*2f;
        }
        else
        {
            lastWeight -= Time.unscaledDeltaTime * 2f;
        }

        lastWeight = Mathf.Clamp(lastWeight,0f,1f);

        float weight = Mathf.SmoothStep(0f, 1f, lastWeight);

        Vector3 pos;
        Vector3 posEnd;

        pos = new Vector3((ui.sizeDelta.x- GetScaledAbilityBarWidth())/2f-4,iconContainer.sizeDelta.y/2f-4,0);
        posEnd = new Vector3(0, -ui.sizeDelta.y/2f,0);
        iconContainer.anchoredPosition = Vector3.Lerp(pos,posEnd,weight);

        Vector3 startSize = Vector3.one / CorporateDesign.Instance.iconSize.x * CorporateDesign.Instance.smallIconSize.x;

        iconContainer.localScale = Vector3.Lerp(startSize,Vector3.one,weight);

        pos = new Vector3(0, descriptionBackground.rectTransform.sizeDelta.y);
        posEnd = new Vector3(ui.sizeDelta.x, pos.y);
        descriptionBackground.rectTransform.sizeDelta = Vector3.Lerp(pos, posEnd, weight);

        title.transform.localScale = new Vector3(1,weight,1);

        /*
        Camera cam = CameraManager.Instance.Camera;
        float aspect = cam.aspect;

        Matrix4x4 targetMatrix0 = Matrix4x4.Ortho(-cam.orthographicSize * aspect, cam.orthographicSize * aspect, -cam.orthographicSize, cam.orthographicSize, cam.nearClipPlane, cam.farClipPlane);
        Matrix4x4 targetMatrix1 = Matrix4x4.Perspective(cam.fieldOfView, cam.aspect, cam.nearClipPlane, cam.farClipPlane);
        

        cam.projectionMatrix = Globals.MatrixLerp(targetMatrix0, targetMatrix1, weight);
        */

        if (GameManager.Instance.player)
        {
            float energyBarTarget = GameManager.Instance.player.controller.health.Get;
            float currentEnergyBar = Mathf.Lerp(energyBarStart, previousEnergyBarTarget, Mathf.Clamp(Time.time-time,0f,1f));

            if (energyBarTarget != previousEnergyBarTarget)
            {
                energyBarStart = currentEnergyBar;
                previousEnergyBarTarget = energyBarTarget;
            }

            energyText.text = ((int)currentEnergyBar).ToString();
            
            energyBar.transform.localScale = new Vector3(GameManager.Instance.player.controller.health.RelativeHealth,1,1);
            /*if (CanvasMap.Instance.energyLoss)
            {
                float percentage = Mathf.Clamp(1f - (energyBarTarget / (currentEnergyBar + float.Epsilon)), 0, 1);
                CanvasMap.Instance.energyLoss.transform.localScale = new Vector3(percentage, CanvasMap.Instance.energyLoss.transform.localScale.y, CanvasMap.Instance.energyLoss.transform.localScale.z);
            }
            if (CanvasMap.Instance.energyGain)
            {
                float percentage = Mathf.Clamp((energyBarTarget - currentEnergyBar) / (currentEnergyBar + float.Epsilon), 0, 1);
                CanvasMap.Instance.energyGain.transform.localScale = new Vector3(percentage, CanvasMap.Instance.energyGain.transform.localScale.y, CanvasMap.Instance.energyGain.transform.localScale.z);
            }*/

            minEnergyBar.transform.localScale = new Vector3(GameManager.Instance.player.controller.health.RelationMinToMaxHealth, 1, 1);
            minShot.transform.localScale = new Vector3(GameManager.Instance.player.minShot/ GameManager.Instance.player.controller.health.maxHealth, 1, 1);

            /*
            if (CanvasMap.Instance.replenishibleEnergy)
            {
                CanvasMap.Instance.replenishibleEnergy.transform.localScale = new Vector3((GameManager.Instance.player.controller.health.Get + GameManager.Instance.player.controller.health.recoverableHealth) / GameManager.Instance.player.controller.health.maxHealth, CanvasMap.Instance.replenishibleEnergy.transform.localScale.y, CanvasMap.Instance.replenishibleEnergy.transform.localScale.z);
            }
            if (CanvasMap.Instance.minShot)
            {
                CanvasMap.Instance.minShot.transform.localScale = new Vector3(15f / GameManager.Instance.player.controller.health.maxHealth, CanvasMap.Instance.minShot.transform.localScale.y, CanvasMap.Instance.minShot.transform.localScale.z);
            }
            */
        }
    }
}
