﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ControlManager : ScriptableObjectEnum<ControlManager>
{
    public KeyCode kbdUp = KeyCode.UpArrow;
    public KeyCode kbdLeft = KeyCode.LeftArrow;
    public KeyCode kbdDown = KeyCode.DownArrow;
    public KeyCode kbdRight = KeyCode.RightArrow;
    public KeyCode kbdJump = KeyCode.F;
    public KeyCode kbdStrike = KeyCode.D;
    public KeyCode kbdShoot = KeyCode.S;
    public KeyCode kbdAbilities = KeyCode.A;
    public KeyCode kbdPause = KeyCode.Escape;

    public Action gpdA = Action.Strike;
    public Action gpdB = Action.Shoot;
    public Action gpdX = Action.Jump;
    public Action gpdY = Action.Abilities;
    public Action gpdL = Action.Strike;
    public Action gpdR = Action.Shoot;
    public Action gpdSelect = Action.Pause;
    public Action gpdStart = Action.Pause;

    private bool previousUp;
    private bool previousLeft;
    private bool previousDown;
    private bool previousRight;

    private int index = 0;

    private bool setting = false;

    public float sensitivity = 0.5f;

    public override void OnEnable()
    {
        List.Add(this.name, this);
        index = 0;
        StopSet();
        previousUp = false;
        previousLeft = false;
        previousDown = false;
        previousRight = false;
    }

    public static KeyCode GetKey()
    {
        System.Array values = System.Enum.GetValues(typeof(KeyCode));
        foreach (KeyCode code in values)
        {
            if (Input.GetKeyDown(code))
            {
                return code;
            }
        }
        return KeyCode.None;
    }

    public static KeyCode GetKeyBoardKey()
    {
        KeyCode code = GetKey();
        if (code < KeyCode.JoystickButton0 || code > KeyCode.Joystick8Button19)
        {
            return code;
        }
        return KeyCode.None;
    }

    public static KeyCode GetGamePadKey(int gamePadNumber)
    {
        KeyCode code = GetKey();
        if (code >= KeyCode.JoystickButton0 + (gamePadNumber * 20) && code <= KeyCode.Joystick8Button19 + (gamePadNumber * 20))
        {
            return code;
        }
        return KeyCode.None;
    }

    public static KeyCode GetGamePadKey()
    {
        KeyCode code = GetKey();
        if (code >= KeyCode.JoystickButton0 && code <= KeyCode.Joystick8Button19)
        {
            return code;
        }
        return KeyCode.None;
    }

    public bool GetGamePadUp()
    {
        return Input.GetAxis("D-Pad Y") > sensitivity || Input.GetAxis("Left Stick Y") > sensitivity;
    }

    public bool GetGamePadUpDown()
    {
        bool current = GetGamePadUp();
        if (!previousUp && current)
        {
            previousUp = true;
            return true;
        }
        if (!current)
        {
            previousUp = false;
        }
        return false;
    }

    public bool GetUpDown()
    {
        return Input.GetKeyDown(kbdUp) || GetGamePadUpDown();
    }

    public bool GetUp()
    {
        return Input.GetKey(kbdUp) || GetGamePadUp();
    }

    public bool GetUpOut()
    {
        return !Input.GetKey(kbdUp) && !GetGamePadUp();
    }

    public bool GetGamePadDown()
    {
        return Input.GetAxis("D-Pad Y") < -sensitivity || Input.GetAxis("Left Stick Y") < -sensitivity;
    }

    public bool GetGamePadDownDown()
    {
        bool current = GetGamePadDown();
        if (!previousDown && current)
        {
            previousDown = true;
            return true;
        }
        if (!current)
        {
            previousDown = false;
        }
        return false;
    }

    public bool GetDownDown()
    {
        return Input.GetKeyDown(kbdDown) || GetGamePadDownDown();
    }

    public bool GetDown()
    {
        return Input.GetKey(kbdDown) || GetGamePadDown();
    }

    public bool GetDownOut()
    {
        return !Input.GetKey(kbdDown) && !GetGamePadDown();
    }

    public bool GetGamePadLeft()
    {
        return Input.GetAxis("D-Pad X") < -sensitivity || Input.GetAxis("Left Stick X") < -sensitivity;
    }

    public bool GetGamePadLeftDown()
    {
        bool current = GetGamePadLeft();
        if (!previousLeft && current)
        {
            previousLeft = true;
            return true;
        }
        if (!current)
        {
            previousLeft = false;
        }
        return false;
    }

    public bool GetLeftDown()
    {
        return Input.GetKeyDown(kbdLeft) || GetGamePadLeftDown();
    }

    public bool GetLeft()
    {
        return Input.GetKey(kbdLeft) || GetGamePadLeft();
    }

    public bool GetLeftOut()
    {
        return !Input.GetKey(kbdLeft) && !GetGamePadLeft();
    }

    public bool GetGamePadRight()
    {
        return Input.GetAxis("D-Pad X") > sensitivity || Input.GetAxis("Left Stick X") > sensitivity;
    }

    public bool GetGamePadRightDown()
    {
        bool current = GetGamePadRight();
        if (!previousRight && current)
        {
            previousRight = true;
            return true;
        }
        if (!current)
        {
            previousRight = false;
        }
        return false;
    }

    public bool GetRightDown()
    {
        return Input.GetKeyDown(kbdRight) || GetGamePadRightDown();
    }

    public bool GetRight()
    {
        return Input.GetKey(kbdRight) || GetGamePadRight();
    }

    public bool GetRightOut()
    {
        return !Input.GetKey(kbdRight) && !GetGamePadRight();
    }

    public bool GetActionDown(Action action)
    {
        return Input.GetKeyDown(GetKeyboardKeyFromAction(action)) || GetKeysDown(GetGamePadKeyFromAction(action));
    }

    public bool GetAction(Action action)
    {
        return Input.GetKey(GetKeyboardKeyFromAction(action)) || GetKeys(GetGamePadKeyFromAction(action));
    }

    public bool GetActionUp(Action action)
    {
        return Input.GetKeyUp(GetKeyboardKeyFromAction(action)) || GetKeysUp(GetGamePadKeyFromAction(action));
    }

    public KeyCode GetKeyboardKeyFromAction(Action action)
    {
        if (action == Action.Jump)
        {
            return kbdJump;
        }
        if (action == Action.Strike)
        {
            return kbdStrike;
        }
        if (action == Action.Shoot)
        {
            return kbdShoot;
        }
        if (action == Action.Abilities)
        {
            return kbdAbilities;
        }
        if (action == Action.Pause)
        {
            return kbdPause;
        }

        return KeyCode.None;
    }

    public bool GetKeysDown(KeyCode[] keys)
    {
        for (int i = 0; i < keys.Length; i++)
        {
            if (Input.GetKeyDown(keys[i]))
            {
                return true;
            }
        }
        return false;
    }

    public bool GetKeys(KeyCode[] keys)
    {
        for (int i = 0; i < keys.Length; i++)
        {
            if (Input.GetKey(keys[i]))
            {
                return true;
            }
        }
        return false;
    }

    public bool GetKeysUp(KeyCode[] keys)
    {
        for (int i = 0; i < keys.Length; i++)
        {
            if (Input.GetKeyUp(keys[i]))
            {
                return true;
            }
        }
        return false;
    }

    public KeyCode[] GetGamePadKeyFromAction(Action action)
    {
        List<KeyCode> keycodes = new List<KeyCode>();
        if (action == gpdA)
        {
            keycodes.Add(KeyCode.Joystick1Button0);
        }
        if (action == gpdB)
        {
            keycodes.Add(KeyCode.Joystick1Button1);
        }
        if (action == gpdX)
        {
            keycodes.Add(KeyCode.Joystick1Button2);
        }
        if (action == gpdY)
        {
            keycodes.Add(KeyCode.Joystick1Button3);
        }
        if (action == gpdL)
        {
            keycodes.Add(KeyCode.Joystick1Button4);
        }
        if (action == gpdR)
        {
            keycodes.Add(KeyCode.Joystick1Button5);
        }
        if (action == gpdSelect)
        {
            keycodes.Add(KeyCode.Joystick1Button6);
        }
        if (action == gpdStart)
        {
            keycodes.Add(KeyCode.Joystick1Button7);
        }
        return keycodes.ToArray();
    }

    public void SetKey()
    {
        if (setting)
        {
            KeyCode key = GetKeyBoardKey();

            if (index == 0)
            {
                ResetKeyboardKeys();
                StopSet();
            }
            else if (key != KeyCode.None && index < 10)
            {
                switch (index)
                {
                    case 1: kbdUp = key; break;
                    case 2: kbdLeft = key; break;
                    case 3: kbdDown = key; break;
                    case 4: kbdRight = key; break;
                    case 5: kbdJump = key; break;
                    case 6: kbdShoot = key; break;
                    case 7: kbdStrike = key; break;
                    case 8: kbdAbilities = key; break;
                    case 9: kbdPause = key; break;
                }
                StopSet();
            }
            else if(index == 10)
            {
                ResetGamePadKeys();
                StopSet();
            }
            else if(index==19)
            {
                Exit();
                StopSet();
            }
        }
        if (index >= 11 && index < 19)
        {
            StopSet();

            int step = 0;
            if (GetLeftDown())
            {
                step--;
            }
            if (GetRightDown())
            {
                step++;
            }
            if(step!=0)
            {
                switch (index)
                {
                    case 11: gpdA = ToggleAction(gpdA, step); break;
                    case 12: gpdB = ToggleAction(gpdB, step); break;
                    case 13: gpdX = ToggleAction(gpdX, step); break;
                    case 14: gpdY = ToggleAction(gpdY, step); break;
                    case 15: gpdL = ToggleAction(gpdL, step); break;
                    case 16: gpdR = ToggleAction(gpdR, step); break;
                    case 17: gpdSelect = ToggleAction(gpdSelect, step); break;
                    case 18: gpdStart = ToggleAction(gpdStart, step); break;
                }
            }
        }
    }

    public Action ToggleAction(Action action, int step)
    {
        return (Action)(int)Mathf.Repeat((int)action + step, System.Enum.GetValues(typeof(Action)).Length);
    }

    public void ResetKeyboardKeys()
    {
        kbdUp = KeyCode.UpArrow;
        kbdLeft = KeyCode.LeftArrow;
        kbdDown = KeyCode.DownArrow;
        kbdRight = KeyCode.RightArrow;
        kbdJump = KeyCode.F;
        kbdStrike = KeyCode.D;
        kbdShoot = KeyCode.S;
        kbdAbilities = KeyCode.A;
        kbdPause = KeyCode.Escape;
    }

    public void ResetGamePadKeys()
    {
        gpdA = Action.Strike;
        gpdB = Action.Shoot;
        gpdX = Action.Jump;
        gpdY = Action.Abilities;
        gpdL = Action.Strike;
        gpdR = Action.Shoot;
        gpdSelect = Action.Pause;
        gpdStart = Action.Pause;
    }

    public void Exit()
    {
        index = 0;
        setting = false;
    }

    public string Print()
    {
        List<string> lines = new List<string>();
        lines.Add("Reset Keyboard controls");
        lines.Add("Keyboard Up: " + kbdUp);
        lines.Add("Keyboard Left: " + kbdLeft);
        lines.Add("Keyboard Down: " + kbdDown);
        lines.Add("Keyboard Right: " + kbdRight);
        lines.Add("Keyboard Jump: " + kbdJump);
        lines.Add("Keyboard Strike: " + kbdStrike);
        lines.Add("Keyboard Shoot: " + kbdShoot);
        lines.Add("Keyboard Abilities: " + kbdAbilities);
        lines.Add("Keyboard Pause: " + kbdPause);

        lines.Add("Reset Game Pad controls");
        lines.Add("Game Pad [A]: " + gpdA);
        lines.Add("Game Pad [B]: " + gpdB);
        lines.Add("Game Pad [X]: " + gpdX);
        lines.Add("Game Pad [Y]: " + gpdY);
        lines.Add("Game Pad [L]: " + gpdL);
        lines.Add("Game Pad [R]: " + gpdR);
        lines.Add("Game Pad [Select]: " + gpdSelect);
        lines.Add("Game Pad [Start]: " + gpdStart);

        lines.Add("Exit");

        string[] l = lines.ToArray();

        string output = "";
        for (int i = 0; i < l.Length; i++)
        {
            output += l[i];
            if (index == i)
            {
                output += " <";
            }
            output += "\n";
        }

        return output;
    }

    public void Set()
    {
        setting = true;
    }

    public void StopSet()
    {
        setting = false;
    }

    public bool BeingSet()
    {
        return setting;
    }

    public enum Action
    {
        None,
        Jump,
        Strike,
        Shoot,
        Abilities,
        Pause
    }
}
