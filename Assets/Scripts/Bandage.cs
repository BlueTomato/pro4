﻿using UnityEngine;
using System.Collections;

public class Bandage : MonoBehaviour
{
    public int m;
    public BandageController p;
    private Vector3[] vertices;
    private Vector2[] uv;
    private int[] triangles;
    private Vector3[][] controlVertices;
    private Mesh mesh;
    private int vertexLengthX;
    private int vertexLengthZ;
    private int unitsX;
    private int unitsZ;
    private float factorX;
    private float factorZ;

    void Start()
    {
        mesh = gameObject.GetComponent<MeshFilter>().mesh;
        p = transform.parent.GetComponent<BandageController>();

        unitsX = (int)(p.GetUnitsX());
        unitsZ = (int)(p.GetUnitsZ());

        if (unitsX < 1)
        {
            unitsX = 1;
        }
        if (unitsZ < 1)
        {
            unitsZ = 1;
        }

        vertexLengthX = GetLengthX();
        vertexLengthZ = GetLengthZ();
     
        factorX = (float)p.GetUnitsX() / (float)unitsX;
        factorZ = (float)p.GetUnitsZ() / (float)unitsZ;


        controlVertices = new Vector3[GetLengthX()][];
        vertices = new Vector3[vertexLengthX * vertexLengthZ];
        uv = new Vector2[vertices.Length];
        triangles = p.CreateTriangles(GetUnitsX(), GetUnitsZ());

        for (int x = 0; x < GetLengthX(); x++)
        {
            controlVertices[x] = new Vector3[GetLengthZ()];
        }
    }

    public void AdjustVertices()
    {
        for (int x = 0; x < vertexLengthX; x++)
        {
            for (int z = 0; z < vertexLengthZ; z++)
            {
                vertices[x * vertexLengthZ + z] = controlVertices[x][z];
                uv[x * vertexLengthZ + z] = new Vector2((x - (GetUnitsX() / 2)), (z - (GetUnitsZ() / 2)));
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    private void Draw()
    {
        float[] axis = new float[3];

        for (int x = 0; x < GetLengthX(); x++)
        {
            float realX = x * factorX;

            for (int z = 0; z < GetLengthZ(); z++)
            {
                float realZ = z * factorZ;

                for (int n = 0; n < 3; n++)
                {
                    axis[n] = p.ApplySine(n, realX, realZ, transform.localPosition);
                    if(p.flattenUntil>=0)
                    {
                        axis[n] *= Mathf.Clamp(((float)z / (p.flattenUntil + 1)), 0f, 1f);
                    }
                    axis[n] += p.Finalize(n, realX, realZ);
                }

                controlVertices[x][z].Set(axis[0], axis[1], axis[2]);
            }
        }
        AdjustVertices();
    }

    public int GetUnitsX()
    {
        return unitsX;
    }

    public int GetUnitsZ()
    {
        return unitsZ;
    }

    public int GetLengthX()
    {
        return unitsX + 1;
    }

    public int GetLengthZ()
    {
        return unitsZ + 1;
    }


    void LateUpdate()
    {
        Draw();
    }
}
