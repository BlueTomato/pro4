﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance = null;

    public static T Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<T>();
                if (!_instance)
                {
                    GameObject go = new GameObject();
                    _instance = go.AddComponent<T>();
                    _instance.enabled = true;
                }
            }
            return _instance;
        }
    }
}