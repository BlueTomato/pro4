﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SnapObjects : MonoBehaviour
{
    public Vector3 snap = Vector3.one*0.0625f;

    void Update()
    {
        if (Application.isEditor && !Application.isPlaying)
        {
            RealignAllObjects();
        }
        else
        {
            NoSnap[] ns=NoSnap.FindObjectsOfType<NoSnap>();
            for(int i=0;i<ns.Length;i++)
            {
                Destroy(ns[i]);
            }
        }
    }


    public void RealignAllObjects()
    {
        GameObject[] allObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        for (int i = 0; i < allObjects.Length; i++)
        {
            RealignTransform(allObjects[i].transform);
        }
    }

    public void RealignTransform(Transform parent)
    {
        if (!parent.gameObject.GetComponent<NoSnap>())
        {
            Vector3 pos = parent.localPosition;

            float newPosX = pos.x;
            if (snap.x != 0)
            {
                newPosX = pos.x - Mathf.Repeat(pos.x, snap.x);
            }

            float newPosY = pos.y;
            if (snap.y != 0)
            {
                newPosY = pos.y - Mathf.Repeat(pos.y, snap.y);
            }

            float newPosZ = pos.z;
            if (snap.z != 0)
            {
                newPosZ = pos.z - Mathf.Repeat(pos.z, snap.z);
            }

            parent.localPosition = new Vector3(newPosX, newPosY, newPosZ);
            foreach (Transform t in parent.transform)
            {
                RealignTransform(t);
            }
        }
    }
}
