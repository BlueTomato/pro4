﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CenterParents : MonoBehaviour
{
    private bool execute = true;
    void Update()
    {
        if (execute && Application.isEditor && !Application.isPlaying)
        {
            CenterAllParents();
            execute = false;
            DestroyImmediate(this);
        }
    }


    public void CenterAllParents()
    {
        GameObject[] allObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        for (int i = 0; i < allObjects.Length; i++)
        {
            RealignTransform(allObjects[i].transform);
        }
    }

    public void RealignTransform(Transform parent)
    {
        foreach (Transform t in parent)
        {
            RealignTransform(t);
        }

        SmartArray<Transform> c = new SmartArray<Transform>(parent.GetComponentsInChildren<Transform>());
        c.Remove(parent);

        Transform[] parents = new Transform[c.Length];

        if (c.Length > 0)
        {
            Vector3 min = c[0].transform.position;
            Vector3 max = min;

            for (int i = 0; i < c.Length; i++)
            {
                min = Vector3.Min(min, c[i].transform.position);
                max = Vector3.Max(max, c[i].transform.position);
                parents[i] = c[i].parent;
                c[i].parent = null;
            }
            parent.transform.position =  Vector3.Lerp(min,max,0.5f);
            for (int i = 0; i < c.Length; i++)
            {
                c[i].parent = parents[i];
            }
        }
    }
}
