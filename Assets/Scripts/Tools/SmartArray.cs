﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SmartArray<T>: IEnumerable<T>
{
    [SerializeField]
    private T[] array;
    private T[] removalQueue;

    public T this[int i]
    {
        get { return array[i]; }
        set { PushAt(i, value); }
    }

    public T[] Array
    {
        get { return array; }
        set { array = value; }
    }

    public List<T> List
    {
        get { return new List<T>(array); }
    }

    public IEnumerator GetEnumerator()
    {
        return new SmartArrayEnum<T>(this);
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
        return new SmartArrayEnum<T>(this);
    }

    public virtual void OnGUI()
    {
    }

    public T[] RemovalQueue
    {
        get { return removalQueue; }
    }

    public int Length
    {
        get { return array.Length; }
        set { Resize(value); }
    }

    public int RemovalQueueLength
    {
        get { return removalQueue.Length; }
    }

    public SmartArray()
    {
        array = new T[0];
        removalQueue = new T[0];
    }

    public SmartArray(params T[] values):this()
    {
        Resize(values.Length);
        for (int i = 0; i < values.Length; i++)
        {
            array[i] = values[i];
        }
    }

    public static T[] Resize(T[] array, int length)
    {
        array = DetectNull(array);

        if (length < 0)
        {
            length = 0;
        }

        if (array.Length == length)
        {
            return array;
        }

        T[] resized = new T[length];
        for (int i = 0; i < resized.Length; i++)
        {
            if (i < resized.Length && i < array.Length)
            {
                resized[i] = array[i];
            }
            else
            {
                resized[i] = default(T);
            }
        }
        return resized;
    }

    public T[] Resize(int length)
    {
        return array = Resize(array, length);
    }

    public static T[] ResizeFillWith(T[] array, int length, T value)
    {
        int originalLength = array.Length;
        if (originalLength < length)
        {
            array = Resize(array, length);
            for (int i = originalLength; i < length; i++)
            {
                array[i] = value;
            }
        }
        return array;
    }

    public T[] ResizeFillWith(int length, T value)
    {
        return array = ResizeFillWith(array, length, value);
    }

    public static bool Contains(T[] array, T element)
    {
        if(array!=null)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != null && array[i].Equals(element))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool Contains(T element)
    {
        return Contains(array, element);
    }

    public bool RemovalQueueContains(T element)
    {
        return Contains(removalQueue, element);
    }

    public static T[] Push(T[] array, params T[] elements)
    {
        array = Resize(array, array.Length + elements.Length);
        for(int i=0;i<elements.Length;i++)
        {
            array[i+array.Length-elements.Length] = elements[i];
        }
        return array;
    }

    public T[] Push(params T[] elements)
    {
        return array = Push(array, elements);
    }

    public static T[] PushAt(T[] array, int at, T value)
    {
        array = DetectNull(array);
        if (array.Length <= at)
        {
            array = Resize(array, at + 1);
        }
        array[at] = value;
        return array;
    }

    public T[] PushAt(int at, T value)
    {
        return array = PushAt(array, at, value);
    }


    public static T[] PushUnique(T[] array, T element)
    {
        array = DetectNull(array);
        if (!Contains(array, element))
        {
            array = Push(array, element);
        }
        return array;
    }

    public T[] PushUnique(T element)
    {
        return array = PushUnique(element);
    }

    public static T[] DetectNull(T[] array)
    {
        if (array == null)
        {
            array = new T[0];
        }
        return array;
    }

    public static T[] Append(T[] array, T[] toAppend)
    {
        array = DetectNull(array);
        for(int i=0;i<toAppend.Length;i++)
        {
            array=Push(array, toAppend[i]);
        }
        return array;
    }

    public T[] Append(T[] toAppend)
    {
        return array=Append(array,toAppend);
    }

    public static T[] Remove(T[] array, T element)
    {
        array = DetectNull(array);
        bool[] removed = new bool[array.Length];
        int counter = 0;
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] != null && array[i].Equals(element))
            {
                removed[i] = true;
                counter++;
            }
        }
        if (counter > 0)
        {
            T[] newArray = new T[array.Length - counter];
            int ni = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (!removed[i])
                {
                    newArray[ni] = array[i];
                    ni++;
                }
            }
            return newArray;
        }
        return array;
    }

    public T[] Remove(T element)
    {
        return array = Remove(array, element);
    }

    public static T[] RemoveIndex(T[] array, int index)
    {
        array = DetectNull(array);
        if (array.Length > 0 && index < array.Length)
        {
            T[] newArray = new T[array.Length - 1];
            for (int i = 0, ni = 0; i < array.Length; i++, ni++)
            {
                if (ni == index)
                {
                    ni++;
                }
                newArray[ni] = array[i];
            }
            return newArray;
        }
        return array;
    }

    public T[] RemoveIndex(int index)
    {
        return RemoveIndex(array, index);
    }

    public static T[] Pop(T[] array)
    {
        return Resize(array, array.Length - 1);
    }

    public T[] Pop()
    {
        return Pop(array);
    }

    public static T[] Flip(T[] array)
    {
        if (array.Length > 1)
        {
            T[] flipped = new T[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                flipped[i] = array[array.Length - 1 - i];
            }
            return flipped;
        }
        return array;
    }

    public T[] Flip()
    {
        return array=Flip(array);
    }

    public static T[] Swap(T[] array, int a, int b)
    {
        if (a != b)
        {
            int length = Mathf.Max(a, b);
            if (array.Length <= length)
            {
                array = Resize(array, length);
            }
            T oldA = array[a];
            array[a] = array[b];
            array[b] = oldA;
        }
        return array;
    }

    public T[] Swap(int a, int b)
    {
        return array = Swap(array, a, b);
    }

    public static T First(T[] array)
    {
        return array[0];
    }

    public T First()
    {
        return First(array);
    }

    public static T Last(T[] array)
    {
        return array[array.Length - 1];
    }

    public T Last()
    {
        return Last(array);
    }

    public static int Index(T[] array, T element)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i].Equals(element))
            {
                return i;
            }
        }
        return -1;
    }

    public int Index(T element)
    {
        return Index(array, element);
    }

    public static T[] RemoveEmpty(T[] array)
    {
        return Remove(array, default(T));
    }

    public T[] RemoveEmpty()
    {
        return array=RemoveEmpty(array);
    }

    public T[] QueueToBeRemoved(T element)
    {
        return removalQueue = Push(removalQueue,element);
    }

    public void ClearQueue()
    {
        for(int i=0;i<removalQueue.Length;i++)
        {
            Remove(removalQueue[i]);
        }
        removalQueue = new T[0];
    }

    public static T[] Empty(T[] array)
    {
        return Resize(array,0);
    }

    public T[] Empty()
    {
        return array=Empty(array);
    }
}

public class SmartArrayEnum<T> : IEnumerator<T>
{
    private SmartArray<T> smartArray;
    private int position = -1;

    public SmartArrayEnum(SmartArray<T> smartArray)
    {
        this.smartArray = smartArray;
    }

    public T Current
    {
        get
        {
            try
            {
                return smartArray[position];
            }
            catch (IndexOutOfRangeException)
            {
                throw new InvalidOperationException();
            }
        }
    }

    object IEnumerator.Current
    {
        get
        {
            return Current;
        }
    }

    public void Dispose()
    {
    }

    public bool MoveNext()
    {
        position++;
        return (position < smartArray.Length);
    }

    public void Reset()
    {
        position = -1;
    }
}