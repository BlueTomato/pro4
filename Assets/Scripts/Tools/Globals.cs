﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Globals
{
    public static T[] GetValues<T> ()
    {
        return (T[])Enum.GetValues(typeof(T));
    }

    public static float SineInRange(float value, float min, float max)
    {
        if (min > max)
        {
            float swap = min;
            min = max;
            max = swap;
        }
        return ((1f + value) / 2f) * (max - min) + min;
    }

    public static bool AngleIsWithin(float angle, float min, float max)
    {
        angle = Mathf.Repeat(angle,360);
        min = Mathf.Repeat(min, 360);
        max = Mathf.Repeat(max, 360);

        if (max>min)
        {
            angle = Mathf.Repeat(angle-min, 360);
            max = Mathf.Repeat(max-min, 360);
            if (angle <= max)
            {
                return true;
            }
        }
        else
        {
            angle = Mathf.Repeat(angle-max, 360);
            min = Mathf.Repeat(min-max, 360);
            if(angle>=min)
            {
                return true;
            }
        }
        if(angle==0)
        {
            return true;
        }

        return false;
    }

    public static float RestrictAngleOutsideOf(float angle, float lowerBar, float upperBar)
    {
        if (Globals.AngleIsWithin(angle, lowerBar, upperBar))
        {
            float deltaA = Mathf.DeltaAngle(angle, lowerBar);
            float deltaB = Mathf.DeltaAngle(angle, upperBar);

            if (Mathf.Abs(deltaA) < Mathf.Abs(deltaB))
            {
                angle += deltaA;
            }
            else
            {
                angle += deltaB;
            }
        }

        return angle;
    }

    public static float RestrictAngleWithin(float angle, float lowerBar, float upperBar)
    {
        return RestrictAngleOutsideOf(angle, upperBar, lowerBar);
    }

    public static float VectorToAngle(Vector2 vector)
    {
        return Mathf.Repeat(-(Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg - 90), 360);
    }

    public static Vector2 AngleToVector(float angle)
    {
        angle *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
    }

    public static Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float weight)
    {
        Matrix4x4 retVal = new Matrix4x4();
        for (int i = 0; i < 16; i++)
        {
            retVal[i] = Mathf.Lerp(from[i], to[i], weight);
        }
        return retVal;
    }

    public static List<T> GetComponentsRecursive<T>(Transform t) where T : Component
    {
        List<T> retVal = new List<T>();

        T component = t.GetComponent<T>();
        if (component)
        {
            retVal.Add(component);
        }
        foreach (Transform child in t)
        {
            retVal.AddRange(GetComponentsRecursive<T>(child));
        }
        return retVal;
    }

    public static List<T> GetComponentsInChildrenRecursive<T>(Transform t) where T : Component
    {
        List<T> retVal = new List<T>();
        foreach (Transform child in t)
        {
            T component = child.GetComponent<T>();
            if (component)
            {
                retVal.Add(component);
            }
            retVal.AddRange(GetComponentsInChildrenRecursive<T>(child));
        }
        return retVal;
    }

    public static T GetComponentRecursive<T>(Transform t) where T : Component
    {
        T component = t.GetComponent<T>();
        if (component)
        {
            return component;
        }
        foreach (Transform child in t)
        {
            component = GetComponentRecursive<T>(child);
            if(component)
            {
                return component;
            }
        }
        return component;
    }

    public static T GetComponentInChildrenRecursive<T>(Transform t) where T : Component
    {
        foreach (Transform child in t)
        {
            T component = child.GetComponent<T>();
            if (!component)
            {
                component = GetComponentInChildrenRecursive<T>(child);
            }
            if(component)
            {
                return component;
            }
        }
        return null;
    }

    public static float SignZero(float number)
    {
        if(number==0)
        {
            return 0;
        }
        return Mathf.Sign(number);
    }
}

