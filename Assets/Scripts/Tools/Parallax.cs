﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;

public class Parallax : MonoBehaviour
{
    private Vector3 origin;
    public bool parallaxX = true;
    public bool parallaxY = true;

    void Awake()
    {
        origin = transform.position;
    }

    void Start()
    {
        Place();
    }

    void Update ()
    {
        Place();
    }

    private void Place()
    {
        if(Camera.main)
        {
            float offsetX = 0;
            float offsetY = 0;
            if (parallaxX)
            {
                offsetX = transform.position.z * Camera.main.transform.position.x * GameManager.Instance.SmallestInstance * CameraManager.Instance.Parallax;
                offsetY = GameManager.Instance.PixelPerfect(offsetY);
            }
            if (parallaxY)
            {
                offsetY = transform.position.z * Camera.main.transform.position.y * GameManager.Instance.SmallestInstance * CameraManager.Instance.Parallax;
                offsetY = GameManager.Instance.PixelPerfect(offsetY);
            }

            transform.position = new Vector3(origin.x + offsetX, origin.y + offsetY, transform.position.z);
        }
    }
}
