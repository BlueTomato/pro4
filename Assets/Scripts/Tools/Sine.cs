﻿using UnityEngine;
using System.Collections;

public static class Sine
{
    public static float Get(float x, float min, float max, float period)
    {
        return ((Mathf.Sin((x * (Mathf.PI * 2f)) / period) + 1) / 2f) * (max - min) + min;
    }

    public static float Get(float x, float min, float max)
    {
        return Get(x, min, max, 1f / (Mathf.PI * 2f));
    }
}