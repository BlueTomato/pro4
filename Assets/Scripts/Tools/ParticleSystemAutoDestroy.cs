﻿using UnityEngine;
using System.Collections;

public class ParticleSystemAutoDestroy : MonoBehaviour
{
    private ParticleSystem particleSys;

    public ParticleSystem ParticleSystem
    {
        get
        {
            if(!particleSys)
            {
                particleSys = GetComponent<ParticleSystem>();
            }
            return particleSys;
        }
    }

    public void Update()
    {
        if (!ParticleSystem || ParticleSystem && !ParticleSystem.IsAlive())
        {
            Destroy(gameObject);
        }
    }
}