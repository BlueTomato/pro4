﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialComparer : IComparer<Material>
{
    public int Compare(Material x, Material y)
    {
        if (x.Equals(y))
        {
            return 0;
        }
        if (x.GetInstanceID()<y.GetInstanceID())
        {
            return 1;
        }
        return -1;
    }
}