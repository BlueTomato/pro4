﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextMeshShadow : MonoBehaviour
{
    public TextMesh parent;
    public TextMesh textMesh;

    public static TextMeshShadow SetShadow(TextMesh tm,Vector3 offset,Color color)
    {
        TextMesh textMesh = Instantiate(tm);
        textMesh.color = color;
        textMesh.transform.localPosition = offset;
        textMesh.transform.SetParent(tm.transform);

        TextMeshShadow tms = textMesh.gameObject.AddComponent<TextMeshShadow>();
        tms.textMesh = textMesh;
        tms.parent = tm;

        textMesh.gameObject.name = tm.name+" Shadow";

        return tms;
    }

    void Update ()
    {
        textMesh.text = parent.text;
	}
}