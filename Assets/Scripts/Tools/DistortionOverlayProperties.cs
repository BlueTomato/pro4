﻿using Anima2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistortionOverlayProperties : MonoBehaviour
{
    public List<SpriteRenderer> spriteRenderer = new List<SpriteRenderer>();
    public List<SpriteMeshInstance> spriteMeshInstances = new List<SpriteMeshInstance>();
    public List<Material> material = new List<Material>();
    public List<Material> originalMaterial = new List<Material>();
    public float distortionFactor = 1;
    public bool distortionLocal;
    public float overlayFactor = 1;
    public bool overlayLocal;
    public bool extentCorrection=true;
     
	void Start ()
    {
        GetMaterial();
	}

    private List<Material> GetMaterial()
    {
        if (material.Count<1)
        {
            material = new List<Material>();
            originalMaterial = new List<Material>();

            spriteRenderer = Globals.GetComponentsRecursive<SpriteRenderer>(transform);
            spriteMeshInstances = Globals.GetComponentsRecursive<SpriteMeshInstance>(transform);

            for (int i = 0; i < spriteRenderer.Count; i++)
            {
                if (spriteRenderer[i])
                {
                    originalMaterial.Add(spriteRenderer[i].sharedMaterial);
                    material.Add(new Material(originalMaterial[i]));
                    spriteRenderer[i].material = material[i];
                }
            }

            for (int i = 0; i < spriteMeshInstances.Count; i++)
            {
                if (spriteMeshInstances[i])
                {
                    originalMaterial.Add(spriteMeshInstances[i].sharedMaterial);
                    material.Add(new Material(originalMaterial[i]));
                    spriteMeshInstances[i].sharedMaterial = material[i];
                }
            }
        }
        return material;
    }
	
	void LateUpdate ()
    {
        for(int i=0;i<material.Count;i++)
        {
            material[i].CopyPropertiesFromMaterial(originalMaterial[i]);
            if (distortionFactor != 0)
            {
                Vector2 position = transform.position;
                if(distortionLocal)
                {
                    position = spriteRenderer[i].transform.position;
                }
                material[i].SetFloat("_PositionOffsetX", position.x * distortionFactor);
                material[i].SetFloat("_PositionOffsetY", position.y * distortionFactor);
            }
            if (overlayFactor != 0)
            {
                Vector2 position = transform.position;
                if (overlayLocal)
                {
                    position = spriteRenderer[i].transform.position;
                }
                material[i].SetFloat("_OverlayOffsetX", position.x * overlayFactor);
                material[i].SetFloat("_OverlayOffsetY", position.y * overlayFactor);
            }
            if (extentCorrection)
            {
                material[i].SetFloat("_ExtentX", spriteRenderer[i].bounds.extents.x);
                material[i].SetFloat("_ExtentY", spriteRenderer[i].bounds.extents.y);
            }
        }
    }
}
