﻿float _Pixel;

float repeat (float value, float ceiling)
{
	while(value<0)
	{
		value+=ceiling;
	}
	while(value>ceiling)
	{
		value-=ceiling;
	}
	return value;
}
 
float2 repeat (float2 value, float ceiling)
{
	return float2(repeat(value.x,ceiling),repeat(value.y,ceiling));
}
 
float2 repeat (float2 value, float2 ceiling)
{
	return float2(repeat(value.x,ceiling.x),repeat(value.y,ceiling.y));
}

float4 texPoint(sampler2D tex, float2 uv, float4 texelSize)
{
	float2 halfPixel=0.5*texelSize.xy;

	uv*=texelSize.zw;
	uv=floor(uv)+0.5;
	uv*=texelSize.xy;

	return tex2Dgrad(tex, uv,0,0);
}

float4 texBilinear(sampler2D tex, float2 uv, float4 texelSize)
{
	float2 halfPixel=0.5*texelSize.xy;

	uv*=texelSize.zw;
	float2 weight=uv;
	uv=round(uv);
	weight-=uv;
	weight+=0.5;
	uv*=texelSize.xy;

	float4 pixel1=tex2Dgrad(tex, repeat(uv+float2(-halfPixel.x,-halfPixel.y),1),0,0);
	float4 pixel2=tex2Dgrad(tex, repeat(uv+float2(halfPixel.x,-halfPixel.y),1),0,0);
	float4 pixel3=tex2Dgrad(tex, repeat(uv+float2(-halfPixel.x,halfPixel.y),1),0,0);
	float4 pixel4=tex2Dgrad(tex, repeat(uv+float2(halfPixel.x,halfPixel.y),1),0,0);
	float4 pixel12=lerp(pixel1,pixel2,weight.x);
	float4 pixel34=lerp(pixel3,pixel4,weight.x);

	return lerp(pixel12,pixel34,weight.y);
}

float4 texBilinearAtBorders(sampler2D tex, float2 uv, float4 texelSize)
{
	float2 halfPixel=0.5*texelSize.xy;
	float4 result;

	if(uv.x<=halfPixel.x || uv.y<=halfPixel.y || uv.x>=1-halfPixel.x || uv.y>=1-halfPixel.y)
	{
		result=texBilinear(tex,uv,texelSize);
	}
	else
	{
		result=tex2D(tex,uv);
	}
	return result;
}

float3 shiftColors(float3 current, float rg, float rb, float gb)
{
	float3 colorShift=current;

	float mixRG=lerp(current.r,current.g,rg);
	float mixRB=lerp(current.r,current.b,rb);
	float mixGR=lerp(current.g,current.r,rg);
	float mixGB=lerp(current.g,current.b,gb);
	float mixBR=lerp(current.b,current.r,rb);
	float mixBG=lerp(current.b,current.g,gb);

	colorShift.r=lerp(mixRG,mixRB,(1-rg+rb)/2);
	colorShift.g=lerp(mixGR,mixGB,(1-rg+gb)/2);
	colorShift.b=lerp(mixBR,mixBG,(1-rb+gb)/2);

	return colorShift;
}

float4 setBoundaries(float4 c, float4 minCol, float4 maxCol)
{
	c=lerp(minCol,maxCol,c);
	return c;
}

float4 recolor(float4 c, float4 targetCol, float weight, float multiply, float brightness)
{
	if(weight!=0 || multiply!=0 || brightness!=1)
	{
		float Alpha=c.a;

		brightness=2-brightness;
		if(brightness<1)
		{
			c.rgb+=(1-brightness)*0.001;
		}
		else if(brightness>1)
		{
			c.rgb-=(brightness-1)*0.001;
			float power=pow(brightness,brightness);
			brightness=pow(power,power);
		}
		if(brightness!=1)
		{
			c.rgb=pow(c,brightness);
		}
		c=lerp(lerp(c,c*targetCol,multiply),targetCol,weight);
		c.a*=Alpha;
	}
	c.rgb*=c.a;
	return c;
}

float4 _Color;
float4 _AddColor;
float4 _SubtractColor;
float4 _TargetColor;
float4 _MinColor;
float4 _MaxColor;
float _Weight;
float _Multiply;
float _Brightness;
float _ShiftRedGreen;
float _ShiftRedBlue;
float _ShiftGreenBlue;

sampler2D _Overlay;
float4 _Overlay_TexelSize;

float4 _OverlayColor;
float _OverlayScale;
float _OverlayScaleX;
float _OverlayScaleY;
float _OverlaySpeedX;
float _OverlaySpeedY;
float _OverlayOffsetX;
float _OverlayOffsetY;
float _OverlayRepeat;

float _CenterOverlay;

float4 applyColorOperations(float4 c, float2 worldPos)
{
	c.rgb=shiftColors(c,_ShiftRedGreen,_ShiftRedBlue,_ShiftGreenBlue);
	c=recolor(c,_TargetColor,_Weight,_Multiply,_Brightness);
	c+=_AddColor;
	c=min(c,1);
	c-=_SubtractColor;
	c=max(c,0);
	c*=_Color*c.a;

	float2 overlayScale=float2(_OverlayScaleX,_OverlayScaleY)*_OverlayScale;

	if(_OverlayColor.a>0 && overlayScale.x!=0 && overlayScale.y!=0)
	{
		float2 seed=float2(0,0);
		float2 finalScale=float2(0,0);
		if(_OverlayScale!=0)
		{
			finalScale=(_Pixel*_Overlay_TexelSize.xy)/overlayScale;
		}
		if(finalScale.x!=0 && finalScale.y!=0)
		{
			seed.x+=(_Time.g*_OverlaySpeedX)*finalScale.x-_OverlayOffsetX*finalScale;
			seed.y+=(_Time.g*_OverlaySpeedY)*finalScale.y-_OverlayOffsetY*finalScale;
			seed+=worldPos.xy*finalScale;
			if(_CenterOverlay>0)
			{
				seed-=0.5;
			}
			_OverlayRepeat/=2;
			if(_OverlayRepeat<0 || (seed.x+0.5>-_OverlayRepeat && seed.x+0.5<_OverlayRepeat && seed.y+0.5>-_OverlayRepeat && seed.y+0.5<_OverlayRepeat))
			{
				seed=repeat(seed,1);
				float4 overlay=texPoint(_Overlay,seed,_Overlay_TexelSize);
				overlay.rgb*=_OverlayColor.rgb;
				c.rgb=lerp(c.rgb,overlay*c.a,_OverlayColor.a*overlay.a);
			}
		}
	}
	c.rgb=setBoundaries(c,_MinColor,_MaxColor).rgb;

	return c;
}