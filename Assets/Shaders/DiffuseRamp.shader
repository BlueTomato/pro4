﻿Shader "Sprites/Diffuse-Ramp"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Ramp ("Ramp", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        _Cutoff ("Shadow alpha cutoff", Range(0,1)) = 0.5
    }
 
    SubShader
    {
        Tags
        {
            "Queue"="AlphaTest"
            "IgnoreProjector"="True"
            "RenderType"="TransparentCutout"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Cull Off
        Lighting On
        ZWrite On
	    Blend One OneMinusSrcAlpha
 
        CGPROGRAM
        #pragma surface surf Ramp addshadow alphatest:_Cutoff
 
        sampler2D _MainTex;
        fixed4 _Color;
 
        struct Input
        {
            float2 uv_MainTex;
        };

		sampler2D _Ramp;

		half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten)
		{
			half3 ramp;
			if(atten>0.00011)
			{
				ramp=tex2D (_Ramp, float2(atten*2,0)).rgb;
			}
			else
			{
				ramp.rgb=0;
			}
			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * ramp;
			c.a = s.Alpha;
			return c;
        }
 
        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
 
    Fallback "Sprites/Diffuse"
}