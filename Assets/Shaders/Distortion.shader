﻿Shader "Sprites/Diffuse-Distortion"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_AddColor ("Add Color", Color) = (0,0,0,0)
		_SubtractColor ("Subtract Color", Color) = (0,0,0,0)
		_TargetColor ("Tint", Color) = (1,1,1,1)
		_Cutoff ("Shadow alpha cutoff", Range(0,1)) = 0.5
		//Distortion map; Only the red and green channel are read
		_Distortion ("Distortion map (RGB)", 2D) = "white" {}
		_SpeedX("Speed X", float)=0.1
		_SpeedY("Speed Y", float)=0.1
		//Scale of the distortion map
		_Scale("Scale", float)=1
		//How many pixels are in one unit in world space
		_Pixel ("Pixel per unit", int)=16
		//What are the extents of the renderer; makes vertex offset more accurate
		_ExtentX ("Extent X (optional)", float)=0
		_ExtentY ("Extent Y (optional)", float)=0
		//The maximum displacement in pixels
		_DisplacementScale("Displacement Scale", range(0, 1))=1
		_DisplacementX("Displacement X", range(0, 256))=1
		_DisplacementY("Displacement Y", range(0, 256))=1
		_Dissolve ("Dissolve", Range(0,1))=0
		//For sprites that are not centered
		_PivotOffsetX ("Pivot offset X", Range(-1,1))=0
		_PivotOffsetY ("Pivot offset Y", Range(-1,1))=0
		//Optional parameters that are to be manipulated with external scripts to, for instance, offset the additional accrued distortion of moving objects
		_PositionOffsetX ("Position offset X", float)=0
		_PositionOffsetY ("Position offset Y", float)=0
		//Interpolation of the pixels from the distortion map
		[MaterialToggle] _Smooth("Smooth", Float) = 1

		_MinColor ("Minimum Color", Color) = (0,0,0,0)
		_MaxColor ("Maximum Color", Color) = (1,1,1,1)
		_Weight("Weight", range(0, 1))=0
		_Multiply("Multiply", range(0, 1))=0
		_Brightness("Brightness", range(0, 2))=1
		_ShiftRedGreen("Shift Red Green", range(0, 1))=0
		_ShiftRedBlue("Shift Red Blue", range(0, 1))=0
		_ShiftGreenBlue("Shift Green Blue", range(0, 1))=0

		_Overlay ("Overlay Texture (RGBA)", 2D) = "white" {}
		_OverlayColor ("Overlay Color", Color) = (0,0,0,0)
		_OverlayScale ("Overlay Scale",  float)=1
		_OverlayScaleX ("Overlay Scale X",  float)=1
		_OverlayScaleY ("Overlay Scale Y",  float)=1
		_OverlaySpeedX("Overlay Speed X", float)=0.1
		_OverlaySpeedY("Overlay Speed Y", float)=0.1
		_OverlayOffsetX ("Overlay offset X", float)=0
		_OverlayOffsetY ("Overlay offset Y", float)=0
		_OverlayRepeat ("Overlay repeat", float)=-1

		[MaterialToggle] _CenterOverlay("Center Overlay", Float) = 0
	}
		  
	SubShader
	{
		Tags
		{
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
            "RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			"DisableBatching"="True"
		}
		LOD 200
		 
        Cull Off
        Lighting Off
        ZWrite Off
        Fog { Mode Off }
        Blend One OneMinusSrcAlpha
							 
		CGPROGRAM
		#pragma surface surf NoLighting vertex:vert alphatest:_Cutoff
		#include "Assets/Shaders/Recolor.cginc"

		sampler2D _MainTex;
		sampler2D _Distortion;
	   
		float _SpeedX;
		float _SpeedY;
		float _Scale;
		float _DisplacementScale;
		float _DisplacementX;
		float _DisplacementY;
	   
		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
			float4 color : COLOR;
		};

		half4 LightingNoLighting (SurfaceOutput s, half3 lightDir, half atten)
		{
			half4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		float _PivotOffsetX;
		float _PivotOffsetY;
		float _ExtentX;
		float _ExtentY;
		void vert (inout appdata_full v)
		{
			const float2 displacement=float2(_DisplacementX,_DisplacementY)*_DisplacementScale;
			const float2 extents=float2(_ExtentX,_ExtentY);
			float2 direction;
			if(extents.x>0 && extents.y>0)
			{
				direction=v.vertex.xy/extents.xy;
			}
			else
			{
				direction=sign(v.vertex).xy;
				if(direction.x==0 && _PivotOffsetX!=0)
				{
					direction.x=_PivotOffsetX;
				}
				if(direction.y==0 && _PivotOffsetY!=0)
				{
					direction.y=-_PivotOffsetY;
				}
			}

			v.vertex.xy += direction*displacement/_Pixel;
		}

		float4 _MainTex_TexelSize;
		float4 _Distortion_TexelSize;

		float _PositionOffsetX;
		float _PositionOffsetY;
		float _Smooth;

		float _Dissolve;

		void surf (Input IN, inout SurfaceOutput o)
		{
			const float2 displacement=float2(_DisplacementX,_DisplacementY)*_DisplacementScale;
			_Dissolve*=_DisplacementScale;
			float2 uv=IN.uv_MainTex;
			float Alpha=1;

			if(displacement.x>0 || displacement.y>0 || _Dissolve>0)
			{
				float2 distortionSeed=float2(0,0);
				float2 finalScale=float2(0,0);
				if(_Scale!=0)
				{
					finalScale=(_Pixel*_Distortion_TexelSize.xy)/_Scale;
				}
				if(finalScale.x!=0 && finalScale.y!=0)
				{
					distortionSeed.x+=(_Time.g*_SpeedX)-_PositionOffsetX*finalScale.x;
					distortionSeed.y+=(_Time.g*_SpeedY)-_PositionOffsetY*finalScale.y;
					distortionSeed+=IN.worldPos.xy*finalScale;
					distortionSeed=repeat(distortionSeed,1);

					float4 distortion;
					if(_Smooth!=0)
					{
						distortion=texBilinearAtBorders(_Distortion,distortionSeed,_Distortion_TexelSize);
					}
					else
					{
						distortion=texPoint(_Distortion,distortionSeed,_Distortion_TexelSize);
					}

					float2 offset=distortion;

					offset-=0.5;
					offset*=2*displacement*_MainTex_TexelSize.xy;

					uv-=0.5;
					float2 uvAlpha=uv*_Distortion_TexelSize.xy*(_MainTex_TexelSize.zw+displacement*2)+0.5+offset;
					uv=uv*_MainTex_TexelSize.xy*(_MainTex_TexelSize.zw+displacement*2)+0.5+offset;

					distortion.b=tex2D(_Distortion,uvAlpha).b;

					if(_Dissolve>0)
					{
						_Dissolve*=2;
						if(_Dissolve<1)
						{
							Alpha*=1-((1-distortion.b)*_Dissolve);
						}
						else
						{
							Alpha*=(distortion.b*(1-(_Dissolve-1)));
						}
					}
				}
			}

			float4 c = tex2D (_MainTex, uv);

			c=applyColorOperations(c,IN.worldPos.xy);

			o.Albedo=c*IN.color*Alpha*_Color.a;
			o.Alpha=c.a*IN.color.a*Alpha*_Color.a;
			if(uv.x<0 || uv.y<0 || uv.x>1 || uv.y>1)
			{
				o.Alpha=0;
			}
		}
		ENDCG
	}
}